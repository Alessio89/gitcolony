uniform extern texture ScreenTexture;
sampler screen = sampler_state{ Texture = ScreenTexture; };

uniform extern texture MaskTexture;
sampler mask = sampler_state{ Texture = MaskTexture; };

float4 PixelShaderFunction(float2 inCoords : TEXCOORD0) : COLOR
{
	float4 color = tex2D(screen, inCoords);

	color.rgba = color.rgba - tex2D(mask, inCoords).r;
	return color;
}

technique Technique1  
{  
    pass Pass1  
    {  
        PixelShader = compile ps_2_0 PixelShaderFunction();  
    }  
}  