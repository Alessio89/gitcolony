sampler s0;

texture lightMask;
sampler lightSampler = sampler_state{ Texture = lightMask; };

texture map;
sampler mapSampler = sampler_state{ Texture = map; };


float4 param1;
float ambientPower;
texture rainbow;
sampler rainbow_sampler = sampler_state{ Texture = rainbow; };



float visibility;

float4 MiniMapFoW(float2 coords:TEXCOORD0) : COLOR0
{

	float4 color = tex2D(s0, coords);
	 
	float4 mapColor = tex2D(mapSampler, coords);

	return mapColor;


}

float4 FoW(float2 coords:TEXCOORD0) : COLOR0
{
	float4 color = tex2D(s0, coords);
	return color/(visibility);
	//return color;
}

float4 Lights(float2 coords: TEXCOORD0) : COLOR0  
{  

    float4 color = tex2D(s0, coords);
	float4 lightColor = tex2D(lightSampler, coords); 
	return color*lightColor;
}  

float4 AmbientLight(float2 coords: TEXCOORD0) : COLOR0
{
	float4 color = tex2D(s0, coords);

	if (color.a)
	color.rgba = color.rgba + (param1*ambientPower);

	return color;
}

float4 ParticleLight(float2 coords: TEXCOORD0) : COLOR0
{
	float4 color = tex2D(s0, coords);

	if (!color.a)
	color.rgb = color.bgr;

	return color;
}
  
technique Technique1  
{  
    pass Pass1  
    {  
        PixelShader = compile ps_2_0 Lights();  
    }  
}  

technique AmbientColor
{

	pass Pass1
	{
		PixelShader = compile ps_2_0 AmbientLight();
	}

}

technique ParticleLights
{
	pass Pass1
	{
		PixelShader = compile ps_2_0 ParticleLight();
	}
}

technique FogOfWar
{
	pass Pass1
	{
		PixelShader = compile ps_2_0 FoW();
	}
}