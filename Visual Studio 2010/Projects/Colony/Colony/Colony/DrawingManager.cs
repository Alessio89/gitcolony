using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using C3.XNA;

namespace Colony
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    /// 
    public class Line
    {
        public Vector2 start;
        public Vector2 end;
        public Color color;
       public Action Update;

    }

    public static class DrawingManager
    {

        public static List<Line> Lines = new List<Line>();
        public static Game1 GameRef;

        public static void Update(GameTime gameTime)
        {
            foreach (Line l in Lines)
            {
                if (l.Update != null)
                    l.Update();
            }
        }
   

        public static void Draw(GameTime gameTime)
        {
           
        }
    }
}
