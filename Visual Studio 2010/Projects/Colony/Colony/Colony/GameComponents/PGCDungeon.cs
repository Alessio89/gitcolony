﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements.Items.Map_Items;

namespace Colony.Gameplay_Elements
{

    public enum DungeonType
    {
        Labyrinth = 0,
        Castle,
        Cave,
        Prisons,
        Stronghold
    }

    public struct DungeonTiles
    {
        public string tileSet;
        public List<int> floorTiles;
        public List<int> roofTiles;
        public int wallTile;
        public int bottomLeft;
        public int bottomRight;
        public List<int> corridorTiles;
        public int doorTile;
    }

    public static class PGCDungeon
    {
        #region Field Region
        public static Dictionary<DungeonType, DungeonTiles> DungeonTileInterpreter = new Dictionary<DungeonType, DungeonTiles>();
        static Random rnd = new Random();
        #endregion

        #region Methods Region
        public static void Initialize()
        {
            DungeonTiles labyrinthTiles = new DungeonTiles();
            labyrinthTiles.wallTile =13;
            labyrinthTiles.floorTiles = new List<int>() { 0, 1, 2 };

            //labyrinthTiles.roofTiles = new List<int>() { 5, 6, 7, 10, 11, 12, 15, 16, 17 };
            labyrinthTiles.corridorTiles = new List<int>() {24,25,26};
            labyrinthTiles.doorTile = 30;
            labyrinthTiles.bottomLeft = 18;
            labyrinthTiles.bottomRight = 19;
            labyrinthTiles.tileSet = "dungeontiles";

            DungeonTileInterpreter.Add(DungeonType.Labyrinth, labyrinthTiles);
        }

        public static void Interpret(DungeonType dungeonType, int[,] grid, Map map)
        {
            map.Layers.Clear();
            map.AddLayer();
            map.AddLayer();
            // map.Enemies.Clear();
            //float monsterChance = 1f;

            for (int y = 0; y < grid.GetLength(0); y++)
            {
                for (int x = 0; x < grid.GetLength(1); x++)
                {
                    map.Layers[0].Grid[y, x] = new GridCell(-1, -1, -1, map);
                    // Empty Space
                    if (grid[y, x] == 0)
                    {
                        map.PlaceTile(x, y, new GridCell(DungeonTileInterpreter[dungeonType].wallTile, 1, 0, map), 0);
                    }
                    // Room Floor tile
                    else if (grid[y, x] == 1)
                    {
                        int randomfloortile = rnd.Next(DungeonTileInterpreter[dungeonType].floorTiles.Count);
                        map.Layers[0].Grid[y, x] = new GridCell(DungeonTileInterpreter[dungeonType].floorTiles[randomfloortile], 0, 0, map);

                    }

                    // Corridor floor tile
                    else if (grid[y, x] == 2)
                    {
                        map.Layers[0].Grid[y, x] = new GridCell(DungeonTileInterpreter[dungeonType].corridorTiles[rnd.Next(DungeonTileInterpreter[dungeonType].corridorTiles.Count)], 5, 0, map);

                    }
                    // Door Tile
                    else if (grid[y, x] == 3)
                    {
                        map.Layers[0].Grid[y, x] = new GridCell(DungeonTileInterpreter[dungeonType].corridorTiles[rnd.Next(DungeonTileInterpreter[dungeonType].corridorTiles.Count)], 0, 0, map);
                        map.Layers[1].Grid[y, x] = new GridCell(DungeonTileInterpreter[dungeonType].doorTile, 0, 0, map);
                    }
                    map.Layers[0].Grid[y, x].X = x;
                    map.Layers[0].Grid[y, x].Y = y;
                }



                foreach (BaseMapItem i in map.MapItems)
                {
                    map.Layers[0].Grid[i.GridY, i.GridX].Event = 2;
                }

            }

            List<GridCell> doors = new List<GridCell>();
            for (int y = 0; y < map.Grid.GetLength(0); y++)
                for (int x = 0; x < map.Grid.GetLength(1); x++)
                {
                    GridCell gc = map.Layers[0].Grid[y, x];
                    if (map.Layers[0].Grid[y, x].TileID != DungeonTileInterpreter[dungeonType].wallTile)
                    {
                        map.Layers[0].Grid[y, x].neighbourhood = 99;
                        
                    }
                    else
                        map.Layers[0].Grid[y, x].neighbourhood = map.Layers[0].Grid[y, x].CalculateNeighbours();

                    if (DungeonTileInterpreter[dungeonType].corridorTiles.Contains(map.Layers[0].Grid[y, x].TileID))
                    {
                        gc.neighbourhood = gc.CalculateNeighboursByEvent();
                        gc.neighbourhood += 20;
                    }
                }

                 for (int y = 0; y < map.Grid.GetLength(0); y++)
                for (int x = 0; x < map.Grid.GetLength(1); x++)
                {
                    GridCell gc = map.Layers[0].Grid[y, x];
                    if (map.Layers[0].Grid[y, x].TileID != 13)
                    {
                        map.Layers[0].Grid[y, x].neighbourhood = 99;
                    }
                    else
                        map.Layers[0].Grid[y, x].neighbourhood = map.Layers[0].Grid[y, x].CalculateNeighbours();



                }

            for (int y = 0; y < map.Grid.GetLength(0); y++)
                for (int x = 0; x < map.Grid.GetLength(1); x++)
                {
                    GridCell gc = map.Layers[0].Grid[y, x];
                    if (gc.neighbourhood == 13)
                        gc.TileID = 12;
                     if (gc.neighbourhood == 7)
                        gc.TileID = 14;
                     if (gc.neighbourhood == 11)
                        gc.TileID = 7;
                     if (gc.neighbourhood == 14)
                        gc.TileID = 19;
                     if (gc.neighbourhood == 6)
                        gc.TileID = 21;
                     if (gc.neighbourhood == 12)
                        gc.TileID = 22;
                     if (gc.neighbourhood == 9)
                        gc.TileID = 7;
                     if (gc.neighbourhood == 3)
                        gc.TileID = 7;
                     if (gc.neighbourhood == 5)
                        gc.TileID = 15;
                     if (gc.neighbourhood == 1)
                        gc.TileID = 7;
                     if (gc.neighbourhood == 10 || gc.neighbourhood == 8 ||gc.neighbourhood == 2)
                         gc.TileID = 7;
                     if (gc.neighbourhood == 4)
                         gc.TileID = 23;
                    
                     if (gc.neighbourhood == 15 && gc.Top() != null && (gc.Top().neighbourhood == 7 || gc.Top().neighbourhood == 6))
                        gc.TileID = 20;
                     if (gc.neighbourhood == 15 && gc.Top() != null && gc.Top().neighbourhood == 13)
                        gc.TileID = 18;
                     if (gc.neighbourhood == 15 && gc.Top() != null && gc.Top().neighbourhood == 12)
                        gc.TileID = 16;
                     if (gc.neighbourhood == 15 && gc.Top() != null && gc.Top().neighbourhood == 5)
                        gc.TileID = 5;
                     if (gc.neighbourhood == 15 && gc.Right() != null && (gc.Right().neighbourhood == 11 || gc.Right().neighbourhood == 9))
                        gc.TileID = 6;
                     if (gc.neighbourhood == 15 && gc.Left() != null && gc.Left().neighbourhood == 11)
                        gc.TileID = 8;
                     if (gc.neighbourhood == 15 && gc.Left() != null && gc.Left().neighbourhood == 3)
                         gc.TileID = 14;
                     if (gc.neighbourhood == 13 && gc.Left() != null && gc.Left().neighbourhood == 11)
                        gc.TileID = 15;
                     if (gc.neighbourhood == 13 && gc.Left() != null && gc.Left().neighbourhood == 14)
                        gc.TileID = 28;
                     if (gc.neighbourhood == 7 && gc.Right() != null && gc.Right().neighbourhood == 9)
                        gc.TileID = 16;
                     if (gc.neighbourhood == 7 && gc.Right() != null && gc.Right().neighbourhood == 14)
                        gc.TileID = 28;
                     if (gc.neighbourhood == 14 && gc.Bottom() != null && gc.Bottom().neighbourhood == 13)
                        gc.TileID = 17;
                     if (gc.neighbourhood == 15 && gc.Right() != null && gc.Right().neighbourhood == 10)
                        gc.TileID = 24;
                     if (gc.neighbourhood == 15 && gc.Left() != null && gc.Left().neighbourhood == 10)
                         gc.TileID = 5;
                     if (gc.neighbourhood == 13 && gc.Top() != null && gc.Top().neighbourhood == 5)
                        gc.TileID = 27;
                     if (gc.neighbourhood == 5 && gc.Top() != null && gc.Top().neighbourhood == 15)
                        gc.TileID = 15;
                
                    // Doors
                    GridCell gcLeft = gc.Left();
                    GridCell gcRight = gc.Right();
                    if (gc.neighbourhood == 26 || gc.neighbourhood == 23 || gc.neighbourhood == 32 ||
                        gc.neighbourhood == 29 || gc.neighbourhood == 24 || gc.neighbourhood == 21 ||
                        gc.neighbourhood == 22 || gc.neighbourhood == 28)
                    {
                        int wallNeighbours = 0;
                        if ((gc.Top() != null && gc.Top().neighbourhood < 20))
                            wallNeighbours++;
                        if ((gc.Bottom() != null && gc.Bottom().neighbourhood < 20))
                            wallNeighbours++;

                        if ((gc.Left() != null && gc.Left().neighbourhood < 20))
                            wallNeighbours++;
                        if (gc.Right() != null && gc.Right().neighbourhood < 20)
                            wallNeighbours++;

                        if (wallNeighbours > 1)
                            continue;

                        //if (gc.Bottom() != null && gc.Bottom().neighbourhood == 

                        map.Layers[1].Grid[y, x] = new GridCell(DungeonTileInterpreter[dungeonType].doorTile, 5, 0, map);
                        map.Layers[1].Grid[y, x].Visisted = 1f;
                        map.Layers[1].Grid[y, x].Y = y;
                        map.Layers[1].Grid[y, x].X = x;
                        doors.Add(map.Layers[1].Grid[y, x]);
                    }

                    if ((gc.neighbourhood == 27 && gc.Left() != null && DungeonTileInterpreter[dungeonType].floorTiles.Contains(gc.Left().TileID))
                        || (gc.neighbourhood == 31 && gc.Bottom() != null && DungeonTileInterpreter[dungeonType].floorTiles.Contains(gc.Bottom().TileID) && (gc.Left() != null && gc.Left().neighbourhood != 31) && (gc.Right() != null && gc.Right().neighbourhood != 31))
                        )
                    {
                        // 31 -> bottom floor
                        map.Layers[1].Grid[y, x] = new GridCell(DungeonTileInterpreter[dungeonType].doorTile, 5, 0, map);
                        map.Layers[1].Grid[y, x].Visisted = 1f;
                        map.Layers[1].Grid[y, x].Y = y;
                        map.Layers[1].Grid[y, x].X = x;
                        doors.Add(map.Layers[1].Grid[y, x]);
                    }

                }

            foreach (GridCell gc in doors)
            {
                int n = 0;
                for (int y = gc.Y - 1; y <= gc.Y + 1; y++)
                    for (int x = gc.X - 1; x <= gc.X + 1; x++)
                    {
                        if (!map.IsInsideMapBoundaries(new Point(x, y)))
                            continue;
                        if (gc == map.Layers[1].Grid[y, x])
                            continue;
                        if (doors.Contains(map.Layers[1].Grid[y, x]))
                            n++;

                    }

                if (n == 0)
                    gc.TileID = -1;
                if (n >= 3)
                    gc.TileID = -1;

                if (gc.TileID != -1)
                {
                    Door d = new Door(new Vector2(gc.X * Tile.TileWidth, gc.Y * Tile.TileHeight), map.stateManager.Game.Content.Load<Texture2D>(@"Items\door"), map);
                    map.MapItems.Add(d);
                    gc.TileID = -1;
                }
            }


           


        }

        public static void BSPGeneration(ref Map map)
        {

        }


        #endregion
    }
}
