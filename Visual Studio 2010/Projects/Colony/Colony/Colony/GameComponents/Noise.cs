﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using System.IO;

namespace Colony
{

    public static class Noise
    {
        public static float[,] GenerateWhiteNoise(int width, int height)
        {
            Random rnd = new Random();
            float[,] noise = new float[width, height];

            for(int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    noise[x, y] = (float)rnd.NextDouble() % 1;
                }
            }

            return noise;
        }

        public static float[,] GenerateSmoothNoise(float[,] baseNoise, int octave)
        {
            int width = baseNoise.GetLength(0);
            int height = baseNoise.GetLength(1);

            float[,] smoothNoise = new float[width, height];

            int samplePeriod = 1 << octave;
            float sampleFrequency = 1.0f / samplePeriod;

            for (int x = 0; x < width; x++)
            {
                int sample_x0 = (x / samplePeriod) * samplePeriod;
                int sample_x1 = (sample_x0 + samplePeriod) % width;

                float horizontal_blend = (x - sample_x0) * sampleFrequency;

                for (int y = 0; y < height; y++)
                {
                    int sample_y0 = (y / samplePeriod) * samplePeriod;
                    int sample_y1 = (sample_y0 + samplePeriod) % height;
                    float vertical_blend = (y - sample_y0) * sampleFrequency;

                    float top = MathHelper.Lerp(baseNoise[sample_x0, sample_y0],
                        baseNoise[sample_x1, sample_y0], horizontal_blend);

                    float bottom = MathHelper.Lerp(baseNoise[sample_x0, sample_y1],
                        baseNoise[sample_x1, sample_y1], horizontal_blend);

                    smoothNoise[x, y] = MathHelper.Lerp(top, bottom, vertical_blend);
                }
            }


            return smoothNoise;
        }

        public static float[,] GeneratePerlinNoise(float[,] baseNoise, int octaveCount)
        {
            int width = baseNoise.GetLength(0);
            int height = baseNoise.GetLength(1);

            List<float[,]> smoothNoise = new List<float[,]>();

            float persistance = 0.5f;

            for (int i = 0; i < octaveCount; i++)
            {

                float[,] n = new float[width, height];
                smoothNoise.Add(n);
                smoothNoise[i] = GenerateSmoothNoise(baseNoise, i);
            }

            float[,] perlinNoise = new float[width, height];
            float amplitude = 1.0f;
            float totalAmplitude = 0.0f;

            for (int octave = octaveCount - 1; octave >= 0; octave--)
            {
                amplitude *= persistance;
                totalAmplitude += amplitude;
                
                for(int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                    {
                        perlinNoise[x, y] += smoothNoise[octave][x, y] * amplitude;
                    }

                for (int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                        perlinNoise[x, y] /= totalAmplitude;
            }

            return perlinNoise;
        }
    }
}
