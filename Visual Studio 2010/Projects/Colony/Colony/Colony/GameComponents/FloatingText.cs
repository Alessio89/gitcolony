﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;


namespace Colony.Gameplay_Elements
{
    public static class FloatingText
    {

        public static Map Map;
        public static SpriteFont Font;
        public static List<FloatingTextObject> FloatingTextList;

        public static void Initialize()
        {
            FloatingTextList = new List<FloatingTextObject>();
        }

        // AddFloatingText methods & Overrides on a fixed position
        public static void AddFloatingText(string Text, Vector2 Position)
        {
            FloatingTextList.Add(new FloatingTextObject() { text = Text, color = Color.White, position = Position });
        }

        public static void AddFloatingText(string Text, Vector2 Position, Color Color)
        {
            FloatingTextList.Add(new FloatingTextObject() { text = Text, color = Color, position = Position });
        }

        public static void AddFloatingText(string Text, Vector2 Position, Color Color, float Speed, float FadingSpeed)
        {
            FloatingTextList.Add(new FloatingTextObject() { text = Text, color = Color, position = Position, speed = Speed, fadingSpeed = FadingSpeed });
        }

        // AddFloatingText methods & Overrides on a moving source
        public static void AddFloatingText(string Text, BaseCharacter Source)
        {
            FloatingTextList.Add(new FloatingTextObject() { text = Text, color = Color.White, position = Source.Position, source = Source });
        }
        public static void AddFloatingText(string Text, BaseCharacter Source, Action end)
        {
            FloatingTextList.Add(new FloatingTextObject() { text = Text, color = Color.White, position = Source.Position, source = Source, OnTextVanish = end });
        }
        public static void AddFloatingText(string Text, BaseCharacter Source, Color Color)
        {
            FloatingTextList.Add(new FloatingTextObject() { text = Text, color = Color, position = Source.Position, source = Source });
        }

        public static void AddFloatingText(string Text, BaseCharacter Source, Color Color, float Speed, float FadingSpeed)
        {
            FloatingTextList.Add(new FloatingTextObject() { text = Text, color = Color, position = Source.Position, source = Source, speed = Speed, fadingSpeed = FadingSpeed });
        }

        public static void Update(GameTime gameTime)
        {
            for (int i = 0; i < FloatingTextList.Count; i++)
            {
                if (FloatingTextList[i].FlaggedForDelete)
                {
                    FloatingTextList.Remove(FloatingTextList[i]);
                    i--;
                    continue;
                }
                else
                    FloatingTextList[i].Update(gameTime);
            }

            foreach (FloatingTextObject t in FloatingTextList)
                t.Update(gameTime);
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            foreach (FloatingTextObject t in FloatingTextList)
                t.Draw(spriteBatch);
        }

    }

    public class FloatingTextObject
    {
        public string text;
        public Vector2 mposition;
        public Vector2 position
        {
            get { return mposition; }
            set
            { 
            mposition = value; 
            originalPosition = value; } }

        public Color color;
        public BaseCharacter source;
        protected Vector2 originalPosition;

        public float speed = 0.05f;
        public float fadingSpeed = 0.001f;

        public bool FlaggedForDelete;

        bool Wobbling = false;
        public float offset;

        public Action OnTextVanish;

        public FloatingTextObject()
        {
            originalPosition = position;
            offset = (FloatingText.Map.rnd.NextDouble() > 0.5) ? -40 : +40;
        }

        public void Update(GameTime gameTime)
        {
            mposition.Y -= speed * (float)gameTime.ElapsedGameTime.Milliseconds;
            if (source != null)
            {
                
                //mposition.X = source.Position.X;
                originalPosition.X = source.Position.X;
            }

            if (!Wobbling)
            {
                
                mposition.X = MathHelper.Lerp(mposition.X, originalPosition.X+ offset, 0.1f);
            }

            color = Color.Lerp(color, Color.White * 0f, fadingSpeed * (float)gameTime.ElapsedGameTime.Milliseconds);
            if (color.A <= 2f)
            {
                FlaggedForDelete = true;
                if (OnTextVanish != null)
                    OnTextVanish();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(FloatingText.Font, text, position, color);
        }
    }

    public static class Easing
    {
        // Adapted from source : http://www.robertpenner.com/easing/

        public static float Ease(double linearStep, float acceleration, EasingType type)
        {
            float easedStep = acceleration > 0 ? EaseIn(linearStep, type) :
                              acceleration < 0 ? EaseOut(linearStep, type) :
                              (float)linearStep;

            return CMathHelper.Lerp(linearStep, easedStep, Math.Abs(acceleration));
        }

        public static float EaseIn(double linearStep, EasingType type)
        {
            switch (type)
            {
                case EasingType.Step: return linearStep < 0.5 ? 0 : 1;
                case EasingType.Linear: return (float)linearStep;
                case EasingType.Sine: return Sine.EaseIn(linearStep);
                case EasingType.Quadratic: return Power.EaseIn(linearStep, 2);
                case EasingType.Cubic: return Power.EaseIn(linearStep, 3);
                case EasingType.Quartic: return Power.EaseIn(linearStep, 4);
                case EasingType.Quintic: return Power.EaseIn(linearStep, 5);
            }
            throw new NotImplementedException();
        }

        public static float EaseOut(double linearStep, EasingType type)
        {
            switch (type)
            {
                case EasingType.Step: return linearStep < 0.5 ? 0 : 1;
                case EasingType.Linear: return (float)linearStep;
                case EasingType.Sine: return Sine.EaseOut(linearStep);
                case EasingType.Quadratic: return Power.EaseOut(linearStep, 2);
                case EasingType.Cubic: return Power.EaseOut(linearStep, 3);
                case EasingType.Quartic: return Power.EaseOut(linearStep, 4);
                case EasingType.Quintic: return Power.EaseOut(linearStep, 5);
            }
            throw new NotImplementedException();
        }

        public static float EaseInOut(double linearStep, EasingType easeInType, EasingType easeOutType)
        {
            return linearStep < 0.5 ? EaseInOut(linearStep, easeInType) : EaseInOut(linearStep, easeOutType);
        }
        public static float EaseInOut(double linearStep, EasingType type)
        {
            switch (type)
            {
                case EasingType.Step: return linearStep < 0.5 ? 0 : 1;
                case EasingType.Linear: return (float)linearStep;
                case EasingType.Sine: return Sine.EaseInOut(linearStep);
                case EasingType.Quadratic: return Power.EaseInOut(linearStep, 2);
                case EasingType.Cubic: return Power.EaseInOut(linearStep, 3);
                case EasingType.Quartic: return Power.EaseInOut(linearStep, 4);
                case EasingType.Quintic: return Power.EaseInOut(linearStep, 5);
            }
            throw new NotImplementedException();
        }

        static class Sine
        {
            public static float EaseIn(double s)
            {
                return (float)Math.Sin(s * CMathHelper.HalfPi - CMathHelper.HalfPi) + 1;
            }
            public static float EaseOut(double s)
            {
                return (float)Math.Sin(s * CMathHelper.HalfPi);
            }
            public static float EaseInOut(double s)
            {
                return (float)(Math.Sin(s * CMathHelper.Pi - CMathHelper.HalfPi) + 1) / 2;
            }
        }

        static class Power
        {
            public static float EaseIn(double s, int power)
            {
                return (float)Math.Pow(s, power);
            }
            public static float EaseOut(double s, int power)
            {
                var sign = power % 2 == 0 ? -1 : 1;
                return (float)(sign * (Math.Pow(s - 1, power) + sign));
            }
            public static float EaseInOut(double s, int power)
            {
                s *= 2;
                if (s < 1) return EaseIn(s, power) / 2;
                var sign = power % 2 == 0 ? -1 : 1;
                return (float)(sign / 2.0 * (Math.Pow(s - 2, power) + sign * 2));
            }
        }
    }

    public enum EasingType
    {
        Step,
        Linear,
        Sine,
        Quadratic,
        Cubic,
        Quartic,
        Quintic
    }

    public static class CMathHelper
    {
        public const float Pi = (float)Math.PI;
        public const float HalfPi = (float)(Math.PI / 2);

        public static float Lerp(double from, double to, double step)
        {
            return (float)((to - from) * step + from);
        }
    }
}
