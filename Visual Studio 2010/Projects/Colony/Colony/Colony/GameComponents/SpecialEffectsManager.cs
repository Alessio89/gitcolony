﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Particle_System;

namespace Colony.Gameplay_Elements
{
    public static class SfxManager
    {

        #region Field Region

        public static ParticleSystem ParticleSystem;

        #endregion

        #region Methods Region

        public static void Initialize(Map map)
        {
            ParticleSystem = new ParticleSystem(Vector2.Zero, map);
        }

        public static void Update(GameTime gameTime)
        {
            for (int i = 0; i < ParticleSystem.EmitterList.Count; i++)
            {
                if (ParticleSystem.EmitterList[i].FlaggedForRemoval)
                {
                    ParticleSystem.EmitterList.Remove(ParticleSystem.EmitterList[i]);
                    i--;
                }
            }
            ParticleSystem.Update(gameTime);
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            ParticleSystem.Draw(spriteBatch, 1, Vector2.Zero);
        }

        // Cone Method & Overrides
        public static void Cone(Vector2 position, Vector2 direction, Color color1, Color color2, float opening, int intensity, bool damaging = false)
        {
            Vector2 angle = new Vector2(MathHelper.Pi * opening, -MathHelper.Pi * opening);

            ParticleSystem.AddEmitter(
                new Vector2(0.01f, 0.015f),
                direction, angle,
                new Vector2(0.5f, 1f),
                new Vector2(30, 40),
                new Vector2(10f, 20f),
                color1, color2, Color.Black, Color.Black,
                new Vector2(130, 140), new Vector2(130, 140),
                intensity,
                position,
                ParticleSystem.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Images\particle")
                );
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Damaging = damaging;
        }

        public static void Cone(Projectile proj, Color color1, Color color2, float opening, int intensity, bool LimitedDur = true, float duration = 2f, bool damaging = false)
        {
            Vector2 direction = proj.Direction;
            Vector2 angle = new Vector2(MathHelper.Pi * opening, -MathHelper.Pi * opening);

            ParticleSystem.AddEmitter(
                 new Vector2(0.01f, 0.015f),
                 direction, angle,
                 new Vector2(0.5f, 1f),
                 new Vector2(30, 40),
                 new Vector2(10f, 20f),
                 color1, color2, Color.Black, Color.Black,
                 new Vector2(130, 140), new Vector2(130, 140),
                 intensity,
                 proj.Position,
                 ParticleSystem.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Images\particle")
                 );

            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].ProjectileSource = proj;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].FollowSource = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].ConeEffect = true;
            //ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].SourceOffset = new Vector2(source.FrameWidth / 2, source.FrameHeight / 2);

            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].LimitedDuration = LimitedDur;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Duration = duration;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Damaging = damaging;
        }

        public static void Cone(BaseCharacter source, Color color1, Color color2, float opening, int intensity, bool LimitedDur = true, float duration = 2f, string texture = @"Images\particle", bool damaging = false)
        {
            Vector2 direction = Vector2.Zero;
            Vector2 angle = new Vector2(MathHelper.Pi * opening, -MathHelper.Pi * opening);
            switch (source.CurrentAnimation)
            {
                case 0: direction = new Vector2(0, 1); break;
                case 1: direction = new Vector2(-1, 0); break;
                case 2: direction = new Vector2(1, 0); break;
                case 3: direction = new Vector2(0, -1); break;
            }

            ParticleSystem.AddEmitter(
                 new Vector2(0.01f, 0.015f),
                 direction, angle,
                 new Vector2(0.5f, 1f),
                 new Vector2(30, 40),
                 new Vector2(10f, 20f),
                 color1, color2, Color.Black, Color.Black,
                 new Vector2(130, 140), new Vector2(130, 140),
                 intensity,
                 source.Position,
                 ParticleSystem.Map.stateManager.GameRef.Content.Load<Texture2D>(texture)
                 );

            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Source = source;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].FollowSource = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].ConeEffect = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].SourceOffset = new Vector2(source.FrameWidth / 2, source.FrameHeight / 2);

            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].LimitedDuration = LimitedDur;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Duration = duration;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Damaging = damaging;
        }

        public static void Cone(Vector2 position, Vector2 Direction, BaseCharacter source,
            Color color1, Color color2,
            float opening,
            int intensity,
            Vector2 startVelocity, Vector2 endVelocity,
            Vector2 life,
            Vector2 startSize, Vector2 endSize,
            bool followSource, bool coneEffect, Vector2 sourceOffset, bool damaging = false)
        {
            Vector2 direction = Direction;
            Vector2 angle = new Vector2(MathHelper.Pi * opening, -MathHelper.Pi * opening);
            Vector2 pos = position;
            if (source != null)
            {
                switch (source.CurrentAnimation)
                {
                    case 0: direction = new Vector2(0, 1); break;
                    case 1: direction = new Vector2(-1, 0); break;
                    case 2: direction = new Vector2(1, 0); break;
                    case 3: direction = new Vector2(0, -1); break;
                }
                pos = source.Position;
            }



            ParticleSystem.AddEmitter(
                 new Vector2(0.01f, 0.015f),
                 direction, angle,
                 new Vector2(0.5f, 1f),
                 new Vector2(30, 40),
                 new Vector2(10f, 20f),
                 color1, color2, Color.Black, Color.Black,
                 new Vector2(130, 140), new Vector2(130, 140),
                 intensity,
                 pos,
                 ParticleSystem.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Images\particle")
                 );
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Damaging = damaging;
            if (followSource)
            {
                ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Source = source;
                ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].FollowSource = true;
                ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].ConeEffect = coneEffect;
                ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].SourceOffset = sourceOffset;
            }


        }
        public static void Explosion(Vector2 position, Color color1, Color color2, int intensity, float power, bool damaging = false)
        {
            Vector2 angle = new Vector2(MathHelper.Pi, -MathHelper.Pi);

            ParticleSystem.AddEmitter(
                new Vector2(0.01f, 0.015f),
                new Vector2(1, 0), angle,
                new Vector2(0.5f, 1f),
                new Vector2(30, 40),
                new Vector2(10f, 20f),
                color1, color2, Color.Black, Color.Black,
                new Vector2(130, 140) * power, new Vector2(130, 140) * power,
                intensity,
                position,
                ParticleSystem.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Images\particle")
                );
            // ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Source = source;
            // ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].RelPosition = source.Position + new Vector2(source.FrameWidth / 2, source.FrameHeight / 2);
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].SpawnAllAtOnce = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].DeleteAfterSpawn = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Damaging = damaging;
        }
        public static void Explosion(BaseCharacter source, Color color1, Color color2, int intensity, float power, bool followSource = false, bool damaging = false)
        {
            Vector2 angle = new Vector2(MathHelper.Pi, -MathHelper.Pi);

            ParticleSystem.AddEmitter(
                new Vector2(0.01f, 0.015f),
                new Vector2(1, 0), angle,
                new Vector2(0.5f, 1f),
                new Vector2(30, 40),
                new Vector2(10f, 20f),
                color1, color2, Color.Black, Color.Black,
                new Vector2(130, 140) * power, new Vector2(130, 140) * power,
                intensity,
                source.Position,
                ParticleSystem.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Images\particle")
                );
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Source = source;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].FollowSource = followSource;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].RelPosition = source.Position + new Vector2(source.FrameWidth / 2, source.FrameHeight / 2);
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].SpawnAllAtOnce = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].DeleteAfterSpawn = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Damaging = damaging;
        }

        public static void OverHeadAnimation(BaseCharacter source, Texture2D image, bool damaging = false)
        {
            ParticleSystem.AddEmitter(
                            new Vector2(0.01f, 0.015f),
                            new Vector2(0, -1), Vector2.Zero,
                            new Vector2(0.5f, 1f),
                            new Vector2(30, 40),
                            new Vector2(10f, 20f),
                            Color.White, Color.White, Color.Black, Color.Black,
                            new Vector2(130, 140), new Vector2(130, 140),
                            1,
                            source.Position,
                            image
                            );
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Source = source;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].FollowSource = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].RelPosition = source.Position + new Vector2(source.FrameWidth / 2, source.FrameHeight / 2);
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].SpawnAllAtOnce = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].DeleteAfterSpawn = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Damaging = damaging;
        }

        public static void TestEffect(BaseCharacter source, Color color1, Color color2,int intensity, float power, bool damaging = false)
        {
            Vector2 angle = new Vector2(MathHelper.Pi, MathHelper.Pi);

            ParticleSystem.AddEmitter(
                new Vector2(0.01f, 0.015f),
                new Vector2(0.1f, 0), angle,
                new Vector2(0.5f, 1f),
                new Vector2(30, 40),
                new Vector2(10f, 20f),
                color1, color2, Color.Black, Color.Black,
                new Vector2(130, 140) * power, new Vector2(130, 140) * power,
                intensity,
                source.Position,
                ParticleSystem.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Images\particle")
                );
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Source = source;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].FollowSource = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].RelPosition = source.Position + new Vector2(source.FrameWidth / 2, source.FrameHeight / 2);
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].SpawnAllAtOnce = false;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].DeleteAfterSpawn = true;
            ParticleSystem.EmitterList[ParticleSystem.EmitterList.Count - 1].Damaging = damaging;
        }
        #endregion

    }
}
