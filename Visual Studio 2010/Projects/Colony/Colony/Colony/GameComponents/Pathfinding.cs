﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using System.Diagnostics;
using System.Threading;

namespace Colony.Gameplay_Elements
{
    public static class Pathfinding
    {

        #region Field Region

        public static Node startingNode;
        public static Node endingNode;
        public static Node currentNode;
        public static List<Node> Path = new List<Node>();
        public static List<Node> OpenList = new List<Node>();
        public static List<Node> ClosedList = new List<Node>();
        public static List<Node> AdjacentNodes = new List<Node>();
        public static List<Node> SolidNodes = new List<Node>();
        public static bool Debug = false;
        public static int mapWidth = 10;
        public static int mapHeight = 10;
        public static Map map;
        #endregion

        #region Methods Region

        public static void ResetFinder()
        {
            startingNode = null;
            endingNode = null;
            Path = new List<Node>();
            OpenList = new List<Node>();
            ClosedList = new List<Node>();
            currentNode = null;
            AdjacentNodes = new List<Node>();
            
        }

        public static void FindPath(Node startingPoint, Node endingPoint)
        {
            Stopwatch sw = Stopwatch.StartNew();
            ResetFinder();

            startingNode = startingPoint;
            endingNode = endingPoint;
            startingNode.Initialize();
            if (Debug)
                Console.WriteLine("Inizio: {0} {1} Arrivo : {2} {3}", startingNode.X, startingNode.Y, endingNode.X, endingNode.Y);

            // 1) Add the starting square (or node) to the open list.
            OpenList.Add(startingNode);

            // 2) Repeat the following:
            int i = 0;
            while (true)
            {
                if (sw.ElapsedMilliseconds > 0.2f)
                    break;
                i++;
                // a) Look for the lowest F cost square on the open list. We refer to this as the current square.
                if ((from n in OpenList orderby n.Score ascending select n).Count() <= 0)
                {
                    if (Debug)
                        Console.WriteLine("Impossibile trovare un tragitto!");
                    break;
                }
                currentNode = (from n in OpenList orderby n.Score ascending select n).First();
                currentNode.Prop = i.ToString();
                if (Debug)
                    Console.WriteLine("Selezionato il nodo {0}, {1} con Score {2}", currentNode.X, currentNode.Y, currentNode.Score);
                Path.Add(currentNode);
                if (Debug)
                    Console.ReadLine();
                // b) Switch it to the closed list.
                OpenList.Remove(currentNode);
                ClosedList.Add(currentNode);

                // c) For each of the 8 squares adjacent to this current square …
                for (int y = currentNode.Y - 1; y <= currentNode.Y + 1; y++)
                {
                    for (int x = currentNode.X - 1; x <= currentNode.X + 1; x++)
                    {

                        Node neighbour = new Node(x, y, TileType.None);
                        neighbour.Initialize();
                        neighbour.CalculateEnergy(currentNode);
                        // If it is not walkable or if it is on the closed list, ignore it.

                        // Not Walkable tiles
                        bool passable = true;
                        foreach (Node n in SolidNodes)
                        {
                            if (x == n.X && y == n.Y)
                            {
                                if (Debug)
                                    Console.WriteLine("Tile impassabile.");
                                passable = false;
                            }
                        }
                        
                        if (!passable)
                            continue;
                        if (x < 0 || x >= mapWidth || y < 0 || y >= mapHeight)
                            continue;
                        if (y == currentNode.Y && x == currentNode.X)
                        {
                            if (Debug)
                            {
                                Console.WriteLine("Ignoro, nodo corrente");
                                Console.ReadLine();
                            }
                            continue;
                        }
                        if (neighbour.IsInClosed())
                        {
                            if (Debug)
                            {
                                Console.WriteLine("Ignoro, nodo in closed list");
                                Console.ReadLine();
                            }
                            continue;
                        }

                        // If it isn’t on the open list, add it to the open list. Make the current square the parent of this square. Record the F, G, and H costs of the square. 

                        if (!neighbour.IsInOpen())
                        {
                            if (Debug)
                            {
                                Console.WriteLine("Non presente in open list, aggiungo. {0}, {1}", neighbour.X, neighbour.Y);
                                Console.ReadLine();
                            }
                            OpenList.Add(neighbour);
                            neighbour.Parent = currentNode;
                            neighbour.Initialize();
                            neighbour.CalculateEnergy(currentNode);

                        }

                        // If it is on the open list already, check to see if this path to that square is better, using G cost as the measure. A lower G cost means that this is a better path. If so, change the parent of the square to the current square, and recalculate the G and F scores of the square. If you are keeping your open list sorted by F score, you may need to resort the list to account for the change.

                        else
                        {
                            if (Debug)
                            {
                                Console.WriteLine("Presente in open list, controllo tragitto.");
                                Console.ReadLine();
                            }
                            if (neighbour.MovementEergy < currentNode.MovementEergy)
                            {
                                if (Debug)
                                {
                                    Console.WriteLine("Tragitto migliore trovato.");
                                    Console.ReadLine();
                                }
                                neighbour.Parent = currentNode;
                                neighbour.Initialize();


                            }
                        }

                        // d) Stop when you:

                        //Add the target square to the closed list, in which case the path has been found (see note below)

                    }
                }
                if (endingNode.IsInClosed())
                {
                    endingNode.Parent = Path[Path.Count - 1];
                    if (Debug)
                    {

                        Console.WriteLine("Arrivato");
                        foreach (Node n in ReconstructPath())
                        {
                            Console.WriteLine("{0}, {1}", n.X, n.Y);
                        }
                    }
                    break;
                }
                // if (i >= 140)
                //{
                //  endingNode.Parent = Path[Path.Count - 1];
                // Console.WriteLine("Mi arrendo...");
                //break;
                // }

            }
            sw.Stop();
            Console.WriteLine("Impiegati {0} ms per trovare il tragitto", sw.ElapsedMilliseconds);


        }
        public static void FindPathNoDiagonals(Node startingPoint, Node endingPoint, bool IgnoreSolids, GenericEnemy actor = null, List<Node> nodesToAvoid = null)
        {
            Stopwatch sw = Stopwatch.StartNew();
            ResetFinder();

            startingNode = startingPoint;
            endingNode = endingPoint;
            startingNode.Initialize();
            
            if (Debug)
                Console.WriteLine("Inizio: {0} {1} Arrivo : {2} {3}", startingNode.X, startingNode.Y, endingNode.X, endingNode.Y);

            // 1) Add the starting square (or node) to the open list.
            OpenList.Add(startingNode);

            // 2) Repeat the following:
            int i = 0;
            while (true)
            {
               
                i++;
                // a) Look for the lowest F cost square on the open list. We refer to this as the current square.
                if ((from n in OpenList orderby n.Score ascending select n).Count() <= 0)
                {
                    if (Debug)
                        Console.WriteLine("Impossibile trovare un tragitto!");
                    break;
                }
                currentNode = (from n in OpenList orderby n.Score ascending select n).First();
                currentNode.Prop = i.ToString();
                if (Debug)
                    Console.WriteLine("Selezionato il nodo {0}, {1} con Score {2}", currentNode.X, currentNode.Y, currentNode.Score);
                Path.Add(currentNode);
                if (Debug)
                    Console.ReadLine();
                // b) Switch it to the closed list.
                OpenList.Remove(currentNode);
                ClosedList.Add(currentNode);

                // c) For each of the 4 squares adjacent to this current square …
                for (int j = 0; j < 4; j++)
                {
                    Node neighbour = new Node(0, 0, TileType.None);
                    switch (j)
                    {
                        case 0: neighbour = new Node(currentNode.X - 1, currentNode.Y, TileType.None); break;
                        case 1: neighbour = new Node(currentNode.X + 1, currentNode.Y, TileType.None); break;
                        case 2: neighbour = new Node(currentNode.X, currentNode.Y + 1, TileType.None); break;
                        case 3: neighbour = new Node(currentNode.X, currentNode.Y - 1, TileType.None); break;
                    }

                    neighbour.Initialize();
                    neighbour.CalculateEnergy(currentNode);
                    // If it is not walkable or if it is on the closed list, ignore it.

                    // Not Walkable tiles
                    bool passable = true;
                    if (!map.IsInsideMapBoundaries( new Point(neighbour.Y, neighbour.X)))
                        continue;
                     if (map.Layers[0].Grid[neighbour.Y, neighbour.X].Event == 1)
                        passable = false;
                    if (nodesToAvoid != null && (from n in nodesToAvoid where n.X == neighbour.X && n.Y == neighbour.Y select n).Count() > 0)
                        passable = false;
                    foreach (GenericEnemy e in map.Enemies)
                    {
                        if (actor != null && e == actor)
                            break;
                        if (e.GetCenterXOnGrid() == neighbour.X && e.GetCenterYOnGrid() == neighbour.Y)
                        {
                            passable = false;
                            break;
                        }
                    }

                    if (IgnoreSolids)
                        passable = true;
                    if (!passable)
                        continue;
                    if (neighbour.X < 0 || neighbour.X >= mapWidth || neighbour.Y < 0 || neighbour.Y >= mapHeight)
                        continue;
                    if (neighbour.IsInClosed())
                    {
                        if (Debug)
                        {
                            Console.WriteLine("Ignoro, nodo in closed list");
                            Console.ReadLine();
                        }
                        continue;
                    }

                    // If it isn’t on the open list, add it to the open list. Make the current square the parent of this square. Record the F, G, and H costs of the square. 

                    if (!neighbour.IsInOpen())
                    {
                        if (Debug)
                        {
                            Console.WriteLine("Non presente in open list, aggiungo. {0}, {1}", neighbour.X, neighbour.Y);
                            Console.ReadLine();
                        }
                        OpenList.Add(neighbour);
                        neighbour.Parent = currentNode;
                        neighbour.Initialize();
                        neighbour.CalculateEnergy(currentNode);

                    }

                    // If it is on the open list already, check to see if this path to that square is better, using G cost as the measure. A lower G cost means that this is a better path. If so, change the parent of the square to the current square, and recalculate the G and F scores of the square. If you are keeping your open list sorted by F score, you may need to resort the list to account for the change.

                    else
                    {
                        if (Debug)
                        {
                            Console.WriteLine("Presente in open list, controllo tragitto.");
                            Console.ReadLine();
                        }
                        if (neighbour.MovementEergy < currentNode.MovementEergy)
                        {
                            if (Debug)
                            {
                                Console.WriteLine("Tragitto migliore trovato.");
                                Console.ReadLine();
                            }
                            neighbour.Parent = currentNode;
                            neighbour.Initialize();


                        }


                        // d) Stop when you:

                        //Add the target square to the closed list, in which case the path has been found (see note below)

                    }
                }
                if (endingNode.IsInClosed())
                {
                    endingNode.Parent = Path[Path.Count - 1];
                    if (Debug)
                    {

                        Console.WriteLine("Arrivato");
                        foreach (Node n in ReconstructPath())
                        {
                            Console.WriteLine("{0}, {1}", n.X, n.Y);
                        }
                    }
                    break;
                }
                if (i >= 140)
                {
                    endingNode.Parent = Path[Path.Count - 1];
                    if (Debug)
                        Console.WriteLine("Mi arrendo...");
                    break;
                }

            }
            sw.Stop();
            //FloatingText.AddFloatingText(sw.ElapsedMilliseconds.ToString(), new Vector2(endingNode.X * Tile.TileWidth, endingNode.Y * Tile.TileHeight));
            if (Debug)
                Console.WriteLine("Impiegati {0} ms per trovare il tragitto", sw.ElapsedMilliseconds);


        }

        public static List<Node> ReconstructPath()
        {
            List<Node> returnPath = new List<Node>();
            Node tempNode = endingNode;
            //returnPath.Add(tempNode);
            if (endingNode.Parent == null)
                return returnPath;
            if (Vector2.Distance(new Vector2(endingNode.X, endingNode.Y), new Vector2(endingNode.Parent.X, endingNode.Parent.Y)) > 40f)
                return returnPath;
            while (tempNode.Parent != null)
            {
                returnPath.Add(tempNode.Parent);
                tempNode = tempNode.Parent;
            }
            return returnPath;
        }

        public static void PlaceSolid(int x, int y)
        {
            SolidNodes.Add(new Node(x, y, TileType.Solid));
        }
        #endregion


    }

    public class Node
    {
        public int X;
        public int Y;
        public TileType Type;
        public int Heuristic;
        public int MovementEergy;
        public int Score;
        public Node Parent;
        public string Prop = "";
        public bool Occupied = false;

        public Node(int x, int y, TileType type, Node parent)
        {
            X = x;
            Y = y;
            Type = type;
            Parent = parent;
        }
        public Node(int x, int y, TileType type)
        {
            X = x;
            Y = y;
            Type = type;

        }

        public void Initialize()
        {
            // Calcola energia di movimento: Diagonale 14, Orizzontale e verticale 10
            if (Parent != null)
            {
                Vector2 direction = new Vector2(X, Y) - new Vector2(Parent.X, Parent.Y);
                direction.Normalize();
                if (direction.X == direction.Y)
                {
                    MovementEergy = 14;
                }
                else
                    MovementEergy = 10;

                
            }
            // Console.WriteLine("Movement Energy: {0}", MovementEergy);

            // Calcola euristica
            if (Pathfinding.endingNode == null)
            {
                //    Console.WriteLine("Impossibile inizializzare, Ending Node non definito");
                return;
            }
            Heuristic = Math.Abs(Pathfinding.endingNode.X - X) + Math.Abs(Pathfinding.endingNode.Y - Y);
            // Console.WriteLine("Heuristic: {0}", Heuristic);

            // Calcola punteggio
            Score = MovementEergy + Heuristic;
            // Console.WriteLine("Score: {0}", Score);

        }

        public bool IsTheSameAs(Node node)
        {

            return (X == node.X && Y == node.Y);
        }

        public bool IsInClosed()
        {
            foreach (Node n in Pathfinding.ClosedList)
            {
                if (n.X == X && n.Y == Y)
                    return true;
            }
            return false;

        }
        public bool IsInOpen()
        {
            foreach (Node n in Pathfinding.OpenList)
            {
                if (n.X == X && n.Y == Y)
                    return true;
            }
            return false;

        }

        public void CalculateEnergy(Node parentNode)
        {
            Vector2 direction = new Vector2(X, Y) - new Vector2(parentNode.X, parentNode.Y);
            direction.Normalize();
            if (direction.X == direction.Y)
            {
                MovementEergy = 14;
            }
            else
                MovementEergy = 10;

            //foreach (GenericEnemy e in Pathfinding.map.Enemies)
            //{
            //    if (e.CharacterGridX == X && e.CharacterGridY == Y)
            //    {
            //        MovementEergy += 10;
            //        break;
            //    }
            //}
        }
    }
}
