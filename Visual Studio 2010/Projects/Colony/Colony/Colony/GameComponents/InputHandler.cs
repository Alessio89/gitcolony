﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Colony.InputHandler
{
    public class InputHandler : GameComponent
    {
        #region Field Region

        public KeyboardState oldState;
        MouseState oldMouseState;
        bool IsKeyDownOnce = false;

        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public InputHandler(Game game)
            : base(game)
        {
            oldState = Keyboard.GetState();
            oldMouseState = Mouse.GetState();
        }

        #endregion

        #region Method Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            oldState = Keyboard.GetState();
            oldMouseState = Mouse.GetState();
            
        }

        public bool IsKeyPressed(Keys key )
        {
            return ( oldState.IsKeyDown(key) && !Keyboard.GetState().IsKeyDown(key) );
        }

        public bool IsKeyDown(Keys key)
        {
            return (Keyboard.GetState().IsKeyDown(key));
        }

        public bool LeftClick()
        {
            return (oldMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton != ButtonState.Pressed);
        }
        public bool RightClick()
        {
            return (oldMouseState.RightButton == ButtonState.Pressed && Mouse.GetState().RightButton != ButtonState.Pressed);
        }
        public bool LeftClickHold()
        {
            return (oldMouseState.LeftButton == ButtonState.Pressed && Mouse.GetState().LeftButton == ButtonState.Pressed);
        }

        public bool CheckKeyDownOnce(Keys key)
        {
            if (!IsKeyDownOnce)
            {
                IsKeyDownOnce = true;
                return (oldState.IsKeyDown(key) && !Keyboard.GetState().IsKeyDown(key));
            }
            return false;
        }

        #endregion
    }
}
