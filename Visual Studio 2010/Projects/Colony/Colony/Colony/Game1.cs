using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using X2DPE;
using X2DPE.Helpers;
using RamGecXNAControls;
using RamGecXNAControls.ExtendedControls;
using Microsoft.Xna.Framework.Storage;

namespace Colony
{
    
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        public GameStateManager StateManager;
        public InputHandler.InputHandler InputHandler;
        public ParticleComponent particleComponent;
        public bool NewMapLoaded = false;
        public GUIManager GuiManager;

        public Game1()
        {
            // Graphics
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            //graphics.IsFullScreen = true;
            graphics.PreferredBackBufferHeight = 600;
            graphics.PreferredBackBufferWidth = 800;

            // Components
            InputHandler = new InputHandler.InputHandler(this);
            StateManager = new GameStateManager(this, 800, 600);
            particleComponent = new ParticleComponent(this);
            particleComponent.Initialize();
            
            Components.Add(StateManager);
            Components.Add(InputHandler);
            Components.Add(particleComponent);
        }

      
        protected override void Initialize()
        {
            // The dungeon interpreter class
            PGCDungeon.Initialize();
            // Databases initialization
            EnemiesDB.Init();
            // Floating text class
            FloatingText.Initialize();
            base.Initialize();
        }

     
        protected override void LoadContent()
        {
            

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            List<Tileset> tilesets = new List<Tileset>() { new Tileset(Content, @"Tilesets\dungeontileset2") };
            Map m = new Map(new GridCell[40,40], StateManager, tilesets);
            Pathfinding.mapHeight = 40;
            Pathfinding.mapWidth = 40;
            Pathfinding.map = m;
            m.AddLayer();
            m.Layers[0].SetAsFloor(new GridCell(-1, 0, 0, m));
            FloatingText.Map = m;
            m.Camera.Scale = 0.1f;
            //StateManager.PushToStack(new GamePlayState(this, StateManager, m));
            //StateManager.PushToStack(new DebugConsole(this, StateManager));
            //StateManager.PushToStack(new MiniMap(m, this, StateManager));
            StateManager.PushToStack(new AlphaTestIntro(this, StateManager));
            foreach (BaseGameState state in StateManager.GameStates)
                state.LoadContents();

            GuiManager = new GUIManager(this, "Themes", "Default");

            
           
        }

     
        protected override void UnloadContent()
        {
            
        }

        protected override void Update(GameTime gameTime)
        {
         
            GuiManager.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            base.Draw(gameTime);
        }
    }
}
