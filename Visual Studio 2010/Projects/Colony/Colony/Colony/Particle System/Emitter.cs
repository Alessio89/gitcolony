﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;

namespace Colony.Particle_System
{
    public class Emitter
    {

        #region Field Region

        public Vector2 RelPosition;             // Position relative to collection.
        public int Budget;                      // Max number of alive particles.
        float NextSpawnIn;                      // This is a random number generated using the SecPerSpawn.
        float SecPassed;                        // Time pased since last spawn.
        public LinkedList<Particle> ActiveParticles;   // A list of all the active particles.
        public Texture2D ParticleSprite;        // This is what the particle looks like.
        public Random random;                   // Pointer to a random object passed trough constructor.

        public Vector2 SecPerSpawn;
        public Vector2 SpawnDirection;
        public Vector2 SpawnNoiseAngle;
        public Vector2 StartLife;
        public Vector2 StartScale;
        public Vector2 EndScale;
        public Color StartColor1;
        public Color StartColor2;
        public Color EndColor1;
        public Color EndColor2;
        public Vector2 StartSpeed;
        public Vector2 EndSpeed;

        public ParticleSystem Parent;

        public bool SpawnAllAtOnce;
        public bool FollowSource;
        public bool ConeEffect;
        public bool DeleteAfterSpawn;
        public bool LimitedDuration;

        public Projectile ProjectileSource;
        public BaseCharacter Source;
        public Vector2 SourceOffset = Vector2.Zero;
        int SpawnCounter;
        public float Duration = 2f;
        float Timer = 0f;

        public bool FlaggedForRemoval;

        public List<Rectangle> IntersectionRectangles = new List<Rectangle>();
        public bool Damaging;
        #endregion

        #region Constructor Region

        public Emitter(Vector2 SecPerSpawn, Vector2 SpawnDirection, Vector2 SpawnNoiseAngle,
     Vector2 StartLife, Vector2 StartScale, Vector2 EndScale, Color StartColor1,
     Color StartColor2, Color EndColor1, Color EndColor2, Vector2 StartSpeed,
     Vector2 EndSpeed, int Budget, Vector2 RelPosition, Texture2D ParticleSprite,
     Random random, ParticleSystem parent)
        {
            this.SecPerSpawn = SecPerSpawn;
            this.SpawnDirection = SpawnDirection;
            this.SpawnNoiseAngle = SpawnNoiseAngle;
            this.StartLife = StartLife;
            this.StartScale = StartScale;
            this.EndScale = EndScale;
            this.StartColor1 = StartColor1;
            this.StartColor2 = StartColor2;
            this.EndColor1 = EndColor1;
            this.EndColor2 = EndColor2;
            this.StartSpeed = StartSpeed;
            this.EndSpeed = EndSpeed;
            this.Budget = Budget;
            this.RelPosition = RelPosition;
            this.ParticleSprite = ParticleSprite;
            this.random = random;
            this.Parent = parent;
            ActiveParticles = new LinkedList<Particle>();
            this.NextSpawnIn = MathHelper.Lerp(SecPerSpawn.X, SecPerSpawn.Y, (float)random.NextDouble());
            this.SecPassed = 0.0f;
        }
        #endregion

        #region Methods Region

        public void Update(GameTime gameTime)
        {
            if (FollowSource)
            {
                if (ProjectileSource != null)
                {
                    RelPosition = ProjectileSource.Position + SourceOffset;
                    SpawnDirection = ProjectileSource.Direction;
                }
                else
                {
                    RelPosition = Source.Position + SourceOffset;
                    if (ConeEffect)
                    {
                        Vector2 direction = Vector2.Zero;

                        switch (Source.CurrentAnimation)
                        {
                            case 0: direction = new Vector2(0, 1); break;
                            case 1: direction = new Vector2(-1, 0); break;
                            case 2: direction = new Vector2(1, 0); break;
                            case 3: direction = new Vector2(0, -1); break;
                        }

                        SpawnDirection = direction;
                    }
                }
            }

            if (LimitedDuration)
            {
                Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (Timer >= Duration)
                {
                    FlaggedForRemoval = true;
                }
            }

            if (!SpawnAllAtOnce)
            {
                SecPassed += (float)gameTime.ElapsedGameTime.TotalSeconds;
                while (SecPassed > NextSpawnIn)
                {
                    if (ActiveParticles.Count < Budget)
                    {
                        // Spawn a particle
                        Vector2 StartDirection = Vector2.Transform(SpawnDirection, Matrix.CreateRotationZ(MathHelper.Lerp(SpawnNoiseAngle.X, SpawnNoiseAngle.Y, (float)random.NextDouble())));
                        StartDirection.Normalize();
                        Vector2 EndDirection = StartDirection * (float)MathHelper.Lerp(EndSpeed.X, EndSpeed.Y, (float)random.NextDouble());
                        StartDirection *= (float)MathHelper.Lerp(StartSpeed.X, StartSpeed.Y, (float)random.NextDouble());
                        ActiveParticles.AddLast(new Particle(
                            RelPosition + Parent.Position,
                            StartDirection,
                            EndDirection,
                            (float)MathHelper.Lerp(StartLife.X, StartLife.Y, (float)random.NextDouble()),
                            (float)MathHelper.Lerp(StartScale.X, StartScale.Y, (float)random.NextDouble()),
                            (float)MathHelper.Lerp(EndScale.X, EndScale.Y, (float)random.NextDouble()),
                            Color.Lerp(StartColor1, StartColor2, (float)random.NextDouble()),
                            Color.Lerp(EndColor1, EndColor2, (float)random.NextDouble()),
                            this)
                        );
                        ActiveParticles.Last.Value.Damaging = Damaging;
                    }
                    SecPassed -= NextSpawnIn;
                    NextSpawnIn = MathHelper.Lerp(SecPerSpawn.X, SecPerSpawn.Y, (float)random.NextDouble());
                }
            }

            else
            {

                if (ActiveParticles.Count <= 0)
                {
                    if (DeleteAfterSpawn && SpawnCounter > 0)
                    {
                        FlaggedForRemoval = true;
                        return;
                    }
                    SpawnCounter++;
                    for (int i = 0; i < Budget; i++)
                    {
                        Vector2 StartDirection = Vector2.Transform(SpawnDirection, Matrix.CreateRotationZ(MathHelper.Lerp(SpawnNoiseAngle.X, SpawnNoiseAngle.Y, (float)random.NextDouble())));
                        StartDirection.Normalize();
                        Vector2 EndDirection = StartDirection * (float)MathHelper.Lerp(EndSpeed.X, EndSpeed.Y, (float)random.NextDouble());
                        StartDirection *= (float)MathHelper.Lerp(StartSpeed.X, StartSpeed.Y, (float)random.NextDouble());
                        ActiveParticles.AddLast(new Particle(
                            RelPosition + Parent.Position,
                            StartDirection,
                            EndDirection,
                            (float)MathHelper.Lerp(StartLife.X, StartLife.Y, (float)random.NextDouble()),
                            (float)MathHelper.Lerp(StartScale.X, StartScale.Y, (float)random.NextDouble()),
                            (float)MathHelper.Lerp(EndScale.X, EndScale.Y, (float)random.NextDouble()),
                            Color.Lerp(StartColor1, StartColor2, (float)random.NextDouble()),
                            Color.Lerp(EndColor1, EndColor2, (float)random.NextDouble()),
                            this)
                        );
                        ActiveParticles.Last.Value.Damaging = Damaging;
                    }
                }

            }

            
            LinkedListNode<Particle> node = ActiveParticles.First;
            while (node != null)
            {
                bool isAlive = node.Value.Update(gameTime);
                node = node.Next;
                if (!isAlive)
                {
                    if (node == null)
                    {
                        ActiveParticles.RemoveLast();
                    }
                    else
                    {
                        ActiveParticles.Remove(node.Previous);
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, int Scale, Vector2 Offset)
        {
            LinkedListNode<Particle> node = ActiveParticles.First;
            while (node != null)
            {
                node.Value.Draw(spriteBatch, Scale, Offset);
                node = node.Next;
            }
        }

        public void Clear()
        {
            ActiveParticles.Clear();
        }



        #endregion

    }
}
