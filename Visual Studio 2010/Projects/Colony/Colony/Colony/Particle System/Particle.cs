﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements.Enemies;

namespace Colony.Particle_System
{
    public class Particle
    {

        #region Field Region
        public Vector2 Position;
        Vector2 StartDirection;
        Vector2 EndDirection;
        float LifeLeft;
        float StartingLife;
        float ScaleBegin;
        float ScaleEnd;
        public Color StartColor;
        Color EndColor;
        Emitter Parent;
        public float lifePhase;
        Rectangle CollisionRectangle;
        public float currScale;
        public int Scale;
        public Vector2 Offset;
        public Color currCol;
        public Color lightColor = Color.White;
        public bool Damaging;
        #endregion

        #region Constructor region
        public Particle(Vector2 Position, Vector2 StartDirection, Vector2 EndDirection, float StartingLife, float ScaleBegin, float ScaleEnd, Color StartColor, Color EndColor, Emitter Yourself)
        {
            this.Position = Position;
            this.StartDirection = StartDirection;
            this.EndDirection = EndDirection;
            this.StartingLife = StartingLife;
            this.LifeLeft = StartingLife;
            this.ScaleBegin = ScaleBegin;
            this.ScaleEnd = ScaleEnd;
            this.StartColor = StartColor;
            this.EndColor = EndColor;
            this.Parent = Yourself;
            CollisionRectangle = new Rectangle((int)Position.X, (int)Position.Y, (int)ScaleBegin, (int)ScaleBegin);
        }

        #endregion

        #region Methods Region

        public bool Update(GameTime gameTime)
        {
            LifeLeft -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (LifeLeft <= 0)
                return false;
            lifePhase = LifeLeft / StartingLife;  // 1 means newly created 0 means dead.
            Position.X += MathHelper.Lerp(EndDirection.X, StartDirection.X, lifePhase) * (float)gameTime.ElapsedGameTime.TotalSeconds;
            Position.Y += MathHelper.Lerp(EndDirection.Y, StartDirection.Y, lifePhase) * (float)gameTime.ElapsedGameTime.TotalSeconds;
            Map map = Parent.Parent.Map;

            if (map != null)
            {
                int gridX = (int)Position.X / Tile.TileWidth;
                int gridY = (int)Position.Y / Tile.TileHeight;
                if (gridX < 0 || gridY < 0 || gridX > map.Grid.GetLength(1) - 1 || gridY > map.Grid.GetLength(0) - 1)
                    return false;
                if (map.Layers[0].Grid[gridY, gridX].Event == 1)
                    return false;
                if (Damaging && lifePhase > 0.5)
                {
                    foreach (Gameplay_Elements.GenericEnemy e in map.Enemies)
                    {
                        if (e.CollisionRectangle.Contains(new Point((int)Position.X, (int)Position.Y)))
                        {
                            e.TakeDamage(map.Player, 0.1f, gameTime);

                        }
                    }
                }
            }

            

            return true;
        }

        public void Draw(SpriteBatch spriteBatch, int scale, Vector2 offset)
        {
            Scale = scale;
            Offset = offset;
            currScale = MathHelper.Lerp(ScaleEnd, ScaleBegin, lifePhase);
            currCol = Color.Lerp(EndColor, StartColor, lifePhase);

            CollisionRectangle = new Rectangle((int)((Position.X - 0.5f * currScale) * Scale + Offset.X),
                              (int)((Position.Y - 0.5f * currScale) * Scale + Offset.Y),
                              (int)(currScale * Scale),
                              (int)(currScale * Scale));
            

            spriteBatch.Draw(Parent.ParticleSprite,
                     CollisionRectangle,
                     null, currCol, 0, Vector2.Zero, SpriteEffects.None, 0);
        }
        #endregion

    }
}
