﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements
{

    public class Projectile : BaseGameplayElement
    {
        #region Field Region

        public float Damage = 1f;
        public Vector2 Direction;
        float Speed;
        BaseCharacter Target;
        public bool Traveling;
        public int Width;
        public int Height;
        public Color Tint = Color.White;
        public Map Map;
        Rectangle collisionRectangle;
        public float rotation = 3f;
        BaseCharacter From;
        public bool Light;
        public float Distance;
        public Vector2 initialPos;
        float dist;
        bool DamageEnemy;
        bool DamagePlayer;
        public bool FollowTarget = false;
        Color[] ColorData;
        Matrix eMatrix;
        public Action OnHit;
        public bool Rotating = true;
        #endregion

        #region Constructor Region

        public Projectile(Texture2D texture, float Speed, BaseCharacter target, BaseCharacter from, Gamestates.Map_Editor.Map map, bool light = false, bool damageEnemy = true, bool damagePlayer = false, Action OnHit = null)
        {
            this.OnHit = OnHit;
            Graphics = texture;
            this.Speed = Speed;
            Target = target;
            Map = map;
            From = from;
            Light = light;
            DamageEnemy = damageEnemy;
            DamagePlayer = damagePlayer;
            ColorData = new Color[texture.Width * texture.Height];
            texture.GetData(ColorData);
        }

        #endregion

        #region Methods Region

        public virtual void Update(GameTime gameTime)
        {



            if (Traveling)
            {

                if (Rotating)
                    rotation += 0.1f;

                if (Target != null && FollowTarget)
                {
                    Direction = Target.Position - Position;
                    Direction.Normalize();
                    rotation = (float)Math.Atan2(Target.Position.Y - Position.Y, Target.Position.X - Position.X);
                }


                Position += (Direction * Speed) * (float)gameTime.ElapsedGameTime.Milliseconds;
                dist += Speed * (float)gameTime.ElapsedGameTime.Milliseconds;
                if (Distance > 0 && dist > Distance)
                {

                    Map.DeleteProjectile(this);
                    return;
                }
                int gridX = (int)Position.X / Gamestates.Map_Editor.Tile.TileWidth;
                int gridY = (int)Position.Y / Gamestates.Map_Editor.Tile.TileHeight;
                if (gridX < 0 || gridY < 0 || gridX > Map.Grid.GetLength(1) - 1 || gridY > Map.Grid.GetLength(0) - 1)
                {
                }
                else
                {
                    if (Map.Layers[0].Grid[gridY, gridX].Event == 1)
                    {
                        Map.DeleteProjectile(this);

                    }
                }
                

                Matrix transformMatrix = Matrix.Invert(Matrix.CreateTranslation(new Vector3(0, Graphics.Height, 0))) *
                Matrix.CreateRotationZ(rotation) * Matrix.CreateScale(Map.Camera.Scale) *
                Matrix.CreateTranslation(new Vector3(collisionRectangle.X, collisionRectangle.Y, 0));

                if (DamageEnemy)
                {
                    collisionRectangle = new Rectangle((int)Position.X, (int)Position.Y, Width, Height);
                    foreach (GenericEnemy e in Map.Enemies)
                    {
                        if (collisionRectangle.Intersects(e.CollisionRectangle))
                        {
                            eMatrix = Matrix.CreateScale(e.Map.Camera.Scale) * Matrix.CreateTranslation(new Vector3(e.Position, 0f));
                            if (CollisionDetector.IntersectPixels(transformMatrix, Width, Height, ColorData, eMatrix, e.FrameWidth, e.FrameHeight, e.ColorData))
                            {
                                Map.DeleteProjectile(this);
                                e.TakeDamage(From, Damage, gameTime);
                                e.PlayDamageSound();

                                if (e.HitPoints <= 0)
                                    e.Action = EnemyAction.Dieing;
                                break;
                            }
                        }
                    }
                }

                else if (DamagePlayer)
                {
                    collisionRectangle = new Rectangle((int)Position.X, (int)Position.Y, Width, Height);

                    if (collisionRectangle.Intersects(Target.CollisionRectangle))
                    {
                        eMatrix = Matrix.CreateScale(Target.Map.Camera.Scale) * Matrix.CreateTranslation(new Vector3(Target.Position, 0f));
                        if (CollisionDetector.IntersectPixels(transformMatrix, Width, Height, ColorData, eMatrix, Target.FrameWidth, Target.FrameHeight, Target.ColorData))
                        {
                            Map.DeleteProjectile(this);
                            Target.TakeDamage(From, Damage, gameTime);

                        }


                    }

                }

            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
                spriteBatch.Draw(Graphics, new Rectangle((int)Position.X, (int)Position.Y, Width, Height), null, Tint, rotation, new Vector2(Graphics.Width / 2, Graphics.Height / 2), SpriteEffects.None, 0);
        }
        #endregion

    }
}
