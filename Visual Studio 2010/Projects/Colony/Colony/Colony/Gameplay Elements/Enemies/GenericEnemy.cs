﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements.Enemies.AI;
using Colony.Gameplay_Elements.Items.Map_Items;
using System.Diagnostics;
using C3.XNA;
using RamGecXNAControls;

namespace Colony.Gameplay_Elements
{
    public enum EnemyAction
    {
        Idle = 0,
        Wandering,
        Chasing,
        Fleeing,
        Thinking,
        Attacking,
        Casting,
        Shooting,
        Dieing,
        Dead,
        FindingPath

    }

    public class GenericEnemy : BaseCharacter
    {

        #region Field Region
        
        // The enemyPath List stores the path the enemy is taking to reach a certain goal
        public List<Node> enemyPath = new List<Node>();
        public int pathIndex = 0;
        public List<Node> wanderingPath = new List<Node>();
        public int wanderingPathIndex = 0;

        // This is the target the enemy has at the moment. It is assigned when the GetToTarget() method is called
        public BaseCharacter Targeted;
        
        // This will stop any movement when set to true. It's a sort of Emergency Brake. Not actually used
        // for anything at the moment. But will be kept because it may come back useful later on.
        public bool StopMovement = false;

        // This will tell the objects accessing the enemy if it is colliding or not. (Should be converted to read only)
        public bool collide = false;

        // Action is the action the enemy is performing at the moment.
        // LastAction stores the previous action
        public EnemyAction Action = EnemyAction.Idle;
        public EnemyAction LastAction = EnemyAction.Idle;

        // This will give the illusion of intelligence while thinking what to do. The higher this timer, the higher
        // the delay when the enemy has to choose an action
        public float thinkingTimer = 0f;
        public float ThinkingSpeed = 50f;

        // This will switch the animation according to the heading direction if set to true
        public bool AutomaticAnimationSwitch = true;

        // Debug switch. Shows a bunch of stuff for debugging purpose
        public bool Debug = false;

        // The speed the enemy has when wandering around and not chasing a player
        public float WanderingSpeed;

        // If the enemy is ranged (bow, crossbows etc.) this will set how far the enemy can shoot from.
        // Should this be implemented in the weapon instead? Peraphs. But then again, there's now ranged weapon yet.
        public float ShootingRange = 100f;

        // See above
        public float ShootingDelay = 0.8f;

        // The AI container. The IBehaviour interface implement a Think() method which will be called every update
        public IBehaviour AI;

        // Debugging purpose, stores the last steering vector
        public Vector2 lastSteering;

        // Max Speed settings. So... the above WanderSpeed field is for...?
        public float MaxSpeedScalar = 1f;
        public float MaxWanderSpeed = 0.5f;
        
        #endregion

        #region Constructor Region

        public GenericEnemy(Map map, Texture2D spritesheet, int framewidth, int frameheight, int currentSprite)
            : base(map, spritesheet, framewidth, frameheight, currentSprite)
        {
            CollisionRectangle = new Rectangle(0, 0, FrameWidth, FrameHeight);
            AI = new Generic_AI(this);
            Action = EnemyAction.Thinking;
            interval = 140f;
            font = map.stateManager.GameRef.Content.Load<SpriteFont>(@"Fonts\Candara");
        }   

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // Update the collision rectangle position
            CollisionRectangle.X = (int)Position.X;
            CollisionRectangle.Y = (int)Position.Y;

            /// If the user left click the enemy, show the AI Debug GUI
            MouseState ms = Mouse.GetState();
            Vector2 mouse = new Vector2(ms.X, ms.Y);
            Matrix transform = Matrix.Invert(Map.Camera.Transform);

            Vector2.Transform(ref mouse, ref transform, out mouse);
            if (mouse.X >= Position.X && mouse.X <= Position.X + FrameWidth
                && mouse.Y >= Position.Y && mouse.Y <= Position.Y + FrameHeight)
            {
                if (Map.stateManager.GameRef.InputHandler.LeftClick())
                {
                    ShowDiagnosticWindow(new Vector2(ms.X, ms.Y));
                }
            }
            ///////////////////////////////////////////////////////////

            // Call the Think method of the current AI
            if (AI != null)
            {
                AI.Think(gameTime);
            }


         
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
           
            base.Draw(spriteBatch);

            // If the player is near enough, show the enemy's name
            if (!String.IsNullOrEmpty(Name) && Vector2.Distance( Position, Map.Player.Position) < 150f)
            {
                spriteBatch.DrawString(FloatingText.Font, Name, Position + new Vector2(FrameWidth/2 - (FloatingText.Font.MeasureString(Name).X/2), FrameHeight + 3), Color.Red);
            }
            
        }

        // This will be overridden by each caster enemy
        public virtual Spells.BaseSpell ChooseSpell()
        {
            return null;
        }

        public override void OnDeath(BaseCharacter killer)
        {
            base.OnDeath(killer);
            if (Map.rnd.NextDouble() > 0.1f)
            {
                HealthHearth h = new HealthHearth(Position, Map, Map.stateManager.GameRef.Content.Load<Texture2D>(@"Particles\heart"));
                Map.MapItems.Add(h);
                FloatingText.AddFloatingText("Drop", this);
            }
        }

        /// <summary>
        /// Stores the path to the target, using the A*, in the enemyPath list and set the current target as this target
        /// </summary>
        /// <param name="Target">The target to reach</param>
        /// <param name="ignorelastPath">Optional, this will treat all the last path nodes as non walkable</param>
        public void GetToTarget(BaseCharacter Target, bool ignorelastPath = false)
        {

            if (!ignorelastPath)
            {
                enemyPath.Clear();
                Pathfinding.FindPathNoDiagonals(new Node(this.GetCenterXOnGrid(), this.GetCenterYOnGrid(), TileType.None), new Node(Target.GetCenterXOnGrid(), (int)(Target.Position.Y + Target.FrameHeight - 6) / Tile.TileHeight, TileType.None), false);
                enemyPath = Pathfinding.ReconstructPath();
                enemyPath.Reverse();
                pathIndex = 0;
                Targeted = Target;
            }
            else
            {
                
                
                enemyPath = Pathfinding.ReconstructPath();
                enemyPath.Reverse();
                pathIndex = 0;
                Targeted = Target;
            }
            Action = EnemyAction.Chasing;
        }
        

        public virtual void PlayDamageSound()
        { }

        public void ShowDiagnosticWindow(Vector2 ms)
        {
            
            int id = Map.Enemies.IndexOf(this);
            if (Map.stateManager.GameRef.GuiManager.GetControl("Enemy " + id.ToString()) != null)
            {
                return;
            }
            Window w = new Window(new Rectangle((int)ms.X + FrameWidth + 5, (int)ms.Y, 150, 200), "AI Diagnostic - unit: " + id.ToString());
            w.Name = "Enemy " + id.ToString();
            Button aiState = new Button(new Rectangle(35, 35, 80, 20), "AI State");
            Button aiTweak = new Button(new Rectangle(35, 65, 80, 20), "Tweak AI");
            Button aiSwap = new Button(new Rectangle(35, 95, 80, 20), "Change AI");
            Button doAction = new Button(new Rectangle(25, 125, 100, 20), "Perform action");
            Button actorInfo = new Button(new Rectangle(25, 155, 100, 20), "Actor Info");

            aiState.OnClick += (s) =>
            {
                ShowAiStateWindow(ms);
            };

            w.Controls.Add(aiState);
            w.Controls.Add(aiTweak);
            w.Controls.Add(aiSwap);
            w.Controls.Add(doAction);
            w.Controls.Add(actorInfo);

            Image close = new Image(new Rectangle(5, 5, 10, 10), Map.stateManager.GameRef.Content.Load<Texture2D>(@"Gui\closebutton"));
            close.OnClick += (s) =>
            {
                Map.stateManager.GameRef.GuiManager.Controls.Remove(Map.stateManager.GameRef.GuiManager.GetControl(w.Name));
            };

            w.Controls.Add(close);

            Map.stateManager.GameRef.GuiManager.Controls.Add(w);
                  
        }

        public void ShowAiStateWindow(Vector2 ms)
        {
            int id = Map.Enemies.IndexOf(this);
            if (Map.stateManager.GameRef.GuiManager.GetControl("AI State " + id.ToString()) != null)
            {
                return;
            }
            Window w = new Window(new Rectangle((int)ms.X + FrameWidth + 5, (int)ms.Y, 250, 250), "AI State - unit: " + id.ToString());
            w.Name = "AI State " + id.ToString();

            UpdatableLabel l = new UpdatableLabel(new Rectangle(5, 25, 80, 20),  () =>
            {
                return "Current Action: "+this.GetActionText();
            });

            UpdatableLabel lastAction = new UpdatableLabel(new Rectangle(5, 55, 80, 20), () =>
                {
                    return "Last Action: " + this.LastAction.ToString();
                });
            Label steering_l = new Label(new Rectangle(5, 85, 80, 20), "Steering:");
            
            RotableImage steering = new RotableImage(new Rectangle(85, 95, 80, 20), Map.stateManager.GameRef.Content.Load<Texture2D>(@"Images\vector"), Color.Red,
                () =>
                {
                    Vector2 nV = Vector2.Normalize(this.Steering);
                    return (float)(Math.Atan2(nV.X, -nV.Y));

                });
            Label velocity_l = new Label(new Rectangle(5, 125, 80, 20), "Velocity:");
            RotableImage velocity = new RotableImage(new Rectangle(85, 135, 80, 20), Map.stateManager.GameRef.Content.Load<Texture2D>(@"Images\vector"), Color.Green,
                () =>
                {
                    Vector2 nV = Vector2.Normalize(this.Velocity);
                    return (float)(Math.Atan2(nV.X, -nV.Y));

                });

            UpdatableLabel position = new UpdatableLabel(new Rectangle(5, 165, 80, 20), () =>
            {
                return "Position: " + this.Position.X.ToString() + ", " + this.Position.Y.ToString();
            });
            UpdatableLabel distance = new UpdatableLabel(new Rectangle(5, 195, 80, 20), () =>
                {
                    return "Distance: " + Vector2.Distance(this.Position, this.Map.Player.Position).ToString();
                });

            w.Controls.Add(distance);
            w.Controls.Add(velocity_l);
            w.Controls.Add(steering_l);
            w.Controls.Add(l);
            w.Controls.Add(lastAction);
            w.Controls.Add(steering);
            w.Controls.Add(velocity);
            w.Controls.Add(position);
            
            Map.stateManager.GameRef.GuiManager.Controls.Add(w);
        }

        public string GetActionText() { return Action.ToString(); }
        
        #endregion
    }

    public class UpdatableLabel : Label
    {

        Func<string> UpdateText;
       

        public UpdatableLabel(Rectangle bound, Func<string> updateAction) : base(bound, "")
        {
            UpdateText = updateAction;
            
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (UpdateText != null)
                Text = UpdateText();
        }
    }

    public class RotableImage : Image
    {
        public float Rotation = 0f;
        Func<float> UpdateRotation;
        Color color;

        public RotableImage(Rectangle bound, Texture2D image, Color c, Func<float> updateRotation)
            :base(bound, image)
        {
            UpdateRotation = updateRotation;
            color = c;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (UpdateRotation != null)
                Rotation = UpdateRotation();
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            // draw only if texture is present
            if (Texture != null)
                spriteBatch.Draw(Texture, AbsoluteBounds, null, color * Transparency, Rotation, new Vector2(Texture.Width/2, Texture.Height/2), SpriteEffects.None, 0);

            
        }

        

    }
}
