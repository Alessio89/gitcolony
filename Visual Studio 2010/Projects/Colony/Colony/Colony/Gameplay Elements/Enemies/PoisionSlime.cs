﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Enemies
{
    public class PoisonSlime : GenericEnemy
    {
        #region Field Region

        SoundEffect damageSound;

        #endregion

        #region Constructor Region

        public PoisonSlime(Map map)
            : base(map, map.stateManager.Game.Content.Load<Texture2D>(@"SpriteSheets\Monster2"), 32, 32, 6)
        {
            HitPoints = 2f;
            MaxHitPoints = 2f;
            ManaPoints = 0f;
            Attack = 2f;
            Defense = 1f;
            Speed = 400f; // swingSeed in milliseconds
            WanderingSpeed = 0.004f;
            ActiveRadius = 400f;
            damageSound = map.stateManager.GameRef.Content.Load<SoundEffect>(@"SoundFX\slime2");
            MaxVelocity = new Vector2(3f, 3f);
            MaxSpeedScalar = 3f;
            AI = new AI.Generic_AI(this);
            Name = "Poison Slime";
        }
        //(map, map.stateManager.Game.Content.Load<Texture2D>(@"SpriteSheets\Monster2"), 32, 32, rnd.Next(0, 4));
        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

        }

        public override void PlayDamageSound()
        {
            damageSound.Play();
        }
        #endregion

    }
}
