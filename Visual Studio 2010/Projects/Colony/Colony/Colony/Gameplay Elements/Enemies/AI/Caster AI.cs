﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using RamGecXNAControls;
using RamGecXNAControls.ExtendedControls;

namespace Colony.Gameplay_Elements.Enemies.AI
{



    // [ Generic enemy AI ]:
    // Start idle, switch to thinking mode. If the player is in the enemy's range, and can be seen, the enemy will start chasing
    // the player. Otherwise, it will wander around the map. Enemy.ThinkingSpeed will set the delay before one movement and the other.

    public class Caster_AI : IBehaviour
    {
        protected GenericEnemy Actor;
        protected float thinkingTimer = 0f;
        protected float genericTimer = 0f;
        protected float MaxForce = 35f;
        protected float CheckDelay = 1000f;
        protected bool CanFlee = true;
        protected float FleeingCooldown = 0f;
        protected float swingTimer = 0f;
        protected float swingDelay = 1200f;

        public Caster_AI(GenericEnemy actor)
        {
            Actor = actor;

        }

        public virtual void Think(GameTime gameTime)
        {

            // DEBUG
            //Actor.AI = new Test_AI(Actor);
            //return;
            //

            if (Actor.Loop)
            {
                Vector2 v = Actor.Map.Player.Position - Actor.Position;

                float angle = (float)Math.Atan2(v.Y, -v.X);

                if (v != Vector2.Zero)
                {
                    float absDegree = Math.Abs(MathHelper.ToDegrees(angle));
                    if (angle > 0)
                    {
                        if (Math.Abs(MathHelper.ToDegrees(angle)) < 45)
                            Actor.CurrentAnimation = 1;
                        else if (Math.Abs(MathHelper.ToDegrees(angle)) >= 45 && Math.Abs(MathHelper.ToDegrees(angle)) < 135)
                            Actor.CurrentAnimation = 0;
                        else if (absDegree >= 135 && absDegree < 220)
                            Actor.CurrentAnimation = 2;

                    }
                    else
                    {
                        if (Math.Abs(MathHelper.ToDegrees(angle)) < 45)
                            Actor.CurrentAnimation = 1;
                        else if (Math.Abs(MathHelper.ToDegrees(angle)) >= 45 && Math.Abs(MathHelper.ToDegrees(angle)) < 135)
                            Actor.CurrentAnimation = 3;
                        else if (absDegree >= 135 && absDegree < 220)
                            Actor.CurrentAnimation = 2;
                    }
                }

            }

            if (!CanFlee)
            {
                FleeingCooldown += (float)gameTime.ElapsedGameTime.Milliseconds;
                if (FleeingCooldown > 2000)
                {
                    CanFlee = true;
                    FleeingCooldown = 0f;
                }
            }

            if (Actor.Action == EnemyAction.Thinking)
            {

                genericTimer = 0f;
                thinkingTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                if (thinkingTimer > Actor.ThinkingSpeed)
                {
                    thinkingTimer = 0f;
                    Actor.interval = 140f;
                    TakeDecision();
                }

            }

            else if (Actor.Action == EnemyAction.Chasing)
            {
                if (Vector2.Distance(Actor.Position, Actor.Map.Player.Position) < Actor.ShootingRange)
                {
                    Actor.Velocity = Vector2.Zero;
                    Actor.Steering = Vector2.Zero;
                    Actor.LastAction = Actor.Action;
                    Actor.Action = EnemyAction.Shooting;
                    return;
                }

                genericTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                if (genericTimer > 1000f)
                {
                    genericTimer = 0f;
                    //Actor.LastAction = Actor.Action;
                    Actor.Action = EnemyAction.Thinking;
                }

                else
                {

                    //Actor.Steering = Actor.MovementManager.Pursuit(Actor.Map.Player);
                    Actor.Steering = Actor.MovementManager.Pursuit(Actor.Map.Player);

                }

            }

            else if (Actor.Action == EnemyAction.Wandering)
            {
                genericTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                if (genericTimer > CheckDelay)
                {
                    genericTimer = 0f;
                    Actor.Action = EnemyAction.Thinking;
                }
                else
                {
                    Actor.Steering = Actor.MovementManager.Wander(gameTime);
                }
            }

            else if (Actor.Action == EnemyAction.Fleeing)
            {
                genericTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                if (!CanFlee)
                    Actor.Action = EnemyAction.Thinking;
                else
                {
                    genericTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                    if (genericTimer > 4000)
                    {
                        genericTimer = 0f;
                        CanFlee = false;
                    }
                    else
                        Actor.Steering = Actor.MovementManager.Flee(Actor.Map.Player.Position);
                }
            }

            else if (Actor.Action == EnemyAction.Shooting)
            {
                if (Vector2.Distance(Actor.Position, Actor.Map.Player.Position) > Actor.ShootingRange)
                {

                    Actor.LastAction = Actor.Action;
                    swingTimer = 0f;
                    Actor.Action = EnemyAction.Thinking;
                }
                else
                {
                    if (Actor.HitPoints < Actor.MaxHitPoints / 5)
                    {
                        Actor.LastAction = Actor.Action;
                        Actor.Action = EnemyAction.Fleeing;
                        swingTimer = 0f;
                        return;
                    }
                    Actor.Loop = true;
                    Actor.interval = 80f;
                    swingTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                    if (swingTimer >= Actor.Speed)
                    {
                        swingTimer = 0f;
                        Actor.Steering = Vector2.Zero;
                        Actor.Velocity = Vector2.Zero;
                        (Actor.Spells[0] as Spells.Fireball).target = Actor.Map.Player;

                        Spells.BaseSpell s = Actor.ChooseSpell();
                        if (s != null)
                        {
                            if (s is Spells.ITargetableSpell)
                                (s as Spells.ITargetableSpell).SetTarget(Actor.Map.Player);
                            s.Cast();
                        }
                                  
                    }
                }
            }

            else if (Actor.Action == EnemyAction.Dieing)
            {
                SfxManager.Explosion(Actor.Position + new Vector2(Actor.FrameWidth / 2, Actor.FrameHeight / 2), Color.White, Color.WhiteSmoke, 60, 0.21f);
                Actor.Action = EnemyAction.Dead;
            }




            // Actor.Steering += Actor.MovementManager.Separation();
            //Actor.Steering += Actor.MovementManager.AvoidObstacles(gameTime);

            Actor.MovementManager.NoPenetration();
            Actor.Steering = Truncate(Actor.Steering, MaxForce);

            if (Actor.Action != EnemyAction.Wandering)
                Actor.Velocity = Truncate(Actor.Velocity + Actor.Steering, Actor.MaxSpeedScalar);
            else
                Actor.Velocity = Truncate(Actor.Velocity + Actor.Steering, Actor.MaxWanderSpeed);
        }

        public virtual void TakeDecision()
        {
            if (Vector2.Distance(Actor.Position, Actor.Map.Player.Position) < Actor.ActiveRadius && Actor.InLOS(Actor.Map.Player))
            {
                if (Vector2.Distance(Actor.Position, Actor.Map.Player.Position) < Actor.ShootingRange)
                {


                    Actor.LastAction = Actor.Action;
                    Actor.Action = EnemyAction.Shooting;
                    return;
                }
                else
                {
                    if (Actor.LastAction != EnemyAction.Chasing)
                        FloatingText.AddFloatingText("!!", Actor);
                    Actor.LastAction = Actor.Action;
                    Actor.Action = EnemyAction.Chasing;
                }
            }
            else
            {
                if (Actor.LastAction != EnemyAction.Wandering)
                    Actor.Velocity = Vector2.Zero;
                Actor.LastAction = Actor.Action;
                Actor.Action = EnemyAction.Wandering;
            }

            if (Actor.HitPoints < Actor.MaxHitPoints / 5 && CanFlee)
            {
                Actor.LastAction = Actor.Action;
                Actor.Action = EnemyAction.Fleeing;
            }

            if (Actor.HitPoints <= 0)
            {
                Actor.LastAction = Actor.Action;
                Actor.Action = EnemyAction.Dieing;
            }

        }

        public Rectangle d_GetCollisionRectangle() { return Actor.CollisionRectangle; }

        public Vector2 Truncate(Vector2 original, float max)
        {
            if (original.Length() > max)
            {
                original.Normalize();

                original *= max;
            }
            return original;
        }

    }
}