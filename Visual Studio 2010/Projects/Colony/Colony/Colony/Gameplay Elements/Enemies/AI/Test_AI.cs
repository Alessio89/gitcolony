﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using RamGecXNAControls;
using RamGecXNAControls.ExtendedControls;

namespace Colony.Gameplay_Elements.Enemies.AI
{
    public class Test_AI : IBehaviour
    {
        public GenericEnemy Actor;

        public Test_AI(GenericEnemy actor)
        {
            Actor = actor;
        }

        public Rectangle d_GetCollisionRectangle()
        { return Actor.CollisionRectangle; }

        public void Think(GameTime gameTime)
        {

            
            Pathfinding.FindPathNoDiagonals(new Node((int)Actor.Position.X / Tile.TileWidth, (int)Actor.Position.Y / Tile.TileHeight, TileType.None),
                                             new Node((int)Actor.Map.Player.Position.X / Tile.TileWidth, (int)Actor.Map.Player.Position.Y / Tile.TileHeight, TileType.None),
                                             false);
            Actor.enemyPath = Pathfinding.ReconstructPath();
            Actor.enemyPath.Reverse();

            Actor.Steering = Actor.MovementManager.FollowPath(Actor.enemyPath);
            Actor.Velocity += Actor.Steering;



        }
    }
}
