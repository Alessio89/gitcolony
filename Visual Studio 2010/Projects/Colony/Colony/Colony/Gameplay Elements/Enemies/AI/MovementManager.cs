﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using C3.XNA;

namespace Colony.Gameplay_Elements.Enemies.AI
{
    public class MovementManager
    {
        BaseCharacter Actor;

        Vector2 WanderTarget = new Vector2(0, 0);

        float slowingRadius = 150f;
        float circleDistance = 0.5f;
        float circleRadius = 1f;
        float wanderAngle = 0f;
        float wanderTimer = 0f;
        float MaxForce = 28f;
        float swingTimer = 0f;
        float swingDelay = 500f;
        int currentNode = 0;



        public MovementManager(BaseCharacter actor)
        {
            Actor = actor;
        }


        public void ChangeBehaviour()
        {
            //Actor.Velocity = Vector2.Zero;
        }


        public void Attack(BaseCharacter target, GameTime gameTime)
        {
            Actor.Loop = true;
            Actor.interval = 30f;
            Actor.Velocity = Vector2.Zero;
            Actor.Steering = Vector2.Zero;
            swingTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
            if (swingTimer >= swingDelay)
            {
                swingTimer = 0f;
                target.TakeDamage(Actor, 1f, gameTime);
            }
        }

        public bool IsAheadOfLeader(BaseCharacter leader, Vector2 leaderAhead)
        {
            return Vector2.Distance(leaderAhead, Actor.Position) <= 70f;
        }

        public bool IsInMeleeRange(BaseCharacter target)
        {
            return (Vector2.Distance(Actor.Position, target.Position) < 20f);
        }

        public Vector2 FollowLeader(BaseCharacter leader, float distance)
        {
            Vector2 tv = Vector2.Zero;
            Vector2 force = Vector2.Zero;
            if (leader.Velocity != Vector2.Zero)
            {
                tv = Vector2.Normalize(leader.Velocity * -1) * distance;
                leader.lastVelocity = leader.Velocity;
            }
            else if (leader.lastVelocity != Vector2.Zero)
                tv = Vector2.Normalize(leader.lastVelocity * -1) * distance;




            Vector2 ahead = leader.Position + (tv * -1);

            Vector2 behind = leader.Position + tv;

            if (IsAheadOfLeader(leader, ahead))
                force += Evade(leader, 2f);

            force += Arrival(behind);
            force += Separation();

            return force;
        }

        public Vector2 Seek(Vector2 target)
        {
            Vector2 desired_velocity = Vector2.Normalize(target - Actor.Position) * 4f;
            Vector2 steering = desired_velocity - Actor.Velocity;
            return steering;
        }

        public Vector2 OffsetPursuit(BaseCharacter target, float radius)
        {
            float dT = 0.8f * Vector2.Distance(Actor.Position, target.Position);
            Vector2 nextPosition = (target.Position + Vector2.Normalize(target.Velocity) * dT);
            if (float.IsNaN(nextPosition.X) || float.IsNaN(nextPosition.Y))
                nextPosition = target.Position;

            nextPosition += new Vector2(radius, radius);
            return Seek(nextPosition);
        }

        public Vector2 Pursuit(BaseCharacter target)
        {
            float dT = 0.8f * Vector2.Distance(Actor.Position, target.Position);
            Vector2 nextPosition = target.Position + Vector2.Normalize(target.Velocity) * dT;
            if (float.IsNaN(nextPosition.X) || float.IsNaN(nextPosition.Y))
                nextPosition = target.Position;

            return Seek(nextPosition);
        }
        public Vector2 Evade(BaseCharacter target, float strength = 1f)
        {
            float dT = 2f * Vector2.Distance(Actor.Position, target.Position);
            Vector2 nextPosition = target.Position + Vector2.Normalize(target.Velocity) * dT;
            if (float.IsNaN(nextPosition.X) || float.IsNaN(nextPosition.Y))
                nextPosition = target.Position;

            return Flee(nextPosition) * strength;
        }
        public Vector2 Flee(Vector2 target)
        {
            Vector2 desired_velocity = Vector2.Normalize(Actor.Position - target) * Actor.MaxVelocity;
            Vector2 steering = desired_velocity - Actor.Velocity;
            return steering;
        }
        public Vector2 Arrival(Vector2 target)
        {

            if (Vector2.Distance(Actor.Position, target) > slowingRadius)
                return Seek(target);


            float slowingFactor = Vector2.Distance(Actor.Position, target) / slowingRadius;
            slowingFactor *= 0.8f;
            Vector2 desired_velocity = Vector2.Normalize(target - Actor.Position) * slowingFactor;

            Vector2 steering = desired_velocity - Actor.Velocity;



            return steering;




        }

        Vector2 randomTarget = Vector2.Zero;


        public Vector2 Wander(GameTime gameTime)
        {
            wanderTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
            Vector2 wanderForce = Vector2.Zero;
            if (wanderTimer > 500)
            {

                wanderTimer = 0f;
                if (Actor.Velocity == Vector2.Zero)
                {
                    Actor.Velocity = new Vector2((Actor.Map.rnd.Next(3) - 1), (Actor.Map.rnd.Next(3) - 1));
                }

                Vector2 circleCenter = Vector2.Normalize(new Vector2(Actor.Velocity.X, Actor.Velocity.Y)) * circleDistance;
                Vector2 displacement = new Vector2(1, 0) * circleRadius;
                displacement = Vector2.Transform(displacement, Matrix.CreateRotationZ(wanderAngle));
                wanderAngle += MathHelper.ToRadians(Actor.Map.rnd.Next(180));//(Actor.Map.rnd.NextDouble() >= 0.5) ? (float)(Actor.Map.rnd.NextDouble()) : -(float)(Actor.Map.rnd.NextDouble());
                wanderForce = (circleCenter + displacement);

            }
            return wanderForce;




        }

        public Vector2 AvoidObstacles(GameTime gameTime)
        {
            Vector2 avoidance_force = Vector2.Zero;

            Vector2 v = (Actor.Position + Actor.Velocity) - Actor.Position;

            float angle = (float)Math.Atan2(v.Y, -v.X);
            GridCell gc = new GridCell(0, 0, 0, Actor.Map);
            if (v != Vector2.Zero)
            {
                float absDegree = Math.Abs(MathHelper.ToDegrees(angle));
                if (angle > 0)
                {
                    if (Math.Abs(MathHelper.ToDegrees(angle)) < 45)
                        gc = Actor.Map.Layers[0].Grid[Actor.GetCenterYOnGrid(), Actor.GetCenterXOnGrid()].Left();
                    else if (Math.Abs(MathHelper.ToDegrees(angle)) >= 45 && Math.Abs(MathHelper.ToDegrees(angle)) < 135)
                        gc = Actor.Map.Layers[0].Grid[Actor.GetCenterYOnGrid(), Actor.GetCenterXOnGrid()].Bottom();
                    else if (absDegree >= 135 && absDegree < 220)
                        gc = Actor.Map.Layers[0].Grid[Actor.GetCenterYOnGrid(), Actor.GetCenterXOnGrid()].Right();

                }
                else
                {
                    if (Math.Abs(MathHelper.ToDegrees(angle)) < 45)
                        gc = Actor.Map.Layers[0].Grid[Actor.GetCenterYOnGrid(), Actor.GetCenterXOnGrid()].Left();
                    else if (Math.Abs(MathHelper.ToDegrees(angle)) >= 45 && Math.Abs(MathHelper.ToDegrees(angle)) < 135)
                        gc = Actor.Map.Layers[0].Grid[Actor.GetCenterYOnGrid(), Actor.GetCenterXOnGrid()].Top();
                    else if (absDegree >= 135 && absDegree < 220)
                        gc = Actor.Map.Layers[0].Grid[Actor.GetCenterYOnGrid(), Actor.GetCenterXOnGrid()].Right();
                }
            }
            if (gc != null)
            {
                Vector2 ahead = Actor.Position + Vector2.Normalize(Actor.Velocity) * 1f;
                avoidance_force = ahead - new Vector2(gc.X * Tile.TileWidth + Tile.TileWidth / 2, gc.Y * Tile.TileHeight + Tile.TileHeight / 2);
                avoidance_force = Vector2.Normalize(avoidance_force) * 3f;
            }

            return avoidance_force;
        }

        public Vector2 Queue(Vector2 steering)
        {

            GenericEnemy e = GetAllyAhead();
            Vector2 v = new Vector2(Actor.Velocity.X, Actor.Velocity.Y);
            Vector2 brake = Vector2.Zero;

            if (e != null)
            {
                brake.X = -steering.X * 0.8f;
                brake.Y = -steering.Y * 0.8f;

                v *= -1;
                brake += v;
                brake += Separation();
                if (Vector2.Distance(Actor.Position, e.Position) < 30f)
                    Actor.Velocity *= 0.3f;
            }

            return brake;

        }

        public GenericEnemy GetAllyAhead()
        {
            Vector2 qa = new Vector2(Actor.Velocity.X, Actor.Velocity.Y);
            qa.Normalize();

            Vector2 ahead = qa + Actor.Position;

            foreach (GenericEnemy e in (from m in Actor.Map.Enemies
                                        where m != Actor
                                        orderby Vector2.Distance(Actor.Position, m.Position)
                                        select m))
            {

                if (e != Actor && Vector2.Distance(ahead, e.Position) <= 20f)
                {
                    return e;
                }

            }

            return null;
        }

        public Vector2 Separation()
        {
            Vector2 force = Vector2.Zero;
            int neighbourCount = 0;

            foreach (GenericEnemy e in Actor.Map.Enemies)
            {
                if (e != Actor && e.Action != EnemyAction.Attacking && Vector2.Distance(e.Position, Actor.Position) <= 15)
                {
                    force.X += e.Position.X - Actor.Position.X;
                    force.Y += e.Position.Y - Actor.Position.Y;
                    //force *= 1 / Vector2.Distance(Actor.Position, e.Position);
                    neighbourCount++;
                }
            }

            if (neighbourCount != 0)
            {
                force.X /= neighbourCount;
                force.Y /= neighbourCount;
                force *= -1;
                force.Normalize();

            }

            // force = Vector2.Transform(force, Matrix.CreateRotationZ(MathHelper.ToRadians(Actor.Map.rnd.Next(40))));
            //force *= Actor.Velocity * 0.9f;
            force *= 15f;
            return force;
        }

        public Vector2 FollowPath(List<Node> path)
        {
            Vector2 target = Vector2.Zero;

            if (path.Count > 0)
            {
                if (currentNode >= path.Count)
                    return Vector2.Zero;
                target = new Vector2(path[currentNode].X * Tile.TileWidth, path[currentNode].Y * Tile.TileHeight);
                if (Vector2.Distance(Actor.Position, target) <= 10f)
                {
                    currentNode++;
                }

            }


            return (target == Vector2.Zero) ? Vector2.Zero : Seek(target);
        }

        public bool LineIntersectCircle(Vector2 vector, Vector2 circleCenter, float circleRadius)
        {
            if (Vector2.Distance(vector, circleCenter) < circleRadius)
            {
                return true;
            }
            return false;
        }

        public void NoPenetration()
        {
            foreach (GenericEnemy e in Actor.Map.Enemies)
            {
                if (e == Actor)
                    continue;

                Vector2 toEntity = Actor.Position - e.Position;

                float distFromEachOther = toEntity.Length();

                float amountOverlap = /*e.BRadius*/10f + /*Actor.BRadius*/10f - distFromEachOther;

                if (amountOverlap >= 0)
                {
                    Actor.Position += (toEntity / distFromEachOther) * amountOverlap;
                }
            }
        }





    }
}
