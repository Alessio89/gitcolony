﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Enemies
{
    public class Dummy : GenericEnemy
    {

        #region Field Region

        SoundEffect damageSound;
        
        #endregion

        #region Constructor Region

        public Dummy(Map map, Texture2D spriteSheet, int framewidth, int frameheight, int currentSprite)
            : base(map, spriteSheet, framewidth, frameheight, currentSprite)
        {
            HitPoints = 10f;
            MaxHitPoints = 10f;
            ManaPoints = 0f;
            Attack = 1f;
            Defense = 1f;
            Speed = 0.004f;
            WanderingSpeed = 0.004f;
            ActiveRadius = 200f;
            damageSound = map.stateManager.GameRef.Content.Load<SoundEffect>(@"SoundFX\Slime2");
        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

        }

        public override void PlayDamageSound()
        {
            damageSound.Play();
        }

        #endregion

    }
}
