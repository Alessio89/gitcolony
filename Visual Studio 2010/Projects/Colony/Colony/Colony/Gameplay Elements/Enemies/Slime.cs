﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Enemies
{
    public class Slime : GenericEnemy
    {

        #region Field Region

        SoundEffect damageSound;
        
        #endregion

        #region Constructor Region

        public Slime(Map map)
            : base(map, map.stateManager.Game.Content.Load<Texture2D>(@"SpriteSheets\Monster2"), 32, 32, 2)
        {
            HitPoints = 2f;
            MaxHitPoints = 2f;
            ManaPoints = 0f;
            Attack = 1f;
            Defense = 1f;
            Speed = 1000f; // swingSeed in milliseconds
            WanderingSpeed = 0.004f;
            ActiveRadius = 200f;
            damageSound = map.stateManager.GameRef.Content.Load<SoundEffect>(@"SoundFX\slime2");
            MaxVelocity = new Vector2(1.5f, 1.5f);
            MaxSpeedScalar = 1.5f;
            AI = new AI.Generic_AI(this);
            Name = "Slime";

        }
        //(map, map.stateManager.Game.Content.Load<Texture2D>(@"SpriteSheets\Monster2"), 32, 32, rnd.Next(0, 4));
        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

        }

        public override void PlayDamageSound()
        {
            damageSound.Play();
        }

        #endregion

    }
}
