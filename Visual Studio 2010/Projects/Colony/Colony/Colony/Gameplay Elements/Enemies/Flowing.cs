﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Enemies
{
    public class Flowing : GenericEnemy
    {
        #region Field Region

        SoundEffect damageSound;

        #endregion

        #region Constructor Region

        public Flowing(Map map)
            : base(map, map.stateManager.Game.Content.Load<Texture2D>(@"SpriteSheets\Monster2"), 32, 32, 4)
        {
            HitPoints = 6f;
            MaxHitPoints = 6f;
            ManaPoints = 0f;
            Attack = 6f;
            Defense = 1f;
            Speed = 3200f; // swingSeed in milliseconds
            WanderingSpeed = 0.004f;
            ActiveRadius = 400f;
            damageSound = map.stateManager.GameRef.Content.Load<SoundEffect>(@"SoundFX\slime2");
            MaxVelocity = new Vector2(1f, 1f);
            MaxSpeedScalar = 1f;
            AI = new AI.Caster_AI(this);
            Name = "Flowing";
            Spells.Add(new Spells.Fireball(this));
            Spells.Add(new Spells.Iceshard(this));
            ShootingRange = 300f;
        }
        //(map, map.stateManager.Game.Content.Load<Texture2D>(@"SpriteSheets\Monster2"), 32, 32, rnd.Next(0, 4));
        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

        }

        public override void PlayDamageSound()
        {
            damageSound.Play();
        }

        public override Spells.BaseSpell ChooseSpell()
        {
            if (Spells.Count > 0)
                return Spells[Map.rnd.Next(Spells.Count)];
            else
                return null;
        }
        #endregion

    }
}
