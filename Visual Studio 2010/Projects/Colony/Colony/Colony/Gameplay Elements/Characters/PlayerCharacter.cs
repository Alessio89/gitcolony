﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements.Items;
using Colony.Gameplay_Elements.Items.Map_Items;
using Colony.Gui;
using Colony.Gameplay_Elements.Spells;

namespace Colony.Gameplay_Elements
{
    public class PlayerCharacter : BaseCharacter
    {

        #region Field Region

        public List<BaseEquippable> EquipList = new List<BaseEquippable>();
        Rectangle interactionRectangle;
        bool songChangedToFight;

        #endregion

        #region Constructor Region

        public PlayerCharacter(Map map, Texture2D spritesheet, int frameWidth, int frameHeight, int currentSprite)
            : base(map, spritesheet, frameWidth, frameHeight, currentSprite)
        {
            CollisionRectangle = new Rectangle(0, 0, frameWidth, frameHeight);
            EquipList.Add(new Katana(this));
            EquipList.Add(new LongSword(this));
            EquipList.Add(new Torch(map.stateManager.GameRef.Content.Load<Texture2D>(@"Items\torch"), this));
            MaxHitPoints = 20;
            HitPoints = 20;
        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            CollisionRectangle.X = (int)Position.X;
            CollisionRectangle.Y = (int)Position.Y;

            if (Fighting && !songChangedToFight)
            {
                songChangedToFight = true;

                GamePlayState p = Map.stateManager.GetStateByType(typeof(GamePlayState)) as GamePlayState;
                p.dungSongPosition = MediaPlayer.PlayPosition;
                MediaPlayer.Play(p.fightSong);
            }
            else if (!Fighting && songChangedToFight)
            {
                songChangedToFight = false;
                GamePlayState p = Map.stateManager.GetStateByType(typeof(GamePlayState)) as GamePlayState;
                MediaPlayer.Play(p.dungSong);
                
                
                
            }

            InputHandler.InputHandler inputHandler = Map.stateManager.GameRef.InputHandler;

            if (inputHandler.IsKeyPressed(Keys.D1))
                ActionBar.Select(0);
            else if (inputHandler.IsKeyPressed(Keys.D2))
                ActionBar.Select(1);
            else if (inputHandler.IsKeyPressed(Keys.D3))
                ActionBar.Select(2);
            else if (inputHandler.IsKeyPressed(Keys.D4))
                ActionBar.Select(3);
            else if (inputHandler.IsKeyPressed(Keys.D5))
                ActionBar.Select(4);
            else if (inputHandler.IsKeyPressed(Keys.D6))
                ActionBar.Select(5);
            else if (inputHandler.IsKeyPressed(Keys.D7))
                ActionBar.Select(6);

            else if (inputHandler.IsKeyPressed(Keys.D8))
                ActionBar.Select(7);
            else if (inputHandler.IsKeyPressed(Keys.D9))
                ActionBar.Select(8);

            else if (inputHandler.IsKeyPressed(Keys.D0))
                ActionBar.Select(9);

            if (inputHandler.IsKeyPressed(Keys.Up))
            {
                if (ActionBar.SelectedItem >= ActionBar.UsableItems.Length || ActionBar.UsableItems[ActionBar.SelectedItem] == null)
                    return;

                IItem i = ActionBar.UsableItems[ActionBar.SelectedItem].InstancesList[0];
                i.Use();



            }
            if (inputHandler.IsKeyPressed(Keys.E))
            {
                for (int i = 0; i < Map.MapItems.Count; i++)
                {
                    float distance = Vector2.Distance(Map.MapItems[i].Position + new Vector2(Map.MapItems[i].Graphic.Width / 2, Map.MapItems[i].Graphic.Height / 2), Position + new Vector2(FrameWidth / 2, FrameHeight / 2));
                    Rectangle interactionRectangle = new Rectangle();
                    switch (CurrentAnimation)
                    {
                        case 0: interactionRectangle = new Rectangle((int)Position.X, (int)Position.Y + Tile.TileHeight, FrameWidth, FrameHeight); break;
                        case 1: interactionRectangle = new Rectangle((int)Position.X - Tile.TileWidth, (int)Position.Y, FrameWidth, FrameHeight); break;
                        case 2: interactionRectangle = new Rectangle((int)Position.X + Tile.TileWidth, (int)Position.Y, FrameWidth, FrameHeight); break;
                        case 3: interactionRectangle = new Rectangle((int)Position.X, (int)Position.Y - Tile.TileHeight, FrameWidth, FrameHeight); break;

                    }

                    if (Map.MapItems[i].CollisionRectangle.Intersects(interactionRectangle))
                    {

                        Map.MapItems[i].OnInteraction();
                    }
                }
            }

            if (inputHandler.IsKeyDown(Keys.D))
            {
                if (OldAnimation != 2)
                {
                    CurrentAnimation = 2;
                    CurrentFrame = 0;
                    frameSwapTimer = interval;

                }
                MoveRight();
            }
            else if (inputHandler.IsKeyDown(Keys.A))
            {
                if (OldAnimation != 1)
                {
                    CurrentAnimation = 1;
                    CurrentFrame = 0;
                    frameSwapTimer = interval;
                }
                MoveLeft();
            }
            else if (inputHandler.IsKeyDown(Keys.W))
            {
                if (OldAnimation != 3)
                {
                    CurrentAnimation = 3;
                    CurrentFrame = 0;
                    frameSwapTimer = interval;
                }
                MoveUp();
            }
            else if (inputHandler.IsKeyDown(Keys.S))
            {
                if (OldAnimation != 0)
                {
                    CurrentAnimation = 0;
                    CurrentFrame = 0;
                    frameSwapTimer = interval;
                }
                MoveDown();
            }
            OldAnimation = CurrentAnimation;
            if (!inputHandler.IsKeyDown(Keys.D) &&
                !inputHandler.IsKeyDown(Keys.A) &&
                !inputHandler.IsKeyDown(Keys.W) &&
                !inputHandler.IsKeyDown(Keys.S) &&
                !TakingDamage
               )
            {
                if (Math.Abs(Velocity.X) > 0 || Math.Abs(Velocity.Y) > 0)
                {

                    Velocity.X = 0;
                    Velocity.Y = 0;
                }
                Loop = true;
                CurrentFrame = 1;
                Loop = false;
            }
            if (inputHandler.IsKeyDown(Keys.Space))
                Running = true;
            else
                Running = false;
            foreach (BaseEquippable equip in EquipList)
                equip.Update(gameTime);
            if (inputHandler.IsKeyPressed(Keys.NumPad1))
            {
                if (EquipList[0] is BaseSword)
                    ((BaseSword)EquipList[0]).Swing();
            }

            foreach (BaseMapItem i in Map.MapItems)
            {
                if (CollisionRectangle.Intersects(i.CollisionRectangle))
                {

                    i.OnCollision();

                }
            }

            for (int y = CharacterGridY - 5; y <= CharacterGridY + 5; y++)
                for (int x = CharacterGridX - 5; x <= CharacterGridX + 5; x++)
                {
                    if (x < 0 || y < 0 || x >= Map.Grid.GetLength(1) || y >= Map.Grid.GetLength(0))
                        continue;


                    Vector2 c = new Vector2(CharacterGridX, CharacterGridY);
                    Vector2 d = new Vector2(x, y);

                    float distance = Vector2.Distance(c, d);
                   
                    if (distance <= 0)
                    {
                        distance = 1f;
                    }

                    if (!InLOS(new Point(x * Tile.TileWidth, y * Tile.TileHeight)))
                    {
                        if (Map.Layers[0].Grid[y, x].Event == 1 && Map.Layers[0].Grid[y, x].Visisted > distance)
                            Map.Layers[0].Grid[y, x].Visisted = MathHelper.Lerp(Map.Layers[0].Grid[y, x].Visisted, distance, 0.2f);

                        continue;
                    }

                    if (Map.Layers[0].Grid[y, x].Visisted > distance)
                        Map.Layers[0].Grid[y, x].Visisted = MathHelper.Lerp(Map.Layers[0].Grid[y, x].Visisted, distance, 0.2f);
                }


        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            foreach (BaseEquippable equip in EquipList)
            {
                equip.Draw(spriteBatch);
            }
            Map.stateManager.DrawRectangle(interactionRectangle.X, interactionRectangle.Y, interactionRectangle.Width, interactionRectangle.Height, spriteBatch, Color.Red);
        }

        public void Shoot()
        {

        }

        public void OnDeath()
        {
            LifeBar = new LifeBar(0, 20, this);

        }
        #endregion

    }
}
