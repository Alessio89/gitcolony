﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;

namespace Colony.Gameplay_Elements
{
    public class BaseAnimatedSprite : BaseGameplayElement, IDrawableObject
    {

        #region Field Region

        public Texture2D SpriteSheet;
        public int FrameWidth, FrameHeight;
        int framesWide, framesHigh;
        public Vector2 Velocity = new Vector2(0, 0);
        public Vector2 MaxVelocity = new Vector2(5f, 5f);
        public Vector2 MaxRunningVelocity = new Vector2(0.4f, 0.4f);
        public float frameSwapTimer = 0;
        public float interval = 400f;
        Vector2 currentFrame =  new Vector2(0,2);
        Rectangle sourceRectangle;
        public bool Loop = false;
        public int FramesPerAnimation = 3;
        public int AnimationsPerSprite = 4;
        public int Sprites;
        public int CurrentSprite = 0;
        public List<Rectangle> sourceRectangles = new List<Rectangle>();
        public Color Tint = Color.White;
        protected int OldAnimation;

        #endregion

        #region Property Region

        public int CurrentFrame { get { return (int)currentFrame.X; } set { currentFrame.X = value; } }
        public int CurrentAnimation { get { return (int)currentFrame.Y; } set { currentFrame.Y = value; } }
        public float Bottom { get { return (Position.Y + FrameHeight); } }
        public float Top { get { return (Position.Y); } }
        public float Right { get { return (Position.X + FrameWidth); } }
        public float Left { get { return Position.X; } }


        #endregion

        #region Constructor Region

        public BaseAnimatedSprite(Texture2D spriteSheet, int frameWidth, int frameHeight, int currentSprite)
        {
            CurrentSprite = currentSprite;
            SpriteSheet = spriteSheet;
            FrameWidth = frameWidth;
            FrameHeight = frameHeight;

            framesWide = SpriteSheet.Width / frameWidth;
            framesHigh = SpriteSheet.Height / frameHeight;
            
            int SpritesWide = framesWide / FramesPerAnimation;
            int SpritesHigh = framesHigh / AnimationsPerSprite;
            for (int y = 0; y < SpritesHigh; y++)
            {
                Rectangle rect = new Rectangle(0, y*(frameHeight*AnimationsPerSprite), frameWidth * FramesPerAnimation, frameHeight * AnimationsPerSprite);
                for (int x = 0; x < SpritesWide; x++)
                {
                    
                        rect.X = x * (frameWidth * FramesPerAnimation);
                        sourceRectangles.Add(rect);
                    
                }
            }
            
            sourceRectangle = new Rectangle(CurrentFrame * FrameWidth, CurrentAnimation * FrameHeight, FrameWidth, FrameHeight);
            sourceRectangle.X += sourceRectangles[CurrentSprite].X;
            sourceRectangle.Y += sourceRectangles[CurrentSprite].Y;
            
        }

        #endregion

        #region Methods Region

        public virtual void Update(GameTime gameTime)
        {
            if (Loop)
            {
                frameSwapTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

                if (frameSwapTimer >= interval)
                {
                    frameSwapTimer = 0;
                    currentFrame += new Vector2(1, 0);
                    if (currentFrame.X > FramesPerAnimation - 1)
                        currentFrame = new Vector2(0, currentFrame.Y);

                    sourceRectangle = new Rectangle(CurrentFrame * FrameWidth, CurrentAnimation * FrameHeight, FrameWidth, FrameHeight);
                    sourceRectangle.X += sourceRectangles[CurrentSprite].X;
                    sourceRectangle.Y += sourceRectangles[CurrentSprite].Y;
                }
            }
            else
            {
                CurrentFrame = 1;
                sourceRectangle = new Rectangle(CurrentFrame * FrameWidth, CurrentAnimation * FrameHeight, FrameWidth, FrameHeight);
                sourceRectangle.X += sourceRectangles[CurrentSprite].X;
                sourceRectangle.Y += sourceRectangles[CurrentSprite].Y;
            }

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            float z = Position.Y / 600;
            z /= 100;
           // FloatingText.AddFloatingText(z.ToString(), (BaseCharacter)this);
            spriteBatch.Draw(SpriteSheet,
                new Rectangle((int)Position.X, (int)Position.Y, FrameWidth, FrameHeight),
                sourceRectangle,
                Tint,0f, Vector2.Zero,SpriteEffects.None, z);
        }

        public Vector2 GetPosition()
        {
            return Position;
        }

        public int GetCenterXOnGrid()
        {
            return (int)((Position.X + FrameWidth / 2) / Gamestates.Map_Editor.Tile.TileWidth);
        }

        public int GetCenterYOnGrid()
        {
            return (int)((Position.Y + FrameHeight / 2) / Gamestates.Map_Editor.Tile.TileHeight);
        }

        
        #endregion
    }

    public interface IDrawableObject
    {
        void Draw(SpriteBatch spriteBatch);
        Vector2 GetPosition();
    }
}
