﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements.Items.Map_Items;
using Colony.Gameplay_Elements.Items;
using Colony.Gameplay_Elements.Enemies.AI;
using C3.XNA;

namespace Colony.Gameplay_Elements
{
    public class BaseCharacter : BaseAnimatedSprite
    {

        #region Field Region

        public MovementManager MovementManager;
        
        public Map Map;
        public bool IsMoving = false, IgnoreMovemenetDetection = false, Jumping = false;
        public Vector2 lastPosition;
        public int CharacterGridX;
        public int CharacterGridY;

        public string Name;
        public bool debug_Message = false;
        public bool Fighting;
        public float CollisionRadius = 0.4f;
        
        List<Vector2> collisionDetectCells = new List<Vector2>();
        public Color[] ColorData;
        public bool Running = false;
        public LifeBar LifeBar;
        public Rectangle CollisionRectangle;
        public bool TakingDamage = false;
        public Color FlashToWhenDamaged = Color.Red;
        public bool FlashingOn = true;
        float FlashTimer = 0f;

        public Rectangle feetRectangle;

        public bool PushedBack;
        public bool StopWalk;

        public List<Spells.BaseSpell> Spells = new List<Spells.BaseSpell>();
        public SpriteFont font;
        public float rotation = 0f;

        public Inventory Inventory;

        public float HitPoints, MaxHitPoints, ManaPoints, Defense, Attack, Speed, ActiveRadius;

        public Vector2 Steering = Vector2.Zero;
        public Vector2 lastVelocity;

        #endregion

        #region Constructor Region

        public BaseCharacter(Map map, Texture2D spritesheet, int frameWidth, int frameHight, int currentSprite)
            : base(spritesheet, frameWidth, frameHight, currentSprite)
        {
            Map = map;
            lastPosition = Position;
            ColorData = new Color[spritesheet.Width * spritesheet.Height];
            spritesheet.GetData(0,
                new Rectangle(CurrentFrame * frameWidth, CurrentAnimation * frameHight, frameWidth, frameHight),
                ColorData,
                CurrentFrame * CurrentAnimation,
                FrameWidth * frameHight);

            LifeBar = new LifeBar(0, 20, this);
            MovementManager = new Enemies.AI.MovementManager(this);
            feetRectangle = new Rectangle((int)Position.X+20, (int)Position.Y + FrameHeight - FrameHeight / 6, FrameWidth-10, FrameHeight / 6);
        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            LifeBar.Update(gameTime);

            // Flash the sprite if is taking damage. At the moment not working because the actors are drawn inside the batch with the Effects for the visibility
            if (TakingDamage)
            {
                FlashTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                if (FlashingOn)
                {
                    Tint = Color.Lerp(Tint, FlashToWhenDamaged, 0.1f * (float)gameTime.ElapsedGameTime.Milliseconds);
                    if (FlashTimer >= 50f)
                    {
                        FlashTimer = 0f;
                        FlashingOn = false;
                    }
                }
                else
                {
                    Tint = Color.Lerp(Tint, Color.White, 0.1f * (float)gameTime.ElapsedGameTime.Milliseconds);
                    if (FlashTimer >= 50f)
                    {
                        FlashTimer = 0f;
                        FlashingOn = true;
                        TakingDamage = false;
                        PushedBack = false;
                    }
                }
            }

            // Update the spells
            foreach (Gameplay_Elements.Spells.BaseSpell s in Spells)
            {
                s.Update(gameTime);
            }

            // Check if it is moving, if yes activate the animation loop
            if (lastPosition != Position)
                IsMoving = true;
            else
                IsMoving = false;

            if (!IgnoreMovemenetDetection && IsMoving)
                Loop = true;
            else if (!IgnoreMovemenetDetection && !IsMoving)
            {
                Loop = false;

            }

            if (IsMoving)
            {
                lastPosition = Position;
            }

            // Make sure the velocity isn't higher than the Max Velocity (either when running or walking)
            if (!Running)
            {
                if (Math.Abs(Velocity.X) > MaxVelocity.X)
                    Velocity.X = (Velocity.X > 0) ? MaxVelocity.X : -MaxVelocity.X;
                if (Math.Abs(Velocity.Y) > MaxVelocity.Y)
                    Velocity.Y = (Velocity.Y > 0) ? MaxVelocity.Y : -MaxVelocity.Y;
            }
            else
            {
                if (Math.Abs(Velocity.X) > MaxRunningVelocity.X)
                    Velocity.X = (Velocity.X > 0) ? MaxRunningVelocity.X : -MaxRunningVelocity.X;
                if (Math.Abs(Velocity.Y) > MaxRunningVelocity.Y)
                    Velocity.Y = (Velocity.Y > 0) ? MaxRunningVelocity.Y : -MaxRunningVelocity.Y;
            
            }

            CharacterGridX = (int)(Position.X) / Tile.TileWidth;
            CharacterGridY = (int)(Position.Y) / Tile.TileHeight;

            // Controlla che il rettangolo centrato sui piedi non collida con qualcosa

            feetRectangle.X = (int)Position.X+4;
            feetRectangle.Y = ((int)Position.Y + FrameHeight - FrameHeight / 6);

            for (int y = feetRectangle.Top; y <= feetRectangle.Bottom; y++)
            {
                if (!Map.IsInsideMapBoundaries(new Point(CharacterGridX, CharacterGridY)))
                    continue;
                if (Velocity.X > 0)
                {
                    
                    if (Map.IsInsideMapBoundaries(new Point(CharacterGridX, CharacterGridY))
                        && Map.IsInsideMapBoundaries(new Point((feetRectangle.Right+8)/Tile.TileWidth, y/Tile.TileHeight))
                        && (Map.Layers[0].Grid[y / Tile.TileHeight, (feetRectangle.Right + 8) / Tile.TileWidth].Event == 1 || Map.Layers[0].Grid[y / Tile.TileHeight, (feetRectangle.Right + 8) / Tile.TileWidth].Event == 4))
                    {

                        
                        Velocity.X = 0;
                        
                        break;
                    }
                }
                else if (Velocity.X < 0)
                {
                    
                    if (Map.IsInsideMapBoundaries(new Point(CharacterGridX, CharacterGridY))
                        && Map.IsInsideMapBoundaries(new Point((feetRectangle.Left-8)/Tile.TileWidth, y/Tile.TileHeight))
                        && (Map.Layers[0].Grid[y / Tile.TileHeight, (feetRectangle.Left - 8) / Tile.TileWidth].Event == 1 
                        || Map.Layers[0].Grid[y / Tile.TileHeight, (feetRectangle.Left - 8) / Tile.TileWidth].Event == 4))
                    {
                        Velocity.X = 0;
                        break;
                    }
                }
                else if (Velocity.X == 0)
                    break;
            }

            for (int x = feetRectangle.Left; x <= feetRectangle.Right; x++)
            {
                if (!Map.IsInsideMapBoundaries(new Point(CharacterGridX, CharacterGridY)))
                    continue;
                if (Velocity.Y > 0)
                {
                    if (Map.IsInsideMapBoundaries(new Point(CharacterGridX, CharacterGridY))
                        && Map.IsInsideMapBoundaries(new Point(x/Tile.TileWidth, (feetRectangle.Bottom+10)/Tile.TileHeight))
                        && (Map.Layers[0].Grid[(feetRectangle.Bottom+8)/Tile.TileHeight, x/Tile.TileWidth].Event == 1 || Map.Layers[0].Grid[(feetRectangle.Bottom+8)/Tile.TileHeight, x/Tile.TileWidth].Event == 4))
                    {
                        Velocity.Y = 0;
                        break;
                    }
                }
                else if (Velocity.Y < 0)
                {
                    if (Map.IsInsideMapBoundaries(new Point(CharacterGridX, CharacterGridY))
                        && Map.IsInsideMapBoundaries(new Point(x/Tile.TileWidth, (feetRectangle.Top-8)/Tile.TileHeight)) 
                        && (Map.Layers[0].Grid[(feetRectangle.Top - 8) / Tile.TileHeight, x / Tile.TileWidth].Event == 1 || Map.Layers[0].Grid[(feetRectangle.Top - 8) / Tile.TileHeight, x / Tile.TileWidth].Event == 4))
                    {
                        Velocity.Y = 0;
                        break;
                    }
                }
            }

            

            if (float.IsNaN(Velocity.X) || float.IsNaN(Velocity.Y) || float.IsNaN(Position.X) || float.IsNaN(Position.Y))
                return;
            Position += Velocity;// *(float)gameTime.ElapsedGameTime.Milliseconds;
            


        }

        

        

        public void AddSpell(Spells.BaseSpell spell)
        {
            Spells.Add(spell);
        }

        public void CastSpell(Type spell)
        {
            Gameplay_Elements.Spells.BaseSpell Spell = Spells.FirstOrDefault(s => s.GetType() == spell);
            if (Spell != null)
            {

                Spell.Cast();
            }
        }

        public void EndSpellCast(Type spell)
        {
            Gameplay_Elements.Spells.BaseSpell Spell = Spells.FirstOrDefault(s => s.GetType() == spell);
            if (Spell != null)
            {
                Spell.EndCast();
            }
        }

        public virtual void OnDeath(BaseCharacter killer)
        {
        }

        public void TakeDamage(BaseCharacter from, float amount, GameTime gameTime, float power = 3f)
        {

            

            //Velocity = Position - from.Position;
            //if (Velocity == Vector2.Zero)
            //    Velocity = new Vector2(1, 1);
            //Velocity.Normalize();
            //Velocity *= 3f;
            
            TakingDamage = true;
            PushedBack = true;
            HitPoints -= amount;
            FloatingText.AddFloatingText(amount.ToString(), this, Color.Orange);

           

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

           // Map.stateManager.DrawRectangle(feetRectangle, spriteBatch, Color.White);
            //Map.stateManager.DrawRectangle(CollisionRectangle, spriteBatch, Color.Red);

            

        }

        public void DrawSpells(SpriteBatch spriteBatch)
        {
            foreach (Gameplay_Elements.Spells.BaseSpell s in Spells)
            {
                s.Draw(spriteBatch);
            }
        }
        
        public void MoveRight()
        {
        
            Velocity.Y = 0;
            if (Velocity.X < 0)
                Velocity.X = 0;

            Velocity += new Vector2(0.5f, 0);

            if (Running)
                Velocity += new Vector2(0.4f, 0);


        }

        public void MoveLeft()
        {
      
            Velocity.Y = 0;
            if (Velocity.X > 0)
                Velocity.X = 0;
            Velocity -= new Vector2(0.5f, 0);
            if (Running)
                Velocity -= new Vector2(0.4f, 0);
        }

        public void MoveUp()
        {

            Velocity.X = 0;
            if (Velocity.Y > 0)
                Velocity.Y = 0;

            Velocity -= new Vector2(0f, 0.5f);
            if (Running)
                Velocity -= new Vector2(0, 0.04f);
        }
        public void MoveDown()
        {

           
            Velocity.X = 0;
            if (Velocity.Y < 0)
                Velocity.Y = 0;
            Velocity += new Vector2(0f, 0.5f);
            if (Running)
                Velocity += new Vector2(0, 0.04f);

        }
        public bool IntersectPixels(
                            Matrix transformA, int widthA, int heightA, Color[] dataA,
                            Matrix transformB, int widthB, int heightB, Color[] dataB)
        {
            // Calculate a matrix which transforms from A's local space into
            // world space and then into B's local space
            Matrix transformAToB = transformA * Matrix.Invert(transformB);

            // When a point moves in A's local space, it moves in B's local space with a
            // fixed direction and distance proportional to the movement in A.
            // This algorithm steps through A one pixel at a time along A's X and Y axes
            // Calculate the analogous steps in B:
            Vector2 stepX = Vector2.TransformNormal(Vector2.UnitX, transformAToB);
            Vector2 stepY = Vector2.TransformNormal(Vector2.UnitY, transformAToB);

            // Calculate the top left corner of A in B's local space
            // This variable will be reused to keep track of the start of each row
            Vector2 yPosInB = Vector2.Transform(Vector2.Zero, transformAToB);

            // For each row of pixels in A
            for (int yA = 0; yA < heightA; yA++)
            {
                // Start at the beginning of the row
                Vector2 posInB = yPosInB;

                // For each pixel in this row
                for (int xA = 0; xA < widthA; xA++)
                {
                    // Round to the nearest pixel
                    int xB = (int)Math.Round(posInB.X);
                    int yB = (int)Math.Round(posInB.Y);

                    // If the pixel lies within the bounds of B
                    if (0 <= xB && xB < widthB &&
                        0 <= yB && yB < heightB)
                    {
                        // Get the colors of the overlapping pixels
                        Color colorA = dataA[xA + yA * widthA];
                        Color colorB = dataB[xB + yB * widthB];

                        // If both pixels are not completely transparent,
                        if (colorA.A != 0 && colorB.A != 0)
                        {
                            // then an intersection has been found
                            return true;
                        }
                    }

                    // Move to the next pixel in the row
                    posInB += stepX;
                }

                // Move to the next row
                yPosInB += stepY;
            }

            // No intersection found
            return false;
        }
        public bool IntersetcPixels(Rectangle rectangleA, Color[] dataA, Rectangle rectangleB, Color[] dataB)
        {
            int top = Math.Max(rectangleA.Top, rectangleB.Top);
            int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
            int left = Math.Max(rectangleA.Left, rectangleB.Left);
            int right = Math.Min(rectangleA.Right, rectangleB.Right);

            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    Color colorA = dataA[(x - rectangleA.Left) + (y - rectangleA.Top) * rectangleA.Width];
                    Color colorB = dataB[(x - rectangleB.Left) + (y - rectangleB.Top) * rectangleB.Width];

                    if (colorA.A != 0 && colorB.A != 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        Color[] GetImageData(Color[] colorData, int width, Rectangle rectangle)
        {
            Color[] color = new Color[rectangle.Width * rectangle.Height];
            for (int x = 0; x < rectangle.Width; x++)
                for (int y = 0; y < rectangle.Height; y++)
                    color[x + y * rectangle.Width] = colorData[x + rectangle.X + (y + rectangle.Y) * width];
            return color;
        }

        public static void Swap<T>(ref T lhs, ref T rhs) { T temp; temp = lhs; lhs = rhs; rhs = temp; }

        public List<Point> CalculateLOS(Point target)
        {
            List<Point> returnValue = new List<Point>();
            if (float.IsNaN(Position.X) || float.IsNaN(Position.Y))
            {
                Map.stateManager.DebugText("NaN");
                return returnValue;
            }
            int x0 = (int)feetRectangle.X + (feetRectangle.Width / 2);
            int x1 = (int)target.X;// +(Tile.TileWidth / 2);
            int y0 = (int)feetRectangle.Y + (feetRectangle.Height / 2);
            int y1 = (int)target.Y;// +(Tile.TileHeight / 2);
         //   if (Vector2.Distance(Position, new Vector2(target.X, target.Y)) > 20)
           //     return returnValue;
            bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
            if (steep) { Swap<int>(ref x0, ref y0); Swap<int>(ref x1, ref y1); }
            if (x0 > x1) { Swap<int>(ref x0, ref x1); Swap<int>(ref y0, ref y1); }

            int dX = (x1 - x0), dY = Math.Abs(y1 - y0), err = (dX / 2), ystep = (y0 < y1 ? 1 : -1), y = y0;

            for (int x = x0; x <= x1; ++x)
            {
                if (returnValue.Count > 260)
                    return returnValue;
                if (steep)
                {
                    returnValue.Add(new Point(x, y));
                   // Map.Layers[0].Grid[x / Tile.TileHeight, y / Tile.TileWidth].Tint = Color.Black;
                }
                else
                {
                    returnValue.Add(new Point(y, x));
                    //Map.Layers[0].Grid[y / Tile.TileWidth, x / Tile.TileHeight].Tint = Color.Black;
                }

                err = err - dY;
                if (err < 0) { y += ystep; err += dX; }
            }

            return returnValue;
        }

        public bool InLOS(Point target)
        {
            bool returnValue = true;
            List<Point> points = CalculateLOS(target);
       
            foreach (Point p in points)
            {
               // if (!Map.IsInsideMapBoundaries(p))
                 //   continue;
                if (Map.IsInsideMapBoundaries(new Point(p.X/Tile.TileHeight, p.Y / Tile.TileWidth)) && Map.Layers[0].Grid[p.X/Tile.TileHeight, p.Y/Tile.TileWidth].Event == 1)
                {
                    returnValue = false;
                    break;
                }
            }

            
            return returnValue;
        }

        public bool InLOS(BaseCharacter target)
        {
            bool returnValue = true;
            List<Point> points = CalculateLOS(new Point((int)target.feetRectangle.X, (int)target.feetRectangle.Y));
        
            foreach (Point p in points)
            {
                if (Map.Layers[0].Grid[p.X/Tile.TileHeight, p.Y/Tile.TileWidth].Event == 1 || Map.Layers[0].Grid[p.X/Tile.TileHeight, p.Y/Tile.TileWidth].Event == 2)
                {
                    returnValue = false;
                    break;
                }
            }

            return returnValue;
        }

        #endregion
    }

    public enum Direction
    {
        Up = 0,
        Right,
        Down,
        Left
    }
}
