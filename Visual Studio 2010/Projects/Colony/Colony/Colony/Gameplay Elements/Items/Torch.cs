﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Items
{
    public class Torch : BaseEquippable, IItem
    {
        #region Field Region

        bool activated;
        #endregion

        #region Constructor Region

        public Torch(Texture2D texture, BaseCharacter owner) : base(texture, EquipLayer.RightHand, "Torcia", owner)
        {
            Sprite = texture;
            Owner = owner;
        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            
            rotation = 0f;

           
            switch (Owner.CurrentAnimation)
            {
                case 0: Position = Owner.Position + new Vector2(16, 8); rotation = 0f; break;
                case 1: Position = Owner.Position + new Vector2(-4, 12); rotation = 6.05f; break;
                case 2: Position = Owner.Position + new Vector2(26, 8); rotation = 0.8f; break;
                case 3: Position = Owner.Position + new Vector2(18, 8); rotation = 0f; break;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                
                spriteBatch.Draw(Sprite, new Rectangle((int)Position.X, (int)Position.Y, 16,16),null, Color.White, rotation, Vector2.Zero, SpriteEffects.None, 1);
            }
        }

        public new Texture2D GetTexture()
        { return Sprite; }

        public  new void Use()
        { }

        public new void Selected()
        {
            if (!activated)
            {
                Visible = true;
                Owner.Map.LightSources.Add(Owner);
                activated = true;
            }
        }

        public new void UnSelected()
        {
            Visible = false;
        }
        #endregion
    }
}
