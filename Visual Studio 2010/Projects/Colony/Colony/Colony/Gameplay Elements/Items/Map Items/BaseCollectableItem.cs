﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gui;

namespace Colony.Gameplay_Elements.Items.Map_Items
{
    public class BaseCollectableItem : BaseMapItem, IUsable, IItem
    {

        #region Field Region

        public Texture2D Sprite;
        public IItem ItemReference;
        bool TextDisplayed;

        #endregion

        #region Constructor Region

        public BaseCollectableItem(Vector2 position, string name, Texture2D graphic, Map map)
            : base(position, name, graphic, map)
        {
            
            //((BaseSword)ItemReference).Owner = map.Player;
            
               
            Sprite = graphic;
        }

        #endregion

        #region Methods Region

        public override void OnCollision()
        {
            BaseSword sword = new BaseSword(Map.stateManager.GameRef.Content.Load<Texture2D>(@"Items\sword"), EquipLayer.RightHand, "Spada", 2f, 2, Map.Player);
            
                if (Map.Player.Inventory.AddItem<BaseSword>(sword))
                {
                    Map.Player.EquipList.Add(sword);
                    FloatingText.AddFloatingText(Name, Map.Player.Position);
                    FlaggedForRemoval = true;
                }
                else
                {
                    if (!TextDisplayed)
                    {
                        TextDisplayed = true;
                        FloatingText.AddFloatingText("Inventario Pieno", Map.Player, () => { TextDisplayed = false; });
                    }
                }
            
        }

        public void Use() { }
        public Texture2D GetTexture() { return Sprite; }
        public void Selected()
        { }
        public void UnSelected() { }
        #endregion
    }
}
