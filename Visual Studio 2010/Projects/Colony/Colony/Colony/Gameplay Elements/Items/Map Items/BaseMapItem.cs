﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Items.Map_Items
{
    public class BaseMapItem : IDrawableObject
    {
        #region Field Region

        public Vector2 Position;

        public int GridX { get { return (int)Position.X / Tile.TileWidth; } set { Position.X = value * Tile.TileWidth; } }
        public int GridY { get { return (int)Position.Y / Tile.TileWidth; } set { Position.Y = value * Tile.TileHeight; } }

        public string Name;

        //public Action OnCollision;
        
        public Action OnPlace;

        public bool Walkable;

        public Texture2D Graphic;
        public Color Tint = Color.White;
        public Rectangle CollisionRectangle;

        public bool FlaggedForRemoval;

        public Map Map;
        #endregion

        #region Constructor Region

        public BaseMapItem(Vector2 position, string name, Texture2D graphic, Map map)
        {
            Map = map;
            Position = position;
            Name = name;
            Graphic = graphic;
            CollisionRectangle = new Rectangle((int)Position.X, (int)Position.Y, graphic.Width, graphic.Height);
        }

        #endregion

        #region Methods Region

        public virtual void Update(GameTime gameTime)
        {
            CollisionRectangle.X = (int)Position.X;
            CollisionRectangle.Y = (int)Position.Y;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Graphic, Position, Tint);
        }

        public virtual void OnCollision()
        { }

        public virtual void OnInteraction()
        { }

        public Vector2 GetPosition()
        {
            return Position;
        }
        #endregion
    }
}
