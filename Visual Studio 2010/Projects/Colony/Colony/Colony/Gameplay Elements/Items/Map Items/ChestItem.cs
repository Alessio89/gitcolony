﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Items.Map_Items
{
    public class ChestItem : BaseMapItem
    {

        #region Constructor Region

        public ChestItem(Vector2 position, string name, Texture2D graphic, Map map)
            : base(position, name, graphic, map)
        {
            
       
   
        }

        #endregion

        public override void OnInteraction()
        {
            base.OnInteraction();
            BaseCollectableItem i = new BaseCollectableItem(Position + new Vector2(Tile.TileWidth, 0), "Spada", Map.stateManager.GameRef.Content.Load<Texture2D>(@"Items\sword"), Map);
            Map.MapItems.Add(i);
            FlaggedForRemoval = true;
        }

    }
}
