﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Items.Map_Items
{
    public class Door : BaseMapItem
    {
        public Door LinkedDoor;
        public Door(Vector2 position, Texture2D graphic, Map map)
            : base(position, "Door", graphic, map)
        {
            Walkable = false;
        }

        public override void OnInteraction()
        {
            base.OnInteraction();
            FlaggedForRemoval = true;
            if (LinkedDoor != null)
                LinkedDoor.FlaggedForRemoval = true;
        }
    }
}
