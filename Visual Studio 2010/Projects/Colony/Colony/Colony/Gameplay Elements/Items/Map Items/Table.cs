﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Items.Map_Items
{
    public class Table : BaseMapItem
    {

        #region Constructor Region

        public Table(Vector2 position, string name, Texture2D graphic, Map map)
            : base(position, name, graphic, map)
        {
            Walkable = true;
        }

        

        #endregion

        public override void OnInteraction()
        {
            base.OnInteraction();
            FloatingText.AddFloatingText("C'e' del sangue...", Map.Player);
        }

    }
}
