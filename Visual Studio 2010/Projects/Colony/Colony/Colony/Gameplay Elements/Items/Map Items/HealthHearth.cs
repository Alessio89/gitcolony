﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Items.Map_Items
{
    public class HealthHearth : BaseCollectableItem
    {

        public HealthHearth(Vector2 position, Map map, Texture2D sprite)
            : base(position, "Health", sprite, map)
        {
            
        }

        public override void OnCollision()
        {
            

            Map.Player.HitPoints += 3f;
            FloatingText.AddFloatingText("+3", Map.Player, Color.Green);
            if (Map.Player.HitPoints > Map.Player.MaxHitPoints)
                Map.Player.HitPoints = Map.Player.MaxHitPoints;

            FlaggedForRemoval = true;
        }
    }
}
