﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using System.Diagnostics;
using Colony.Gameplay_Elements.Items.Map_Items;

namespace Colony.Gameplay_Elements.Items
{
    public class Inventory
    {

        #region Field Region

        public int MaxItems = 10;
        public BaseCharacter Owner;

        public List<InventoryItem> Items = new List<InventoryItem>();

        #endregion

        #region Constructor Region

        public Inventory(BaseCharacter owner)
        {
            Owner = owner;
        }

        #endregion

        #region Methods Region

        public int GetAmount<T>()
        {
            return (from i in Items where i.InstancesList.OfType<T>().Count() > 0 select i).Count();
        }

        public bool AddItem<T>(IItem item)
        {
           
            IEnumerable<InventoryItem> tItems = (from i in Items where i.InstancesList.OfType<T>().Count() > 0 select i);

            if (tItems.Count() <= 0)
            {
                if (Items.Count < MaxItems)
                {
                    Items.Add( new InventoryItem(item) );
                    return true;
                }

                else
                {
                    return false;
                }
            }

            else
            {

                foreach (InventoryItem i in tItems)
                {
                    if (i.Count < i.Max)
                    {
                        i.InstancesList.Add(item);
                        return true;
                    }
                }

                if (Items.Count < MaxItems)
                {
                    Items.Add(new InventoryItem(item));
                    return true;
                }
                return false;
            }

            
        }

        #endregion
    }

    public class InventoryItem
    {
        #region Field Region

        public List<IItem> InstancesList = new List<IItem>();
        public int Count { get { return InstancesList.Count; } }
        public int Max = 99;

        #endregion

        public InventoryItem(IItem firstInstance)
        {
            InstancesList.Add(firstInstance);
            if (firstInstance is BaseEquippable)
                Max = 1;
        }

    }

    public interface IItem
    {
        Texture2D GetTexture();
        void Use();
        void Selected();
        void UnSelected();
    }
}
