﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Items
{
    public class BaseSword : BaseEquippable
    {
        #region Field Region

        float Speed;
        int width = 32;
        int height = 32;
        List<GenericEnemy> HitEnemies = new List<GenericEnemy>();
        SoundEffect swosh;
        public Color[] ColorData;

        #endregion

        #region Constructor Region

        public BaseSword(Texture2D sprite, EquipLayer layer, string name, float damage, float speed, BaseCharacter owner)
            : base(sprite, layer, name, owner)
        {
            Damage = damage;
            Speed = speed;
            swosh = owner.Map.stateManager.GameRef.Content.Load<SoundEffect>(@"SoundFX\sword sound");
            //CollisionPoints.Add(new Point(32, 0));
            if (sprite != null)
            {
                ColorData = new Color[sprite.Width * sprite.Height];
                sprite.GetData(ColorData);
            }
        }
        
        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            
            if (Visible)
            {
                switch (Owner.CurrentAnimation)
                {
                    case 0:  CollisionRectangle.X = (int)Owner.Position.X; CollisionRectangle.Y = (int)Owner.Position.Y + Owner.FrameHeight; break;
                    case 1: CollisionRectangle.X = (int)Owner.Position.X - width; CollisionRectangle.Y = (int)Owner.Position.Y; break;
                    case 2:  CollisionRectangle.X = (int)Owner.Position.X + width; CollisionRectangle.Y = (int)Owner.Position.Y; break;
                    case 3:  CollisionRectangle.X = (int)Owner.Position.X; CollisionRectangle.Y = (int)Owner.Position.Y - height; break;
                }
                
                SwingTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                
                rotation += 0.1f;
           
                foreach (GenericEnemy e in Owner.Map.Enemies)
                {
                    if (e.CollisionRectangle.Intersects(CollisionRectangle))
                    {
                        Matrix eMatrix = Matrix.CreateScale(Owner.Map.Camera.Scale) *  Matrix.CreateTranslation(new Vector3(e.Position, 0f));
                        if (CollisionDetector.IntersectPixels( transformMatrix, Sprite.Width, Sprite.Height, ColorData,
                            eMatrix, e.FrameHeight, e.FrameWidth, e.ColorData))
                        {
                            if (HitEnemies.Contains(e))
                                continue;
                           e.PlayDamageSound();
                            
                            HitEnemies.Add(e);
                            e.TakeDamage(Owner, Damage, gameTime);
                            if (e.HitPoints <= 0)
                                e.Action = EnemyAction.Dieing;
                        }
                    }
                }

                if (SwingTimer >=200f || rotation >= rotation + 1.5f)
                {
                    rotation = 0f;
                    Visible = false;
                    HitEnemies.Clear();
                }
            }
        }

        public override void Use()
        {
            base.Use();
            Swing();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            //DrawRectangle(CollisionRectangle.X, CollisionRectangle.Y, CollisionRectangle.Width, CollisionRectangle.Height, spriteBatch, Color.Blue);
            //Rectangle r = rect;// CalculateBoundingRectangle(rect, transformMatrix);
            //DrawRectangle(r.X, r.Y, r.Width, r.Height, spriteBatch, Color.Red);
        }

        public void Swing()
        {
            HitEnemies.Clear();
            SwingTimer = 0f;
            Visible = true;
            swosh.Play();
            switch (Owner.CurrentAnimation)
            {
                case 0: rotation = 1.5f; CollisionRectangle.X = (int)Owner.Position.X; CollisionRectangle.Y = (int)Owner.Position.Y + Owner.FrameHeight; break;
                case 1: rotation = 3f; CollisionRectangle.X = (int)Owner.Position.X - width; CollisionRectangle.Y = (int)Owner.Position.Y; break;
                case 2: rotation = 0f; CollisionRectangle.X = (int)Owner.Position.X + width; CollisionRectangle.Y = (int)Owner.Position.Y; break;
                case 3: rotation = 4.5f; CollisionRectangle.X = (int)Owner.Position.X; CollisionRectangle.Y = (int)Owner.Position.Y - height; break;
            }

        }

        public void Init()
        {
            ColorData = new Color[Sprite.Width * Sprite.Height];
            Sprite.GetData(ColorData);
        }

        #endregion
    }
}
