﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using C3.XNA;

namespace Colony.Gameplay_Elements.Items
{
    public class LongSword : BaseSword
    {

        public LongSword(BaseCharacter owner)
            : base(null, EquipLayer.RightHand, "Long Sword", 0.5f, 2f, owner)
        {
            Sprite = owner.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Items\sword1");
            Init();
        }

    }
}
