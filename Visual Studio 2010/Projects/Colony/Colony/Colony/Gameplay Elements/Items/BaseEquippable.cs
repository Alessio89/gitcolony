﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gui;

namespace Colony.Gameplay_Elements.Items
{

    public enum EquipLayer
    {
        RightHand = 0,
        LeftHand,
        Head,
        Arms,
        Torso,
        Legs,
        Gloves
    }

    public interface IEquippable
    {

    }

    public class BaseEquippable : IEquippable, IUsable, IItem
    {

        #region Field Region

        public EquipLayer EquipLayer;
        public string Name;
        public Texture2D Sprite;
        public BaseCharacter Owner;
        protected bool Visible;
        protected float SwingTimer = 0f;
        public float rotation = 0f;
        public Rectangle CollisionRectangle;
        public float Damage;
        public Rectangle rect;
        Texture2D debugTexture;
        public Vector2 Position;
        public Matrix transformMatrix;

        #endregion

        #region Construtor Region

        public BaseEquippable(Texture2D sprite, EquipLayer layer, string name, BaseCharacter owner)
        {
            Sprite = sprite;
            EquipLayer = layer;
            Name = name;
            Owner = owner;
            CollisionRectangle = new Rectangle((int)Owner.Position.X + 32, (int)Owner.Position.Y + 19, 32, 32);
            debugTexture = owner.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Gui\1pixelwhite");
        }

        #endregion

        #region Methods Region

        public virtual void Update(GameTime gameTime)
        {
            rect = new Rectangle((int)Owner.Position.X+Owner.FrameWidth/2, (int)Owner.Position.Y+Owner.FrameHeight/2, 32, 32);
            Position.X = rect.X;
            Position.Y = rect.Y;
            //CollisionRectangle = rect;
            transformMatrix = Matrix.Invert(Matrix.CreateTranslation(new Vector3(0, Sprite.Height, 0))) *
                Matrix.CreateRotationZ(rotation) * Matrix.CreateScale(Owner.Map.Camera.Scale) *
                Matrix.CreateTranslation(new Vector3(rect.X, rect.Y, 0));
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            
            if (Visible)
                spriteBatch.Draw(Sprite, rect, null, Color.White, rotation, new Vector2(0, Sprite.Height), SpriteEffects.None, 0);
            
        }

       

        public void DrawRectangle(int topLeftX, int topLeftY, int width, int height, SpriteBatch spritebatch, Color color)
        {
            spritebatch.Draw(debugTexture, new Rectangle(topLeftX, topLeftY, width, 1), color);
            spritebatch.Draw(debugTexture, new Rectangle(topLeftX, topLeftY, 1, height), color);
            spritebatch.Draw(debugTexture, new Rectangle((topLeftX) + width, topLeftY , 1, height), color);
            spritebatch.Draw(debugTexture, new Rectangle(topLeftX, (topLeftY) + height, width, 1), color);
        }

        public virtual void Use()
        { }

        public Texture2D GetTexture()
        { return Sprite; }

        public void Selected()
        {
            //FloatingText.AddFloatingText("Asd", Owner);
        }

        public void UnSelected() { }

        #endregion

    }
}
