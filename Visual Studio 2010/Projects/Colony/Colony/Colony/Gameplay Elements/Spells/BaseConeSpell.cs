﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;


namespace Colony.Gameplay_Elements.Spells
{
    public class BaseConeSpell : BaseSpell
    {

        #region Field Region

        float Damage;
        float Area;
        Color color1;
        Color color2;
        public string Texture = @"Images\particle";

        #endregion

        #region Constructor Region

        public BaseConeSpell(string name, SpellElements element, BaseCharacter caster, float damage, float area, Color color1, Color color2)
            : base(name, SpellElements.Fire, caster)
        {
            Damage = damage;
            Area = area;
            this.color1 = color1;
            this.color2 = color2;
          
        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

        }

        public override void Cast()
        {
            base.Cast();
            SfxManager.Cone(Caster, color1, color2, 0.2f, 300, true, 0.9f, Texture, true);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
        #endregion

    }
}
