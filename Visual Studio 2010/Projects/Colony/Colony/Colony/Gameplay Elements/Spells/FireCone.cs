﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;


namespace Colony.Gameplay_Elements.Spells
{
    public class FireCone : BaseConeSpell
    {

        #region Field Region

        
        
        

        #endregion

        #region Constructor Region

        public FireCone(BaseCharacter caster, float damage, float area) : base("Fire Cone", SpellElements.Fire, caster, damage, area, Color.Purple, Color.Purple)
        {
            Icon = caster.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Projectiles\fireball");
        }

        #endregion

        #region Methods Region
        public override void Cast()
        {
            SfxManager.TestEffect(Caster, Color.Red, Color.Yellow, 200, 1f);
        }
        #endregion

    }
}
