﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;


namespace Colony.Gameplay_Elements.Spells
{
    public class IceCone : BaseConeSpell
    {

        #region Field Region

        #endregion

        #region Constructor Region

        public IceCone(BaseCharacter caster, float damage, float area)
            : base("Ice Cone", SpellElements.Ice, caster, damage, area, Color.Blue, Color.WhiteSmoke)
        {
            Icon = caster.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Spell Icons\icecone");
            Texture = @"Particles\rune";
        }

        #endregion

        #region Methods Region

        #endregion

    }
}

