﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;

namespace Colony.Gameplay_Elements.Spells
{
    public class testspell : BaseSpell
    {
        public BaseCharacter target;
        public testspell(BaseCharacter caster) : base( "test", SpellElements.Ice, caster)
        {
            Icon = caster.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Projectiles\fireball");
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            ps.Position = Caster.Position;
        }

        public override void Cast()
        {

            Projectile p = new Projectile(Caster.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Projectiles\fireball"), 0.1f, null, Caster, Caster.Map, true);
            if (target != null)
                new Projectile(Caster.Map.stateManager.GameRef.Content.Load<Texture2D>(@"Projectiles\fireball"), 0.1f, target, Caster, Caster.Map, false, false, true);
            p.Traveling = true;
            p.Visible = true;
           // SfxManager.Cone(p, Color.Red, Color.Orange, 0.1f, 50, true, 0.36f);
            
            p.Width = 32;
            p.Height = 32;
            p.Damage = 1f;
            p.Distance = 100f;
            p.Position = Caster.Position + new Vector2(Caster.FrameWidth / 2, Caster.FrameHeight / 2);
            p.initialPos = p.Position;
            
            switch (Caster.CurrentAnimation)
            {
                case 0: p.Direction = new Vector2(0, 1); p.rotation = 1.5f; break;
                case 1: p.Direction = new Vector2(-1, 0); p.rotation = 3f; break;
                case 2: p.Direction = new Vector2(1, 0); p.rotation = 0f; break;
                case 3: p.Direction = new Vector2(0, -1); p.rotation = 4.5f; break;
            }
            p.Position += p.Direction * 20;
            SfxManager.Explosion(p.initialPos + (p.Direction *20), Color.White, Color.WhiteSmoke, 5, 0.2f);
                
            Caster.Map.Projectiles.Add(p);   
        }
    }
}
