﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using Colony.Gui;
using Colony.Gameplay_Elements.Items;

namespace Colony.Gameplay_Elements.Spells
{

    public enum SpellElements
    {
        Fire = 0,
        Water,
        Ice,
        Poision,
        Electricity,
        Air
    }

    public class BaseSpell : IUsable, IItem
    {

        #region Field Region

        public bool IsProjectile;
        public SpellElements Element;
        public BaseCharacter Caster;
        public string Name;
        public bool Casting;
        public Particle_System.ParticleSystem ps;
        public float Duration = 3000f;
        public float SpellTimer = 0f;
        public Texture2D Icon;

        #endregion

        #region Constructor Region

        public BaseSpell(string name, SpellElements element, BaseCharacter caster, bool isprojectile = false)
        {
            IsProjectile = isprojectile;
            Element = element;
            Caster = caster;
            Name = name;
            ps = new Particle_System.ParticleSystem(caster.Position, caster.Map);
        }


        #endregion

        #region Methods Region

        public virtual void Update(GameTime gameTime)
        {
            if (Casting)
            {
                ps.Update(gameTime);
                SpellTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                if (SpellTimer >= Duration)
                {
                    SpellTimer = 0f;
                    Casting = false;
                }
            }
            else
                return;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (Casting)
                ps.Draw(spriteBatch, 1, Vector2.Zero);
            else
                return;
        }

        public virtual void Cast()
        {
            Casting = true;
            
            
            
        }
        public virtual void EndCast()
        {
            Casting = false;
            ps.Clear();
        }

        public Texture2D GetTexture()
        {
            return Icon;
        }

        public void Use()
        {
            Cast();
        }

        public void Selected()
        { }

        public void UnSelected() { }

        #endregion

    }
}
