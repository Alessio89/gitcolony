﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using Colony.Gui;
using Colony.Gameplay_Elements.Items;

namespace Colony.Gameplay_Elements.Spells
{
    public interface ITargetableSpell
    {
        void SetTarget(BaseCharacter target);
    }

    public class Fireball : BaseSpell, ITargetableSpell
    {
        public BaseCharacter target;

        public Fireball(BaseCharacter caster)
            : base("Fireball", SpellElements.Fire, caster, true)
        {
        }

        public override void Cast()
        {
            base.Cast();
            bool damagePlayer = (Caster is GenericEnemy);
            bool damageEnemy = (Caster is PlayerCharacter);
            Projectile p = new Projectile(
                            Caster.Map.stateManager.Game.Content.Load<Texture2D>(@"Projectiles\fireball"),
                            0.1f, target, Caster, Caster.Map, true,damageEnemy, damagePlayer);

            p.Traveling = true;
            p.Visible = true;
            
            p.FollowTarget = false;
            p.Width = 32;
            p.Height = 32;
            p.Damage = 4f;
            p.Distance = (Caster is GenericEnemy) ? (Caster as GenericEnemy).ShootingRange : 10f;
            p.Position = Caster.Position + new Vector2(Caster.FrameWidth / 2, Caster.FrameHeight / 2);
            p.initialPos = p.Position;
            p.Tint = Color.Red;
            p.Direction = target.Position - Caster.Position;
            p.Direction.Normalize();
            p.rotation = (float)Math.Atan2(target.Position.Y - Caster.Position.Y, target.Position.X - Caster.Position.X);
            //switch (Caster.CurrentAnimation)
            //{
            //    case 0: p.Direction = new Vector2(0, 1); p.rotation = 1.5f; break;
            //    case 1: p.Direction = new Vector2(-1, 0); p.rotation = 3f; break;
            //    case 2: p.Direction = new Vector2(1, 0); p.rotation = 0f; break;
            //    case 3: p.Direction = new Vector2(0, -1); p.rotation = 4.5f; break;
            //}
            //p.Position += p.Direction * 20;
            SfxManager.Explosion(p.initialPos + (p.Direction * 20), Color.White, Color.WhiteSmoke, 5, 0.2f);
            p.OnHit = () =>
            {
                SfxManager.Explosion(p.Position, Color.Red, Color.Orange, 50, 1f);
            };
            Caster.Map.Projectiles.Add(p);   
                        
        }

        public void SetTarget(BaseCharacter target)
        { this.target = target; }

    }
}
