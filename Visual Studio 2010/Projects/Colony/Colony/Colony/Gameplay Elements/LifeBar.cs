﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;


namespace Colony.Gameplay_Elements
{
    public class LifeBar : BaseGameplayElement
    {

        #region Field Region

        public float Max, Min, Current;
        Color EmptyColor = Color.Red;
        Color FillColor = Color.Green;
        BaseCharacter AttachedTo;

        #endregion

        #region Constructor Region

        public LifeBar(float min, float max, BaseCharacter link)
        {
            Max = link.FrameWidth;
            Min = 0;
            AttachedTo = link;
            Current = AttachedTo.HitPoints;
            ContentManager content = link.Map.stateManager.GameRef.Content;
            Graphics = content.Load<Texture2D>(@"Gui\1pixelwhite");
        }

        #endregion

        #region Methods Region

        public virtual void Update(GameTime gameTime)
        {
            Current = (AttachedTo.HitPoints * Max) / AttachedTo.MaxHitPoints;
            Position = AttachedTo.Position + new Vector2(0, -8);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Graphics, new Rectangle((int)Position.X, (int)Position.Y, (int)Max, 5), EmptyColor);
            spriteBatch.Draw(Graphics, new Rectangle((int)Position.X, (int)Position.Y, (int)Current, 5), FillColor);
            AttachedTo.Map.stateManager.DrawRectangle((int)Position.X, (int)Position.Y, (int)Max, 5, spriteBatch, new Color(0,0,0,255));
        }

        public void Decrease(float amount)
        {
            Current -= amount;
        }

        #endregion
    }
}
