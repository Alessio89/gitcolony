﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;


namespace Colony.Gameplay_Elements
{

    public class BaseGameplayElement
    {

        #region Field Region

        public Texture2D Graphics;
        public bool Visible, UseCameraMatrix = false;
        public Vector2 Position;

        #endregion

        #region Constructor Region
        #endregion

        #region Methods Region
        #endregion

    }
}
