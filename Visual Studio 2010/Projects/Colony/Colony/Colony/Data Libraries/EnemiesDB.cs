﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements.Items.Map_Items;
using Colony.Gameplay_Elements.Items;
using Colony.Gameplay_Elements.Enemies;
using Colony.Gameplay_Elements;
using C3.XNA;

namespace Colony
{
    public static class EnemiesDB
    {
        public static List<Type> EnemyTypes = new List<Type>();

        public static List<Type> Slimes = new List<Type>();

        public static void Init()
        {
            EnemyTypes.Add(typeof(Slime));
            EnemyTypes.Add(typeof(PoisonSlime));
            EnemyTypes.Add(typeof(Flowing));

            Slimes.Add(typeof(Slime));
            Slimes.Add(typeof(PoisonSlime));
        }
    }
}
