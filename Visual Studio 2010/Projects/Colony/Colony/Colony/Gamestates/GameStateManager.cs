﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Colony.Gamestates
{
    public class GameStateManager : DrawableGameComponent
    {
        #region Field Region

        private Game1 gameRef;
        int screenWidth;
        int screenHeight;
        List<BaseGameState> gameStates = new List<BaseGameState>();
        bool NewState = false;
        List<BaseGameState> screensToChange = new List<BaseGameState>();
        bool RemoveState = false;
        Texture2D RectangleTexture;

        #endregion

        #region Property Region

        public Game1 GameRef { get { return gameRef; } }
        public int ScreenWidth { get { return screenWidth; } private set { screenWidth = value; } }
        public int ScreenHeight { get { return screenHeight; } private set { screenHeight = value; } }
        public List<BaseGameState> GameStates { get { return gameStates; } set { gameStates = value; } }

        #endregion

        #region Constructor Region

        public GameStateManager(Game gameRef, int screenWidth, int screenHeight)
            : base(gameRef)
        {
            this.gameRef = gameRef as Game1;
            this.screenHeight = screenHeight;
            this.screenWidth = screenWidth;
        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (NewState)
            {
                foreach (BaseGameState state in screensToChange)
                {
                    GameStates.Add(state);
                    state.LoadContents();
                    Console.WriteLine("State added {0}", state);
                }
                screensToChange = new List<BaseGameState>();
                NewState = false;
            }
            if (RemoveState)
            {
                GameStates.Remove(GameStates[GameStates.Count - 1]);
                GameStates[GameStates.Count - 1].HasFocus = true;
                RemoveState = false;
            }
            for (int i = 0; i < GameStates.Count; i++)
            {
                if (GameStates[i].FlaggedForRemoval)
                {
                    GameStates.Remove(GameStates[i]);
                    i--;
                    continue;
                }
                if (GameStates[i].HasFocus)
                    GameStates[i].Update(gameTime);
            }
        }
        public override void Initialize()
        {
            base.Initialize();

        }
        protected override void LoadContent()
        {
            foreach (BaseGameState state in GameStates)
            {
                state.LoadContents();
            }
            RectangleTexture = gameRef.Content.Load<Texture2D>(@"Gui\1pixelwhite");
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            foreach (BaseGameState state in GameStates)
            {

                state.Draw(gameTime);
            }
        }

        public void PushToStack(BaseGameState state)
        {
            state.stateManager = this;
            screensToChange.Add(state);
            NewState = true;
        }

        public void RemoveLastState()
        {
            RemoveState = true;
        }



        public void DrawRectangle(int topLeftX, int topLeftY, int width, int height, SpriteBatch spritebatch, Color color, bool fill = false)
        {
            spritebatch.Draw(RectangleTexture, new Rectangle(topLeftX, topLeftY, width, 1), color);
            spritebatch.Draw(RectangleTexture, new Rectangle(topLeftX, topLeftY, 1, height), color);
            spritebatch.Draw(RectangleTexture, new Rectangle((topLeftX) + width, topLeftY, 1, height), color);
            spritebatch.Draw(RectangleTexture, new Rectangle(topLeftX, (topLeftY) + height, width, 1), color);
            if (fill)
            {
                for (int y = topLeftY; y < topLeftY + height; y++)
                    for (int x = topLeftX; x < topLeftX + width; x++)
                        spritebatch.Draw(RectangleTexture, new Rectangle(x, y, width, 1), color);
            }
        }

        public void DrawRectangle(Rectangle rect, SpriteBatch spritebatch, Color color)
        {
            int topLeftX = rect.Left;
            int topLeftY = rect.Top;
            int width = rect.Width;
            int height = rect.Height;
            spritebatch.Draw(RectangleTexture, new Rectangle(topLeftX, topLeftY, width, 1), color);
            spritebatch.Draw(RectangleTexture, new Rectangle(topLeftX, topLeftY, 1, height), color);
            spritebatch.Draw(RectangleTexture, new Rectangle((topLeftX) + width, topLeftY, 1, height), color);
            spritebatch.Draw(RectangleTexture, new Rectangle(topLeftX, (topLeftY) + height, width, 1), color);
        }

        public BaseGameState GetStateByType(Type t)
        {
            if ((from s in GameStates where s.GetType() == t select s).Count() > 0)
            {
                return (from s in GameStates where s.GetType() == t select s).First();
            }
            else
                return null;
        }

        public void DebugText(string text)
        {
            DebugConsole d = ((DebugConsole)GetStateByType(typeof(DebugConsole)));
            if (d != null)
                d.DebugText(text);
        }

        #endregion
    }
}
