﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using Colony.Gameplay_Elements.Items;
using Colony.Gameplay_Elements.Enemies;
using Colony.Gui;
using Colony.Gui.Gameplay_Interface;
using Colony.InputHandler;
using Colony.Particle_System;
using Colony.Gameplay_Elements.Spells;
using X2DPE;
using X2DPE.Helpers;
using Colony.Gamestates.States;
using Colony.Gameplay_Elements.Enemies.AI;

namespace Colony.Gamestates
{
    public class MiniMap : BaseGameState
    {
        #region Field Region
        Map Map;
        Vector2 Position;
        RenderTarget2D minimap;
        Texture2D m;
        Texture2D bg;
        SpriteBatch spriteBatch;
        GraphicsDevice device;
        Camera2D camera;
        Texture2D pixel;
        Effect effect;
        Texture2D mask;
        GamePlayState g;
        Vector2 pos;

        float updateTimer = 0f;

        int TilesWide = 8;
        int TilesHigh = 8;
        int TileWidth = 10;
        int TileHeight = 10;

        List<Vector4> cells = new List<Vector4>();

        // x e- y -> grid x e y
        // z e w -> posizione su schermo

        #endregion

        #region Constructor Region

        public MiniMap(Map map, Game game, GameStateManager manager)
            : base(game, manager)
        {
            screenWidth = 200;
            screenHeight = 200;
            Map = map;
            Position = new Vector2(100, 300);
            var pp = stateManager.GameRef.GraphicsDevice.PresentationParameters;
            minimap = new RenderTarget2D(stateManager.GameRef.GraphicsDevice, 800, 600);//,false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
            spriteBatch = stateManager.GameRef.spriteBatch;
            device = stateManager.GameRef.GraphicsDevice;
            bg = game.Content.Load<Texture2D>(@"Gui\iteminventorygui");
            camera = new Camera2D(game);
            camera.Position = Vector2.Zero;
            camera.Scale = 1f;
            camera.Initialize();
            pixel = game.Content.Load<Texture2D>(@"Gui\1pixelwhite");

            effect = game.Content.Load<Effect>(@"Shaders\Effect1");
            g = stateManager.GetStateByType(typeof(GamePlayState)) as GamePlayState;
            m = game.Content.Load<Texture2D>(@"Images\mask");

        }


        #endregion

        #region Methods Region


        public override void Update(GameTime gameTime)
        {
            if (!stateManager.GameRef.NewMapLoaded)
                return;
            base.Update(gameTime);
            device.SetRenderTarget(minimap);
            device.Clear(Color.Black);

            cells.Clear();
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, effect, Matrix.CreateScale(1f));

            Point playerGrid = new Point(Map.Player.CharacterGridX, Map.Player.CharacterGridY);
            int xpos = 0;
            int ypos = 0;
            int tempy = 0;
            for (int y = playerGrid.Y - 15; y <= playerGrid.Y + 30; y++)
            {

                if (y < 0 || y >= Map.Grid.GetLength(0) - 1)
                    continue;
                ypos++;
                for (int x = playerGrid.X - 15; x <= playerGrid.X + 30; x++)
                {
                    effect.CurrentTechnique = effect.Techniques["FogOfWar"];
                    if (x < 0 || x >= Map.Grid.GetLength(1) - 1)
                        continue;
                    if (tempy != y)
                    {
                        xpos = 0;
                        tempy = y;
                    }
                    xpos++;
                    effect.Parameters["visibility"].SetValue(Map.Layers[0].Grid[y, x].Visisted);
                    effect.CurrentTechnique.Passes[0].Apply();
                    spriteBatch.Draw(Map.tilesets[(int)Map.Layers[0].Grid[y, x].TilesetID].TileSet,
                                                       new Rectangle(xpos * TileWidth, ypos * TileHeight, TileWidth, TileHeight),
                                                       Map.tilesets[(int)Map.Layers[0].Grid[y, x].TilesetID].rectangles[(int)Map.Layers[0].Grid[y, x].TileID],
                                                       Color.White);

                    cells.Add(new Vector4(x, y, xpos * TileWidth, ypos * TileHeight));
                    effect.CurrentTechnique = effect.Techniques[0];
                    





                }



            }
            spriteBatch.End();

            spriteBatch.Begin();
            Vector4 playerPos = Vector4.Zero;
            if ((from c in cells where (c.X == Map.Player.CharacterGridX && c.Y == Map.Player.CharacterGridY) select c).Count() > 0)
                playerPos = (from c in cells where (c.X == Map.Player.CharacterGridX && c.Y == Map.Player.CharacterGridY) select c).First();
            

            spriteBatch.Draw(pixel, new Rectangle((int)playerPos.Z, (int)playerPos.W, 8, 8), Color.Green);

            foreach (GenericEnemy e in Map.Enemies)
            {
                if (Map.Camera.IsInView(e.Position, new Vector2(32, 32)))
                {
                    IEnumerable<Vector4> v = (from c in cells where (c.X == e.CharacterGridX && c.Y == e.CharacterGridY) select c);
                    if (v.Count() > 0)
                    {
                        Vector4 enemyPos = v.First();
                        spriteBatch.Draw(pixel, new Rectangle((int)enemyPos.Z, (int)enemyPos.W, 8, 8), Color.Red);
                    }
                }
            }
            spriteBatch.End();


            /* */


            device.SetRenderTarget(null);


        }

        public override void Draw(GameTime gameTime)
        {
            if (!stateManager.GameRef.NewMapLoaded)
                return;
            base.Draw(gameTime);

            spriteBatch.Begin();
            spriteBatch.Draw(bg, new Rectangle(800-210-5, 10, 210, 210), Color.White);
            spriteBatch.Draw((Texture2D)minimap, new Rectangle(800-210+1, 15, 200, 200), Color.White);

            spriteBatch.End();

        }
        #endregion
    }
}

/*
            
            */