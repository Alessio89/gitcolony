﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using X2DPE;
using X2DPE.Helpers;
using RamGecXNAControls;
using RamGecXNAControls.ExtendedControls;
using Microsoft.Xna.Framework.Storage;

namespace Colony.Gamestates.States
{
    public class GameIntroState : BaseGameState
    {

        #region Field Region

        Texture2D background, newGame, loadGame, quitGame;
        SoundEffect Intro;

        float fade = 0f;
        bool fadeIn = true;

        Particle_System.ParticleSystem ps = new Particle_System.ParticleSystem(Vector2.Zero, null);
        ParticleComponent p;
        Emitter emitter, emitter2;
        Texture2D mouseIcon;
        Vector2 emitterVelocity = new Vector2(1, 0);
        Random rnd = new Random();
        Map map;
        RenderTarget2D map_RenderTarget;
        int selected = 0;
        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public GameIntroState(Game game, GameStateManager manager)
            : base(game, manager)
        {

        }

        #endregion

        #region Methods Region

        public override void LoadContents()
        {
            background = Game.Content.Load<Texture2D>(@"AlphaTest\title");
            Intro = Game.Content.Load<SoundEffect>(@"Music\introsong");
            
            newGame = Game.Content.Load<Texture2D>(@"AlphaTest\newgame");
            loadGame = Game.Content.Load<Texture2D>(@"AlphaTest\loadgame");
            quitGame = Game.Content.Load<Texture2D>(@"AlphaTest\quit");
            mouseIcon = Game.Content.Load<Texture2D>(@"Mouse Icons\pointer");

            map = new Map(new GridCell[100, 100], stateManager, new List<Tileset>() { new Tileset(Game.Content, @"Tilesets\dungeontileset2") });
            map.Camera = new Camera2D(Game);
            map.Camera.Initialize();
            map.Camera.Position = new Vector2(0, 0);
            map.Camera.Scale = 0.1f;
            int[,] tempg = map.WalkThroughDungeon();
            PGCDungeon.Interpret(DungeonType.Labyrinth, tempg, map);
            map_RenderTarget = new RenderTarget2D(stateManager.GameRef.GraphicsDevice, 800, 600);
            stateManager.GameRef.NewMapLoaded = true;

            SoundEffectInstance introMusicInstanced = Intro.CreateInstance();
            introMusicInstanced.IsLooped = true;

            introMusicInstanced.Play();
           
            p = new ParticleComponent(Game);

            p.Initialize();
            Game.IsMouseVisible = false;

            emitter = new Emitter()
             {
                 Active = true,
                 TextureList = new List<Texture2D>()
                 {
                     Game.Content.Load<Texture2D>(@"Images\particle")
                 },
                 RandomEmissionInterval = new RandomMinMax(12.0d),
                 ParticleLifeTime = 2000,
                 ParticleDirection = new RandomMinMax(0, 359),
                 ParticleSpeed = new RandomMinMax(0.05f, 0.1f),
                 ParticleRotation = new RandomMinMax(0, 100),
                 RotationSpeed = new RandomMinMax(0.015f),
                 ParticleFader = new ParticleFader(true, true, 2000),
                 ParticleScaler = new ParticleScaler(true, 1.3f)
             };



            emitter2 = new Emitter()
            {
                Active = true,
                TextureList = new List<Texture2D>()
		{
			Game.Content.Load<Texture2D>("Images\\particle")
		 },
                RandomEmissionInterval = new RandomMinMax(35.0d),
                ParticleLifeTime = 1000,
                ParticleDirection = new RandomMinMax(0, 359),
                ParticleSpeed = new RandomMinMax(0.05f, 0.1f),
                ParticleRotation = new RandomMinMax(0, 100),
                RotationSpeed = new RandomMinMax(0.015f),
                ParticleFader = new ParticleFader(true, true, 2000),
                ParticleScaler = new ParticleScaler(true, 1.3f)
            };
            emitter2.Position = new Vector2(200, 200);

        }


        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            emitter.Position += new Vector2((rnd.Next(3) - 1) * 10, (rnd.Next(3) - 1) * 10);
            emitterVelocity = new Vector2((rnd.Next(3) - 1) * 10, (rnd.Next(3) - 1) * 10);

            emitter2.Position += emitterVelocity;
            emitter2.Position = Vector2.Clamp(emitter2.Position, Vector2.Zero, new Vector2(800, 600));
            emitter.Position = Vector2.Clamp(emitter.Position, Vector2.Zero, new Vector2(800, 600));
            p.Update(gameTime);
            if (fadeIn)
            {
                fade += 0.02f;
                if (fade > 1)
                    fadeIn = false;
            }
            else
            {
                fade -= 0.02f;
                if (fade < 0.1f)
                    fadeIn = true;
            }
            int mx = Mouse.GetState().X;
            int my = Mouse.GetState().Y;

            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.Enter))
            {
                ChangeScreen();
            }

            if (mx > 400 - newGame.Width / 2 - 5
                    && my > 300
                    && mx < (400 - newGame.Width / 2 - 5) + newGame.Width
                    && my < 300 + newGame.Height)
            {
                if (selected != 1)
                    fade = 1f;
                selected = 1;

                if (stateManager.GameRef.InputHandler.LeftClick())
                {


                    ChangeScreen();
                }
            }
            else if (mx > 400 - loadGame.Width / 2 - 5
                && my > 350
                && mx < (400 - loadGame.Width / 2 - 5) + loadGame.Width
                && my < 350 + loadGame.Height)
            {
                if (selected != 2)
                    fade = 1f;
                selected = 2;
                if (stateManager.GameRef.InputHandler.LeftClick())
                {
                    fade = 0f;
                }
            }
            else if (mx > 400 - quitGame.Width / 2 - 5
                && my > 400
                && mx < (400 - quitGame.Width / 2 - 5) + quitGame.Width
                && my < 400 + quitGame.Height)
            {
                if (selected != 3)
                    fade = 1f;
                selected = 3;
                if (stateManager.GameRef.InputHandler.LeftClick())
                    stateManager.GameRef.Exit();
            }
            else
                selected = 0;
            map.Camera.Position += new Vector2(5, 0);
            SpriteBatch s = stateManager.GameRef.spriteBatch;

            stateManager.GameRef.GraphicsDevice.SetRenderTarget(map_RenderTarget);
            stateManager.GameRef.GraphicsDevice.Clear(Color.Transparent);
            int cy = (int)map.Camera.Position.Y / Tile.TileHeight;
            int cx = (int)map.Camera.Position.X / Tile.TileWidth;

            stateManager.GameRef.GraphicsDevice.SetRenderTarget(null);


        }

        public void ChangeScreen()
        {
            FlaggedForRemoval = true;
            List<Tileset> tilesets = new List<Tileset>() { new Tileset(stateManager.Game.Content, @"Tilesets\dungeontileset2") };
            Map m = new Map(new GridCell[30,30], stateManager, tilesets);
            Pathfinding.mapHeight = 100;
            Pathfinding.mapWidth = 100;
            Pathfinding.map = m;
            m.AddLayer();
            m.Layers[0].SetAsFloor(new GridCell(-1, 0, 0, m));
            FloatingText.Map = m;
            m.Camera.Scale = 0.1f;
            PGCDungeon.BSPGeneration(ref m);
            stateManager.PushToStack(new GamePlayState(stateManager.Game, stateManager, m));
            stateManager.PushToStack(new DebugConsole(stateManager.Game, stateManager));
            stateManager.PushToStack(new MiniMap(m, stateManager.Game, stateManager));
            stateManager.PushToStack(new LoadingScreen(stateManager.GameRef, stateManager));

        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            SpriteBatch s = stateManager.GameRef.spriteBatch;
            stateManager.GameRef.spriteBatch.Begin();
            stateManager.GameRef.spriteBatch.Draw(background, new Rectangle(
                0, 0, screenWidth, screenHeight), Color.White);
            if (selected == 1)
                s.Draw(newGame, new Vector2(400 - newGame.Width / 2 - 5, 300), Color.White * fade);
            else
                s.Draw(newGame, new Vector2(400 - newGame.Width / 2 - 5, 300), Color.White);

            if (selected == 2)
                s.Draw(loadGame, new Vector2(400 - loadGame.Width / 2 - 5, 350), Color.White * fade);
            else
                s.Draw(loadGame, new Vector2(400 - loadGame.Width / 2 - 5, 350), Color.White);

            if (selected == 3)
                s.Draw(quitGame, new Vector2(400 - quitGame.Width / 2 - 5, 400), Color.White * fade);
            else
                s.Draw(quitGame, new Vector2(400 - quitGame.Width / 2 - 5, 400), Color.White);
            p.Draw(gameTime, s);

            s.Draw(mouseIcon, new Vector2(Mouse.GetState().X, Mouse.GetState().Y), Color.White);
            //s.Draw(map_RenderTarget, new Rectangle(0, 200, 400,300),  Color.White);
            stateManager.GameRef.spriteBatch.End();
        }

        #endregion

    }
}
