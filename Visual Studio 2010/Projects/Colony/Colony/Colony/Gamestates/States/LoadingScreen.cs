﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using X2DPE;
using X2DPE.Helpers;
using RamGecXNAControls;
using RamGecXNAControls.ExtendedControls;
using Microsoft.Xna.Framework.Storage;

namespace Colony.Gamestates.States
{
    public class LoadingScreen : BaseGameState
    {
        int loadingdots = 0;
        float loadingdotsDelay = 100f;
        float loadingDotsTimer = 0f;

        public int maxDots = 5;

        public LoadingScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            loadingDotsTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
            if (loadingDotsTimer > loadingdotsDelay)
            {
                loadingDotsTimer = 0f;
                loadingdots++;
                if (loadingdots > maxDots)
                    loadingdots = 0;
            }
        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            SpriteBatch s = stateManager.GameRef.spriteBatch;
            string dots = "";
            for (int i = 0; i <= loadingdots; i++)
                dots += ".";
            s.Begin();
            s.DrawString(FloatingText.Font, "Loading new map"+dots, new Vector2(300, 300), Color.White);
            s.End();
        }

    }
}
