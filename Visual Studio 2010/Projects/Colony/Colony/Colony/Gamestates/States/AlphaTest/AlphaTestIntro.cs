﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using X2DPE;
using X2DPE.Helpers;
using RamGecXNAControls;
using RamGecXNAControls.ExtendedControls;
using Microsoft.Xna.Framework.Storage;


namespace Colony.Gamestates.States
{
    public class AlphaTestIntro : BaseGameState
    {

        Texture2D IntroText;

        public bool FadeIn = false;
        public bool FadeOut = false;
        public float FadeInTime = 2000f;
        float FadeOutTime = 10000f;

        float skipTimer = 0f;

        public Color FadingColor = Color.White * 0f;
        float Life = 0f;

        public AlphaTestIntro(Game game, GameStateManager manager)
            : base(game, manager)
        {
            IntroText = game.Content.Load<Texture2D>(@"AlphaTest\introtext");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (stateManager.GameRef.InputHandler.IsKeyDown(Keys.Space))
            {
                skipTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
                if (skipTimer >= 400f)
                {
                    ChangeScreen();
                }
            }
            else
                skipTimer = 0f;
            
            Life += (float)gameTime.ElapsedGameTime.Milliseconds;

            if (Life <= FadeInTime && Life < FadeOutTime)
            {
                FadingColor = Color.Lerp(FadingColor, Color.White, 0.02f);
                if (FadingColor.R > 200)
                    FadingColor = Color.White;
            }

            else if (Life >= FadeOutTime)
            {
                FadingColor = Color.Lerp(FadingColor, Color.White * 0f, 0.02f);
            }

            if (Life >= 14000f)
            {

                ChangeScreen();
                
            }
        }

        public void ChangeScreen()
        {
            FlaggedForRemoval = true;
            
            stateManager.PushToStack(new GameIntroState(stateManager.GameRef, stateManager));
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            SpriteBatch s = stateManager.GameRef.spriteBatch;
            
            s.Begin();
            s.Draw(IntroText, Vector2.Zero, FadingColor);
            s.End();
        }
    }
}
