﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using Colony.Gameplay_Elements.Items;
using Colony.Gameplay_Elements.Enemies;
using Colony.Gui;
using Colony.Gui.Gameplay_Interface;
using Colony.InputHandler;
using Colony.Particle_System;
using Colony.Gameplay_Elements.Spells;
using X2DPE;
using X2DPE.Helpers;
using Colony.Gameplay_Elements.Enemies.AI;
using RamGecXNAControls;
using RamGecXNAControls.ExtendedControls;
using C3.XNA;
using System.Threading;


namespace Colony.Gamestates.States
{
    public class GamePlayState : BaseGameState
    {

        #region Field Region

        // The current Map
        public Map CurrentMap;

        // The player character
        public PlayerCharacter Player;

        // Shader effects
        Effect testEffect;
        Vector4 AmbientLight = new Vector4(1, 1, 1, 0);
        float ambientPower = -0.2f;
        Texture2D lightMask;
        RenderTarget2D lightsTarget;
        RenderTarget2D mapTarget;

        public Texture2D mapTexture;
        public RenderTarget2D mainTarget;
        float lightWidth = 150;
        float lightHeight = 150;
        bool lightFading = true;
        bool SoundOn = true;
        // Old mouse state (?)
        MouseState oldState;

        // Sound effects
        SoundEffect fireball;

        // Generic Font (?)
        SpriteFont GenericFont;

        Inventory playerInventory;
        FrameRateCounter fps;

        public Song dungSong;
        public Song fightSong;
        public TimeSpan dungSongPosition;

        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public GamePlayState(Game game, GameStateManager manager, Map startingMap)
            : base(game, manager)
        {
            CurrentMap = startingMap;
            fps = new FrameRateCounter(manager.Game);
            fps.spriteFont = manager.Game.Content.Load<SpriteFont>(@"Fonts\Candara");
            manager.GameRef.NewMapLoaded = false;
        }

        #endregion

        #region Methods Region

        public override void LoadContents()
        {

            // Loading assets and initializing stuff.

            // WARNING! Order of initialization DOES MATTER! DO NOT CHANGE IF YOU DON'T KNOW WHAT YOU'RE DOING.

            // Generic Font load (?)
            GenericFont = stateManager.GameRef.Content.Load<SpriteFont>(@"Fonts\font");

            SfxManager.Initialize(CurrentMap);
            FloatingText.Map = CurrentMap;
            FloatingText.Initialize();
            FloatingText.Font = GenericFont;

            ActionBar.Initialize(stateManager.GameRef.Content.Load<Texture2D>(@"Gui\actionbargui"), stateManager.GameRef);

            dungSong = stateManager.GameRef.Content.Load<Song>(@"Music\dung");
            fightSong = stateManager.GameRef.Content.Load<Song>(@"Music\Forge");
            MediaPlayer.Volume = 0.2f;
            MediaPlayer.Play(dungSong);

            // Player character initialization
            Player = new PlayerCharacter(CurrentMap,
                stateManager.GameRef.Content.Load<Texture2D>(@"Spritesheets\player3"),
                32,
                32,
                0);
            Player.interval = 120f;
            FireCone fcone = new FireCone(Player, 1f, 1f);
            IceCone icone = new IceCone(Player, 1f, 1f);
            testspell testspell = new Gameplay_Elements.Spells.testspell(Player);

            Player.AddSpell(testspell);
            Player.AddSpell(icone);
            Player.AddSpell(fcone);
            CurrentMap.Player = Player;

            // Camera initialization
            CurrentMap.Camera.Initialize();
            CurrentMap.Camera.Scale = 1f;
            CurrentMap.Camera.Position = Player.Position;

            // Shader Effects initialization
            lightMask = stateManager.GameRef.Content.Load<Texture2D>(@"Images\lightmask");
            testEffect = stateManager.GameRef.Content.Load<Effect>(@"Shaders\Effect1");

            var pp = stateManager.GameRef.GraphicsDevice.PresentationParameters;
            lightsTarget = new RenderTarget2D(stateManager.GameRef.GraphicsDevice,
                pp.BackBufferWidth, pp.BackBufferHeight);
            mainTarget = new RenderTarget2D(stateManager.GameRef.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight);
            mapTarget = new RenderTarget2D(stateManager.Game.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight);

            // Sound effects loading
            fireball = stateManager.GameRef.Content.Load<SoundEffect>(@"SoundFX\foom_0");

            // Old mouse state init (?)
            oldState = Mouse.GetState();


            /*for (int i = 0; i < Player.EquipList.Count; i++)
                ActionBar.UsableItems.Add(Player.EquipList[i]);
            for (int i = 0; i < Player.Spells.Count; i++)
                ActionBar.UsableItems.Add(Player.Spells[i]);
            */
            playerInventory = new Inventory(Player);
            Player.Inventory = playerInventory;
            foreach (BaseEquippable e in Player.EquipList)
                Player.Inventory.AddItem<BaseEquippable>(e);
            ThreadPool.QueueUserWorkItem(new WaitCallback((o) => { LoadMap(); }));
           
            //while (!LoadedMap)
            //{
            //    continue;
            //}


        }
        public bool LoadedMap = false;
        void LoadMap()
        {
            int[,] tempgrid = CurrentMap.BSPGeneration();
            PGCDungeon.Interpret(DungeonType.Labyrinth, tempgrid, CurrentMap);
            stateManager.GameRef.NewMapLoaded = true;
        }

        public override void Update(GameTime gameTime)
        {
            if (!stateManager.GameRef.NewMapLoaded)
                return;
            else
            {
                if (stateManager.GetStateByType(typeof(LoadingScreen)) != null)
                    stateManager.GetStateByType(typeof(LoadingScreen)).FlaggedForRemoval = true;
            }

            base.Update(gameTime);
            fps.Update(gameTime);
            /// Update order:
            /// 1. Camera
            /// 2. Map
            /// 3. Input handling
            /// 4. Lights position and glow effect
            /// 5. SFX

            // 1. Camera
            CurrentMap.Camera.Position = Player.Position;
            CurrentMap.Camera.Update(gameTime);


            // 2. Map
            CurrentMap.Update(gameTime);

            // 3. Input handling
            oldState = Mouse.GetState();

            // Numpad4: Test the SpecialEffectsManager class
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.NumPad4))
            {
                //stateManager.PushToStack(new MiniMap(CurrentMap, Game, stateManager));
                for (int y = 0; y < CurrentMap.Grid.GetLength(0); y++)
                    for (int x = 0; x < CurrentMap.Grid.GetLength(1); x++)
                    {
                        CurrentMap.Layers[0].Grid[y, x].Visisted = 1f;



                    }
                // stateManager.GameRef.particleComponent.particleEmitterList.Add(emitter);
            }

            stateManager.GameRef.particleComponent.Update(gameTime);
            if (!SoundOn)
                MediaPlayer.Volume = 0f;
            // NumPad6: inventory test
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.NumPad6))
            {
              
            }

            FloatingText.Update(gameTime);

            // NumPad5: Spawn test dummy
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.NumPad5))
            {


                Slime s = new Slime(CurrentMap);
                s.Position = Player.Position + new Vector2((CurrentMap.rnd.Next(3) - 1) * 3, (CurrentMap.rnd.Next(3) - 1) * 3);
                s.MaxHitPoints = 10f;
                s.HitPoints = 10f;
                //s.Debug = true;
                s.AI = new Generic_AI(s);
                s.Action = EnemyAction.Thinking;
                s.ThinkingSpeed = 50;
                s.MaxVelocity = new Vector2(2f, 2f);

                CurrentMap.Enemies.Add(s);
                if (stateManager.GetStateByType(typeof(DebugConsole)) != null)
                    ((DebugConsole)stateManager.GetStateByType(typeof(DebugConsole))).DebugText("Creato nuovo nemico");

            }
            // NumPad7: inventory test
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.NumPad7))
            {
                if ((from s in stateManager.GameStates where s.GetType() == typeof(InventoryGui) select s).Count() <= 0)
                {
                    stateManager.PushToStack(new InventoryGui(playerInventory, stateManager.GameRef, stateManager));
                    HasFocus = false;
                }
            }


            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.NumPad9))
            {
                if ((from s in stateManager.GameStates where s.GetType() == typeof(SpellSelectionGui) select s).Count() <= 0)
                {
                    stateManager.PushToStack(new SpellSelectionGui(Player, stateManager.GameRef, stateManager));
                    HasFocus = false;
                }

            }
            // Minus key: Generate a dungeon using Digging Agent
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.OemMinus))
            {
                Window window = new Window(new Rectangle(0, 0, 100, 100));
                stateManager.GameRef.GuiManager.Controls.Add(window);
            }
            // Plus Key: Generate a dungeon using BSP
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.OemPlus))
            {
                stateManager.PushToStack(new LoadingScreen(stateManager.GameRef, stateManager));
                stateManager.GameRef.NewMapLoaded = false;
                ThreadPool.QueueUserWorkItem(new WaitCallback((o) => { LoadMap(); }));
                return;
            }
            // NumPad2: Cast a test spell (for testing spell effects)
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.NumPad2))
            {
                CurrentMap.ShowAutoTile = !CurrentMap.ShowAutoTile;
            }
            // Releasing NumPad2: End the casting
            else
                Player.EndSpellCast(typeof(testspell));
            // NumPad3: Cast an Ice Cone spell
            if (stateManager.GameRef.InputHandler.IsKeyDown(Keys.NumPad3))
            {
                Player.CastSpell(typeof(IceCone));
            }
            // Releasing NumPad3: Stop casting
            else
                Player.EndSpellCast(typeof(IceCone));

            // 4. Lights position and glowing effect
            if (lightFading)
            {
                lightWidth = MathHelper.Lerp(lightWidth, 150, 0.01f * (float)gameTime.ElapsedGameTime.Milliseconds);
                lightHeight = MathHelper.Lerp(lightHeight, 150, 0.01f * (float)gameTime.ElapsedGameTime.Milliseconds); ;
            }
            else
            {
                lightWidth = MathHelper.Lerp(lightWidth, 200, 0.01f * (float)gameTime.ElapsedGameTime.Milliseconds);
                lightHeight = MathHelper.Lerp(lightHeight, 200, 0.01f * (float)gameTime.ElapsedGameTime.Milliseconds);
            }

            if (lightHeight <= 150 || lightWidth <= 150)
            {
                lightFading = false;
            }
            else if (lightHeight >= 200 || lightWidth >= 200)
            {
                lightFading = true;
            }

            // 5. SFX
            SfxManager.Update(gameTime);

            ActionBar.Update(gameTime);

            DrawingManager.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (!stateManager.GameRef.NewMapLoaded)
                return;
            SpriteBatch spriteBatch = stateManager.GameRef.spriteBatch;
            GraphicsDevice device = stateManager.GameRef.GraphicsDevice;
            stateManager.GameRef.GraphicsDevice.Clear(Color.Black);

            /// Draw Order:
            /// 1. Draw to render target for lightning effects
            /// 2. Draw map to Main render target
            /// 3. Draw light and main render target on the screen
            /// 4. Draw Actors
            /// 5. Draw Spell effects

            // 1. Draw to render target for lightning effects
            device.SetRenderTarget(lightsTarget);
            device.Clear(Color.White);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive, SamplerState.LinearClamp, DepthStencilState.None, null, null, CurrentMap.Camera.Transform);

            foreach (Projectile p in CurrentMap.Projectiles)
            {
                if (p.Light)
                {
                    spriteBatch.Draw(lightMask, new Rectangle((int)p.Position.X - (int)lightWidth / 2, (int)p.Position.Y - (int)lightHeight / 2, (int)lightWidth, (int)lightHeight), Color.White);
                }
            }

            foreach (BaseCharacter light in CurrentMap.LightSources)
                spriteBatch.Draw(lightMask, new Rectangle((int)light.Position.X - (int)lightWidth / 2, (int)light.Position.Y - (int)lightHeight / 2, (int)lightWidth, (int)lightHeight), Color.White);

            spriteBatch.End();
            ////////////////////////////////////

            // 2. Draw map to main render target
            device.SetRenderTarget(mainTarget);
            device.Clear(Color.Black);

            //testEffect.CurrentTechnique = testEffect.Techniques["AmbientColor"];
            //testEffect.Parameters["param1"].SetValue(AmbientLight);
            //testEffect.Parameters["ambientPower"].SetValue(ambientPower);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, CurrentMap.Camera.Transform);

            


            CurrentMap.Draw(spriteBatch);
            //testEffect.CurrentTechnique.Passes[0].Apply();

            spriteBatch.End();
            ///////////////////////////////////

            spriteBatch.Begin(SpriteSortMode.Immediate, null, SamplerState.PointClamp, null, null, null, CurrentMap.Camera.Transform);
            DrawingManager.Draw(gameTime);
            foreach (GenericEnemy e in CurrentMap.Enemies)
            {
                if (!e.Debug)
                    continue;
                Primitives2D.DrawRectangle(spriteBatch, e.feetRectangle, Color.Red);


            }
            Primitives2D.DrawRectangle(spriteBatch, CurrentMap.Player.feetRectangle, Color.Blue);
            spriteBatch.End();

            // mapTexture = (Texture2D)mainTarget;

            // 3. Draw light and main render target on to the screen
            device.SetRenderTarget(null);
            device.Clear(Color.Black);

            testEffect.CurrentTechnique = testEffect.Techniques["Technique1"];

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

            testEffect.Parameters["lightMask"].SetValue(lightsTarget);
            testEffect.CurrentTechnique.Passes[0].Apply();

            spriteBatch.Draw(mainTarget, Vector2.Zero, Color.White);

            spriteBatch.End();
            //////////////////////////////////

            // 4. Draw Actors
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.LinearClamp, DepthStencilState.None, null, null, CurrentMap.Camera.Transform);
            testEffect.CurrentTechnique = testEffect.Techniques["FogOfWar"];
           
           

            //Player.Draw(spriteBatch);
            //Player.LifeBar.Draw(spriteBatch);*/
            Player.LifeBar.Draw(spriteBatch);
            foreach (GenericEnemy e in CurrentMap.Enemies)
            {
                if (!CurrentMap.IsInsideMapBoundaries(new Point(e.CharacterGridX, e.CharacterGridY)))
                    continue;
                if (!CurrentMap.Camera.IsInView( e.Position, new Vector2(32,32)))
                    continue;
                if (CurrentMap.Layers[0].Grid[e.CharacterGridY, e.CharacterGridX].Visisted < 4f)
                    e.LifeBar.Draw(spriteBatch);
            }
            FloatingText.Draw(spriteBatch);
            spriteBatch.End();
            //////////////////////////////////////////

            // 5. Draw spell effects
            testEffect.CurrentTechnique = testEffect.Techniques["ParticleLights"];

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive, SamplerState.LinearClamp, DepthStencilState.None, null, null, CurrentMap.Camera.Transform);
            //testEffect.CurrentTechnique.Passes[0].Apply();
            Player.DrawSpells(spriteBatch);
            SfxManager.Draw(spriteBatch);
            stateManager.GameRef.particleComponent.Draw(gameTime, spriteBatch);

            spriteBatch.End();

            spriteBatch.Begin();
            ActionBar.Draw(spriteBatch);
            spriteBatch.End();

            fps.Draw(gameTime);

            spriteBatch.Begin();
            stateManager.GameRef.GuiManager.Draw(spriteBatch);
            spriteBatch.End();
            ////////////////////////////////////////
        }

        #endregion

    }
}
