﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Colony.Gamestates
{
    public class BaseGameState : DrawableGameComponent
    {

        #region Field Region

        public GameStateManager stateManager;
        protected int screenWidth;
        protected int screenHeight;
        public bool HasFocus = true;
        public bool FlaggedForRemoval;


        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public BaseGameState(Game game, GameStateManager manager)
            : base(game)
        {
            
            stateManager = manager;
            screenHeight = stateManager.ScreenHeight;
            screenWidth = stateManager.ScreenWidth;
        }

        #endregion

        #region Methods Region

        public virtual void LoadContents()
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        public void Close()
        {
            FlaggedForRemoval = true;
        }

        #endregion
    }
}
