﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;

namespace Colony.Gamestates.Map_Editor
{
    public class Tileset
    {
        public int tilesHigh;
        public int tilesWide;
        public Texture2D TileSet;
        public Rectangle[] rectangles;
        public string TilesetAssetname;

        public Tileset(ContentManager loader, string assetName)
        {
            TileSet = loader.Load<Texture2D>(assetName);
            TilesetAssetname = assetName;
            tilesHigh = TileSet.Height / Tile.TileHeight;
            tilesWide = TileSet.Width / Tile.TileWidth;

            rectangles = new Rectangle[tilesHigh * tilesWide];

            int tile = 0;

            for (int y = 0; y < tilesHigh; y++)
            {
                for (int x = 0; x < tilesWide; x++)
                {
                    rectangles[tile] = new Rectangle(x * Tile.TileWidth, y * Tile.TileHeight, Tile.TileWidth, Tile.TileHeight);
                    tile++;
                }
            }
        }

    }
}
