﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.InputHandler;

namespace Colony.Gui
{
    public class BaseGuiElement
    {

        #region Field Region

        protected string text;
        protected Color color = Color.White;
        protected Color drawedColor = Color.White;
        protected Color highlight_color;
        protected int width;
        protected int height;
        public Vector2 Position;
        public SpriteFont font;
        protected Texture2D image;
        public bool Visible = true;
        protected Gamestates.GameStateManager Manager;
        public bool HasFocus = false;
        protected bool DebugMode = false;

        #endregion

        #region Constructor Region

        public BaseGuiElement(Gamestates.GameStateManager manager)
        {
            Manager = manager;
        }

        #endregion

        #region Methods Region

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            
        }

        protected bool IsMouseInsideThis()
        {
            int mouseX = Mouse.GetState().X;
            int mouseY = Mouse.GetState().Y;

            if (mouseX > Position.X &&
                mouseX < Position.X + width &&
                mouseY > Position.Y &&
                mouseY < Position.Y + height)
            {
                return true;
            }

            return false;
        }

        protected void GetFocusAt(int xMin, int yMin, int xMax, int yMax)
        {
            int mouseXinGui = Mouse.GetState().X - (int)Position.X;
            int mouseYinGui = Mouse.GetState().Y - (int)Position.Y;

            if (mouseXinGui > xMin && mouseXinGui < xMax && mouseYinGui > yMin && mouseYinGui < yMax)
            {
                HasFocus = true;
                if (DebugMode)
                    Console.WriteLine("Debug: Focus preso");
            }
        }
        protected void LoseFocusAt(int xMin, int yMin, int xMax, int yMax)
        {
            int mouseXinGui = Mouse.GetState().X - (int)Position.X;
            int mouseYinGui = Mouse.GetState().Y - (int)Position.Y;

            if (mouseXinGui < xMin || mouseXinGui > xMax || mouseYinGui < yMin || mouseYinGui > yMax)
            {
                HasFocus = false;
            }
        }

        public Vector2 GetPosition()
        {
            return Position;
        }

        #endregion

    }
}
