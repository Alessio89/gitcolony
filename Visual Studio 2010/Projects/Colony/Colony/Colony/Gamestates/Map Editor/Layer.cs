﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;


namespace Colony.Gamestates.Map_Editor
{
    public class Layer
    {
        public GridCell[,] Grid;

        public Layer(Map map)
        {
            Grid = new GridCell[map.Grid.GetLength(0), map.Grid.GetLength(1)];
            for (int y = 0; y < Grid.GetLength(0); y++)
                for (int x = 0; x < Grid.GetLength(1); x++)
                {
                    Grid[y, x] = new GridCell(-1, -1, -1, map);
                    Grid[y, x].X = x;
                    Grid[y, x].Y = y;
                }
        }

        public void SetAsFloor(GridCell tileID)
        {
            for (int y = 0; y < Grid.GetLength(0); y++)
                for (int x = 0; x < Grid.GetLength(1); x++)
                    Grid[y, x].TileID = tileID.TileID;

        }
    }
}
