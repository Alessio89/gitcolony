﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gui;

// TODO: Mostrare come BaseGuiElement

namespace Colony.Gamestates.Map_Editor
{
    public class MapSaveDialog : BaseGameState
    {

        #region Field Region

        Texture2D background;
        SpriteFont font;
        string tempString = "";
        string fileName = "";
        MapEditorState mapEditor;

        #endregion

        #region Constructor Region

        public MapSaveDialog(Game game, GameStateManager manager, MapEditorState mapeditor)
            : base(game, manager)
        {
            mapEditor = mapeditor;
        }

        #endregion

        #region Methods Region

        public override void LoadContents()
        {
            background = stateManager.GameRef.Content.Load<Texture2D>(@"Gui\menuBg");
            font = stateManager.GameRef.Content.Load<SpriteFont>("menuFont");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            InputHandler.InputHandler inputHandler = stateManager.GameRef.InputHandler;

            char k;
            if (KeysManager.TryConvertKeyboardInput(inputHandler, out k))
                tempString += k;

            if (inputHandler.IsKeyPressed(Keys.Back) && tempString.Length > 0)
            {
                tempString = tempString.Remove(tempString.Length - 1);
            }

            if (inputHandler.IsKeyPressed(Keys.Escape))
            {
                stateManager.RemoveLastState();
            }

            if (inputHandler.IsKeyPressed(Keys.Enter))
            {
                if (tempString != "")
                {

                    if (File.Exists(@"Maps\" + tempString + ".xml") && tempString != "Sovrascrivere?")
                    {
                        fileName = tempString;
                        tempString = "Sovrascrivere?";
                        return;
                    }
                    else if (File.Exists(@"Maps\" + fileName + ".xml") && tempString == "Sovrascrivere?")
                    {
                        mapEditor.SaveMap(fileName);
                        stateManager.RemoveLastState();
                    }
                    else
                    {
                        mapEditor.SaveMap(tempString);
                        stateManager.RemoveLastState();
                    }

                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            stateManager.GameRef.spriteBatch.Begin();
            stateManager.GameRef.spriteBatch.Draw(background, new Rectangle(250, 200, 300, 100), Color.White);
            stateManager.GameRef.spriteBatch.DrawString(font, "Salva mappa con nome:", new Vector2(260, 210), Color.White);
            stateManager.GameRef.spriteBatch.DrawString(font, tempString, new Vector2(270, 250), Color.Blue);
            stateManager.GameRef.spriteBatch.End();
        }
        #endregion

    }

}
