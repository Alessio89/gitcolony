﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gui;
using Colony.Gameplay_Elements;

// TODO: Migliorare collisioni?
//       Carica altri tileset
//       Load & Save map salvano nome tileset
//       Migliorare dungeon PGC


namespace Colony.Gamestates.Map_Editor
{


    public class MapEditorState : BaseGameState
    {

        #region Field Region

        // DA ASSEGNARE
        PlayerCharacter animatedSprite;
        Vector2 animatedSpriteSpawn = new Vector2(0, 0);
        Gameplay_Elements.Enemies.Slime enemy, enemy2;
        
        

        // Tilesets List: Keeps track of the loaded tilesets
        public List<Tileset> tilesets = new List<Tileset>();
        Tileset tileset;

        // Selected Elements: Keeps track of the selected tileset, tile and layer
        public int SelectedTileSet = 0;
        public int SelectedLayer = 0;
        public GridCell SelectedTile = new GridCell(0, 0, 0, null);

        // Gui Field: All the menus classes
        Menu menu, eventMenu;
        TilesList tileslist;
        SpriteFont font;
        Texture2D modalScreen, menuBg;
        MapEditorToolSelection ToolSelect;
        Texture2D whitepixel;
        GenericTextInput textInput;
        List<BaseGuiElement> Menus = new List<BaseGuiElement>();

        // Map class
        public Map map;
        int[,] EventsGrid;

        // History Stacks, for Undo and Redo
        Stack<ActionData> History = new Stack<ActionData>();
        Stack<ActionData> ForwardHistory = new Stack<ActionData>();

        // The current Action for the Left Click mouse button
        public ClickAction CurrentAction = ClickAction.Paint;
        Texture2D brushIcon, fillIcon, eraseIcon, eventsIcon;

        // map.Camera class
        

        #endregion

        #region Constructor Region

        public MapEditorState(Game game, GameStateManager manager, Tileset tileset)
            : base(game, manager)
        {
            this.tileset = tileset;
            tilesets.Add(tileset);
            map = new Map(new GridCell[50, 50], manager, tilesets);
            EventsGrid = new int[map.Grid.GetLength(0), map.Grid.GetLength(1)];
            map.Layers.Add(new Layer(map));
            map.Layers[0].SetAsFloor(new GridCell(-1, 0, 0, map));
            

        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {

            base.Update(gameTime);

            // Update map.Camera position
            map.Camera.Update(gameTime);

            // Disable the Event selection tool if it's not selected
            if (CurrentAction != ClickAction.Events)
                eventMenu.CanOpen = false;

            // Make sure the selected layer isn't greater than the layers list. This should never
            // happen, but one never knows ;)
            if (SelectedLayer > map.Layers.Count - 1)
                SelectedLayer = map.Layers.Count - 1;

            // Clone the Game input handler instance for easier management
            InputHandler.InputHandler inputHandler = stateManager.GameRef.InputHandler;

            // Yet to assign
            if (inputHandler.IsKeyPressed(Keys.D0))
            {
                tileslist.ChangeTileset(0);
            }
            if (inputHandler.IsKeyPressed(Keys.D1))
            {
                tileslist.ChangeTileset(1);
            }

            // Handle the directional arrow keys: if the Tile and Tileset selection tool isn't active, it moves the map.Camera.
            // If it is active, it moves the Tile and Tileset selection tool. (For big tilesets)
            if (inputHandler.IsKeyDown(Keys.Down))
            {
                if (!tileslist.HasFocus)
                    map.Camera.Focus += new Vector2(0, 2);
                else
                    tileslist.Position += new Vector2(0, 2);
            }
            else if (inputHandler.IsKeyDown(Keys.Left))
            {
                if (!tileslist.HasFocus)
                    map.Camera.Focus += new Vector2(-2, 0);
                else
                    tileslist.Position += new Vector2(-2, 0);
            }
            else if (inputHandler.IsKeyDown(Keys.Right))
            {
                if (!tileslist.HasFocus)
                    map.Camera.Focus += new Vector2(2, 0);
                else
                    tileslist.Position += new Vector2(2, 0);

            }
            else if (inputHandler.IsKeyDown(Keys.Up))
            {
                if (!tileslist.HasFocus)
                    map.Camera.Focus += new Vector2(0, -2);
                else
                    tileslist.Position += new Vector2(0, -2);
            }

            // + and - keys to zoom in and out of the map. Holding Left Control speeds the zooming
            if (inputHandler.IsKeyDown(Keys.OemPlus) && map.Camera.Scale < 3f)
                map.Camera.Scale += 0.01f;
            else if (inputHandler.IsKeyDown(Keys.OemMinus) && map.Camera.Scale > 0.3f)
                map.Camera.Scale -= 0.01f;
            else if (inputHandler.IsKeyDown(Keys.OemPlus) && map.Camera.Scale < 3f && inputHandler.IsKeyDown(Keys.LeftControl))
                map.Camera.Scale += 0.05f;
            else if (inputHandler.IsKeyDown(Keys.OemMinus) && map.Camera.Scale > 0.3f && inputHandler.IsKeyDown(Keys.LeftControl))
                map.Camera.Scale -= 0.05f;

            // Tools quick selection: F1, F2, F3,...
            if (inputHandler.IsKeyPressed(Keys.F1))
                CurrentAction = ClickAction.Paint;
            else if (inputHandler.IsKeyPressed(Keys.F2))
                CurrentAction = ClickAction.Erase;
            else if (inputHandler.IsKeyPressed(Keys.F3))
                CurrentAction = ClickAction.Fill;

            // Save and Load shortcuts: Ctrl+S for save, Ctrl+L for load
            if (inputHandler.IsKeyDown(Keys.LeftControl) && inputHandler.IsKeyPressed(Keys.S))
                AskForSave();
            else if (inputHandler.IsKeyDown(Keys.LeftControl) && inputHandler.IsKeyPressed(Keys.L))
                AskForLoad();

            // History keys: Ctrl+z for Undo, Ctrl+y for Redo
            if (inputHandler.IsKeyDown(Keys.LeftControl) && inputHandler.IsKeyPressed(Keys.Z))
            {
                if (History.Count <= 0)
                    return;
                ActionData lastAction = History.Pop();
                ForwardHistory.Push(new ActionData(lastAction.gridX, lastAction.gridY, map.Layers[lastAction.Layer].Grid[lastAction.gridY, lastAction.gridX], lastAction.Layer));
                map.Layers[lastAction.Layer].Grid[lastAction.gridY, lastAction.gridX] = lastAction.OldTile;

            }
            else if (inputHandler.IsKeyDown(Keys.LeftControl) && inputHandler.IsKeyPressed(Keys.Y))
            {
                if (ForwardHistory.Count <= 0)
                    return;
                ActionData forwardAction = ForwardHistory.Pop();
                History.Push(new ActionData(forwardAction.gridX, forwardAction.gridY, map.Layers[forwardAction.Layer].Grid[forwardAction.gridY, forwardAction.gridX], forwardAction.Layer));
                map.PlaceTile(forwardAction.gridX, forwardAction.gridY, forwardAction.OldTile, forwardAction.Layer);

            }

            // Mouse Left Click: Does something based on the selected tool, see LeftClickAction method for reference
            if (inputHandler.LeftClickHold())
            {
                LeftClickAction();
            }

            // Update each Gui element
            foreach (BaseGuiElement gui in Menus)
                gui.Update(gameTime);

            // Da assegnare

            
                animatedSprite.Update(gameTime);
            if (inputHandler.IsKeyPressed(Keys.V))
            {
                foreach (GenericEnemy e in map.Enemies)
                    e.GetToTarget(animatedSprite);
            }
            
            foreach (GenericEnemy e in map.Enemies)
                e.Update(gameTime);
            
            if (inputHandler.IsKeyPressed(Keys.L))
            {
                int[,] tempgrid = map.GenerateDungeon(12, 2, 5, 3, 4);
                PGCDungeon.Interpret(DungeonType.Labyrinth, tempgrid, map);
            }

            if (inputHandler.IsKeyPressed(Keys.M))
            {
                enemy.Position = animatedSprite.Position;
            }
            //if (inputHandler.IsKeyPressed(Keys.J))
            //{
            //    Gameplay_Elements.Enemies.Slime e = new Gameplay_Elements.Enemies.Slime(map, stateManager.Game.Content.Load<Texture2D>(@"Spritesheets\Monster2"), 32, 32,2);
            //    e.Position = new Vector2(30, 30);
            //    e.Debug = true;
            //    e.Tint = Color.Red;
            //    map.Enemies.Add(e);
            //    e.Action = EnemyAction.Idle;
            //}
            
            map.Camera.Position = animatedSprite.Position;

        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            SpriteBatch spriteBatch = stateManager.GameRef.spriteBatch;

            /// In this batch, everything will be drawn using the map.Camera.Transform matrix
            /// This batch should be exclusive for the map, grid and events calls
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.LinearClamp, DepthStencilState.None, null, null, map.Camera.Transform);

            map.Draw(spriteBatch);
            if (CurrentAction == ClickAction.Events)
            {
                foreach (Layer layer in map.Layers)
                {
                    for (int y = 0; y < map.Grid.GetLength(0); y++)
                        for (int x = 0; x < map.Grid.GetLength(1); x++)
                            spriteBatch.DrawString(font, layer.Grid[y, x].Event.ToString(), new Vector2(x * Tile.TileWidth, y * Tile.TileHeight), Color.Red);
                }
            }

            animatedSprite.Draw(spriteBatch);
            foreach (GenericEnemy e in map.Enemies)
                e.Draw(spriteBatch);
            spriteBatch.End();

            /// In this batch, everything will be drawn as it is created. No matrix used
            /// This batch should contains every Gui element such as menus, dialogs and selection tools
            spriteBatch.Begin();

            Vector2 selectedGrid = GridCellUnderMouse();
            if (CurrentAction == ClickAction.Fill || CurrentAction == ClickAction.Paint)
                spriteBatch.Draw(tilesets[SelectedTileSet].TileSet, new Vector2(Mouse.GetState().X - 16, Mouse.GetState().Y - 16), tilesets[SelectedTileSet].rectangles[(int)SelectedTile.TileID], Color.White, 0, new Vector2(0, 0), 1f, SpriteEffects.None, 0);

            foreach (BaseGuiElement gui in Menus)
                gui.Draw(spriteBatch);

            if (!HasFocus)
                spriteBatch.Draw(modalScreen, new Rectangle(0, 0, 800, 600), Color.White * 0.3f);

            if (CurrentAction == ClickAction.Erase)
                spriteBatch.Draw(eraseIcon, new Rectangle(Mouse.GetState().X, Mouse.GetState().Y - 25, 32, 32), Color.White);
            else if (CurrentAction == ClickAction.Fill)
                spriteBatch.Draw(fillIcon, new Rectangle(Mouse.GetState().X, Mouse.GetState().Y - 16, 32, 32), Color.White);
            else if (CurrentAction == ClickAction.Paint)
                spriteBatch.Draw(brushIcon, new Rectangle(Mouse.GetState().X, Mouse.GetState().Y - 32, 32, 32), Color.White);
            else if (CurrentAction == ClickAction.Events)
                spriteBatch.Draw(eventsIcon, new Rectangle(Mouse.GetState().X - 8, Mouse.GetState().Y - 28, 32, 32), Color.White);

            spriteBatch.End();
        }

        public void AddLayer()
        {
            map.Layers.Add(new Layer(map));
        }

        public void RemoveLayer(int layer)
        {
            if (map.Layers[layer] != null)
            {
                map.Layers.Remove(map.Layers[layer]);
            }
            else
            {
                throw new Exception("Layer non presente nella mappa.");
            }
        }

       
        public void SaveMap(string filename)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            XmlWriter writer = XmlWriter.Create(@"Maps\" + filename + ".xml", settings);
            writer.WriteStartDocument();
            writer.WriteStartElement("Map");
            writer.WriteAttributeString("Width", map.Grid.GetLength(1).ToString());
            writer.WriteAttributeString("Height", map.Grid.GetLength(0).ToString());

            writer.WriteStartElement("Layers");

            writer.WriteElementString("Count", map.Layers.Count.ToString());


            for (int y = 0; y < map.Grid.GetLength(0); y++)
            {
                for (int x = 0; x < map.Grid.GetLength(1); x++)
                {

                    writer.WriteStartElement("Tile");
                    writer.WriteAttributeString("X", x.ToString());
                    writer.WriteAttributeString("Y", y.ToString());
                    writer.WriteAttributeString("Event", map.Grid[y, x].Event.ToString());
                    for (int i = 0; i < map.Layers.Count; i++)
                    {
                        writer.WriteAttributeString("Layer_" + i.ToString(), map.Layers[i].Grid[y, x].TileID.ToString());
                        writer.WriteAttributeString("Layer_" + i.ToString() + "_tileset", map.Layers[i].Grid[y, x].TilesetID.ToString());

                    }
                    writer.WriteEndElement();
                }
            }

            writer.WriteEndElement();

            writer.WriteEndDocument();

            writer.Flush();
            writer.Close();
        }

        public void LoadMap(string filename)
        {

            map.Layers = new List<Layer>();

            XmlReader reader = XmlReader.Create(@"Maps\" + filename + ".xml");

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element && reader.Name == "Count")
                {
                    while (reader.NodeType != XmlNodeType.EndElement)
                    {
                        reader.Read();
                        if (reader.NodeType == XmlNodeType.Text)
                        {
                            for (int i = 0; i < Int32.Parse(reader.Value); i++)
                            {
                                map.Layers.Add(new Layer(map));
                            }
                        }
                    }
                }

                if (reader.NodeType == XmlNodeType.Element && reader.Name == "Tile")
                {
                    int x = Convert.ToInt32(reader.GetAttribute(0));
                    int y = Convert.ToInt32(reader.GetAttribute(1));

                    map.Grid[y, x].Event = Convert.ToInt32(reader.GetAttribute(2));
                    for (int i = 0; i < map.Layers.Count; i += 2)
                    {
                        int tile = Convert.ToInt32(reader.GetAttribute(i + 3));
                        int tilesetId = Convert.ToInt32(reader.GetAttribute(i + 4));
                        GridCell gridCell = new GridCell(tile, map.Grid[y, x].Event, tilesetId, map);
                        map.PlaceTile(x, y, gridCell, i);

                    }


                }


            }
            reader.Close();

        }

        public void StartNew(int x, int y)
        {

            map.Grid = new GridCell[y, x];
            EventsGrid = new int[y, x];
            map.Layers = new List<Layer>();
            AddLayer();

        }

        private void AskForSave()
        {
            HasFocus = false;
            menu.Visible = false;
            stateManager.PushToStack(new MapSaveDialog(stateManager.GameRef, stateManager, this));
        }
        private void AskForLoad()
        {
            menu.Visible = false;
            HasFocus = false;
            stateManager.PushToStack(new MapLoadDialog(stateManager.GameRef, stateManager, this));
        }
        private void AskForNew()
        {
            menu.Visible = false;

            textInput.Visible = true;
            textInput.HasFocus = true;
        }

        private void FillLayerWithSelected()
        {
            menu.Visible = false;
            for (int y = 0; y < map.Grid.GetLength(0); y++)
            {
                for (int x = 0; x < map.Grid.GetLength(1); x++)
                {
                    History.Push(new ActionData(x, y, map.Layers[SelectedLayer].Grid[y, x], SelectedLayer));
                    Console.WriteLine("Inserito {0}", History.Peek());
                    map.Layers[SelectedLayer].Grid[y, x] = SelectedTile;
                }
            }

        }

        private Vector2 GridCellUnderMouse()
        {
            float gridX = (Mouse.GetState().X / Tile.TileWidth);
            float gridY = (Mouse.GetState().Y / Tile.TileHeight);
            return new Vector2(gridX, gridY);
        }

        public Vector2 GridCellUnderCoordinates(Vector2 position)
        {
            Vector2 transformedPosition = position;
            float gridX = transformedPosition.X / Tile.TileWidth;
            float gridY = transformedPosition.Y / Tile.TileWidth;

            return Vector2.Transform(new Vector2(gridX, gridY), Matrix.Invert(map.Camera.Transform));

        }

        private Vector2 GridCellTopLeftPosition(float x, float y)
        {


            Vector2 position = new Vector2(x, y);

            return new Vector2(position.X * Tile.TileWidth, position.Y * Tile.TileHeight);
        }

        private void LeftClickAction()
        {
            int gridX = (int)(Mouse.GetState().X - map.Camera.Position.X) / Tile.TileWidth;
            int gridY = (int)(Mouse.GetState().Y - map.Camera.Position.Y) / Tile.TileHeight;
            Vector2 mouse = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            Matrix transform = Matrix.Invert(map.Camera.Transform);

            Vector2.Transform(ref mouse, ref transform, out mouse);

            gridY = (int)(mouse.Y / Tile.TileHeight);
            if (gridY < 0) gridY = 0;
            else if (gridY >= map.Grid.GetLength(0)) gridY = map.Grid.GetLength(0) - 1;

            gridX = (int)(mouse.X / Tile.TileWidth);
            if (gridX < 0) gridX = 0;
            else if (gridX >= map.Grid.GetLength(1)) gridX = map.Grid.GetLength(1) - 1;
            switch (CurrentAction)
            {
                case ClickAction.Paint:
                    bool canplace = true;
                    foreach (BaseGuiElement _menu in Menus)
                    {
                        if (_menu.HasFocus)
                        {
                            canplace = false;
                            break;
                        }
                    }

                    if (canplace)
                    {




                        if (map.Layers[SelectedLayer].Grid[gridY, gridX].TileID == SelectedTile.TileID && map.Layers[SelectedLayer].Grid[gridY, gridX].TilesetID == SelectedTile.TilesetID)
                            break;

                        History.Push(new ActionData(gridX, gridY, map.Layers[SelectedLayer].Grid[gridY, gridX], SelectedLayer));
                        map.PlaceTile(gridX, gridY, SelectedTile, SelectedLayer);
                    }
                    break;

                case ClickAction.Erase:
                    bool canerase = true;
                    foreach (BaseGuiElement _menu in Menus)
                    {
                        if (_menu.HasFocus)
                        {
                            canerase = false;
                            break;
                        }
                    }

                    if (canerase)
                    {
                        if (map.Layers[SelectedLayer].Grid[gridY, gridX].TileID < 0)
                            return;
                        Console.WriteLine("Mouse grid x {0} Mouse grid y {1}", gridX, gridY);
                        History.Push(new ActionData(gridX, gridY, map.Layers[SelectedLayer].Grid[gridY, gridX], SelectedLayer));
                        map.PlaceTile(gridX, gridY, new GridCell(-1, map.Layers[SelectedLayer].Grid[gridX, gridY].Event, map.Layers[SelectedLayer].Grid[gridX, gridY].TilesetID, map), SelectedLayer);
                    }
                    break;

                case ClickAction.Fill:
                    bool canfill = true;
                    foreach (BaseGuiElement _menu in Menus)
                    {
                        if (_menu.HasFocus)
                        {
                            canfill = false;
                            break;
                        }
                    }
                    if (canfill)
                    {
                        FillLayerWithSelected();
                    }
                    break;

                case ClickAction.Events:
                    bool canopeneventmenu = true;
                    foreach (BaseGuiElement _menu in Menus)
                    {
                        if (_menu.HasFocus)
                        {
                            canopeneventmenu = false;
                            break;
                        }
                    }
                    if (canopeneventmenu)
                        eventMenu.CanOpen = true;

                    break;
            }

        }
        public override void LoadContents()
        {
            
            map.PlaceTile(5, 5, new GridCell(0, (int)TileType.PlayerSpawn, 0, map), 0);

            

            map.Camera.Position = new Vector2(map.Grid.GetLength(1) * Tile.TileWidth / 2, map.Grid.GetLength(0) * Tile.TileHeight / 2);

            map.Camera.Initialize();
            menuBg = stateManager.GameRef.Content.Load<Texture2D>(@"Gui\menuBg");
            font = stateManager.GameRef.Content.Load<SpriteFont>("menuFont");
            menu = new Menu(new string[6] { "Nuova", "Salva", "Carica", "Riempi", "Genera", "Posiziona pg" }, menuBg, stateManager);
            menu.Position = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            menu.OnClicks.Add(() => { AskForNew(); });
            menu.OnClicks.Add(() => { AskForSave(); });
            menu.OnClicks.Add(() => { AskForLoad(); });
            menu.OnClicks.Add(() => { FillLayerWithSelected(); });
            menu.OnClicks.Add(() => {});
            menu.OnClicks.Add(() => { animatedSprite.Position = Vector2.Transform(menu.GetPosition(), Matrix.Invert( map.Camera.Transform )); });
            string[] eventActions = new string[Enum.GetNames(typeof(TileType)).Length];
            for (int i = 0; i < eventActions.Length; i++)
            {
                eventActions[i] = Dictionaries.EventTypes[(TileType)i];
            }
            eventMenu = new Menu(eventActions, menuBg, stateManager);

            for (int i = 0; i < eventActions.Length; i++)
            {
                eventMenu.OnClicks.Add(() =>
                {

                    int gridX = (int)(eventMenu.GetPosition().X - map.Camera.Position.X) / Tile.TileWidth;
                    int gridY = (int)(eventMenu.GetPosition().Y - map.Camera.Position.Y) / Tile.TileHeight;
                    Vector2 eventmenuPos = new Vector2(eventMenu.GetPosition().X, eventMenu.GetPosition().Y);
                    Matrix transform = Matrix.Invert(map.Camera.Transform);

                    Vector2.Transform(ref eventmenuPos, ref transform, out eventmenuPos);

                    gridY = (int)(eventmenuPos.Y / Tile.TileHeight);
                    if (gridY < 0) gridY = 0;
                    else if (gridY >= map.Grid.GetLength(0)) gridY = map.Grid.GetLength(0) - 1;

                    gridX = (int)(eventmenuPos.X / Tile.TileWidth);
                    if (gridX < 0) gridX = 0;
                    else if (gridX >= map.Grid.GetLength(1)) gridX = map.Grid.GetLength(1) - 1;
                    map.Layers[SelectedLayer].Grid[gridY, gridX].Event = eventMenu.selectedItem;
                    eventMenu.Visible = false;

                });
            }
            menu.Visible = false;
            eventMenu.Visible = false;
            eventMenu.CanOpen = false;
            eventMenu.RightClickActivation = false;
            ContentManager content = stateManager.GameRef.Content;

            animatedSprite = new PlayerCharacter(map, content.Load<Texture2D>(@"SpriteSheets\Actor1"), 32, 32, 0);
            animatedSprite.interval = 160f;
            
            
            //Pathfinding.Debug = true;
            map.Player = animatedSprite;
            
            tilesets.Add(new Tileset(content, @"Tilesets\sewer_1"));

            tileslist = new TilesList(stateManager, this);
            tileslist.Position = new Vector2(520, 5);
            tileslist.Visible = false;
            ToolSelect = new MapEditorToolSelection(stateManager, this);




            brushIcon = content.Load<Texture2D>(@"Mouse Icons\brush");
            eraseIcon = content.Load<Texture2D>(@"Mouse Icons\eraser");
            fillIcon = content.Load<Texture2D>(@"Mouse Icons\fill");
            eventsIcon = content.Load<Texture2D>(@"Mouse Icons\event");

            whitepixel = content.Load<Texture2D>(@"Gui\1pixelwhite");

            textInput = new GenericTextInput(stateManager);
            textInput.Texts.Add("Larghezza:");
            textInput.Texts.Add("Altezza:");
            textInput.textPositions.Add(new Vector2(30, 10));
            textInput.textPositions.Add(new Vector2(220, 10));
            textInput.Position = new Vector2(200, 200);
            textInput.TextBoxes.Add(new TextBox(stateManager, 40, 20, 3, textInput.Position + new Vector2(140, 13)));
            textInput.TextBoxes.Add(new TextBox(stateManager, 40, 20, 3, textInput.Position + new Vector2(300, 13)));
            textInput.OnConfirm = () =>
            {
                if (textInput.Visible)
                {
                    textInput.HasFocus = false;
                    textInput.Visible = false;
                    StartNew(Convert.ToInt32(textInput.TextBoxes[1].GetText()), Convert.ToInt32(textInput.TextBoxes[0].GetText()));
                    HasFocus = true;
                }
            };
            textInput.Visible = false;
            Menus.Add(textInput);
            Menus.Add(eventMenu);
            Menus.Add(menu);
            Menus.Add(tileslist);
            Menus.Add(ToolSelect);

            animatedSprite.Position = animatedSpriteSpawn;
            eventMenu.openableMenus.Add(menu);
            menu.openableMenus.Add(eventMenu);
            map.Camera.Focus = animatedSprite.Position;
            Pathfinding.mapHeight = map.Grid.GetLength(0);
            Pathfinding.mapWidth = map.Grid.GetLength(1);

            modalScreen = stateManager.GameRef.Content.Load<Texture2D>(@"Gui\modalScreen");
        }
        #endregion

    }

    public enum ClickAction
    {
        Paint = 0,
        Erase,
        Fill,
        Events
    }

    public enum TileType
    {
        None = 0,
        Solid,
        Water,
        Ice,
        PlayerSpawn,

    }

   
    public struct ActionData
    {
        public int gridX;
        public int gridY;
        public GridCell OldTile;
        public int Layer;

        public ActionData(int gridX, int gridY, GridCell OldTile, int Layer)
        {
            this.gridX = gridX;
            this.gridY = gridY;
            this.OldTile = OldTile;
            this.Layer = Layer;
        }
    }

    public static class Dictionaries
    {
        public static Dictionary<TileType, string> EventTypes = new Dictionary<TileType, string>()
        {
            { TileType.None, "Nessuno" }, { TileType.Solid, "Solido" }, { TileType.Ice, "Ghiaccio" }, { TileType.Water, "Acqua" }, { TileType.PlayerSpawn, "Spawn" }
        };
    }



}
