﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using System.Diagnostics;
using Colony.Gameplay_Elements.Items.Map_Items;
using Colony.Gameplay_Elements.Items;
using C3.XNA;

namespace Colony.Gamestates.Map_Editor
{
    public class Map
    {
        // Map properties
        public GridCell[,] Grid;
        public List<Layer> Layers = new List<Layer>();
        public List<Tileset> tilesets = new List<Tileset>();
        Texture2D gridImage;
        public bool GridActive = false;
        public Color GridColor = Color.Red;

        // State Manager reference
        public GameStateManager stateManager;

        // Actors
        public PlayerCharacter Player;
        public List<GenericEnemy> Enemies = new List<GenericEnemy>();
        public List<GenericEnemy> EnemiesRemoveQueue = new List<GenericEnemy>();
        public List<BaseMapItem> MapItems = new List<BaseMapItem>();


        // Projectiles
        public List<Projectile> Projectiles = new List<Projectile>();
        public List<Projectile> ProjsRemoveQueue = new List<Projectile>();

        // Random instane (to be passed at everything requesting it)
        public Random rnd = new Random();

        // Camera and Lights
        public Camera2D Camera;
        public List<BaseCharacter> LightSources = new List<BaseCharacter>();
        Texture2D tileSize;
        public Particle_System.ParticleSystem ParticleSystem;
        Effect Shader;

        public bool ShowAutoTile = false;

        /// <summary>
        ///  Create an instance of the map class
        /// </summary>
        /// <param name="mapSize"> A 2-D int array to store the size of the map and layers</param>
        /// <param name="manager"> The reference ti the State Manager instane</param>
        /// <param name="tilesets"> A list of tilesets to be used for this instane</param>
        public Map(GridCell[,] mapSize, GameStateManager manager, List<Tileset> tilesets)
        {
            Grid = mapSize;
            stateManager = manager;
            this.tilesets = tilesets;
            gridImage = manager.GameRef.Content.Load<Texture2D>(@"Gui\1pixelwhite");
            Camera = new Camera2D(manager.Game);
            tileSize = new Texture2D(manager.Game.GraphicsDevice, Tile.TileWidth, Tile.TileHeight);
            ParticleSystem = new Particle_System.ParticleSystem(Vector2.Zero, this);
            Shader = manager.Game.Content.Load<Effect>(@"Shaders\Effect1");
        }

        public void Update(GameTime gameTime)
        {
            // Delete from the list all removed object since the last update

            foreach (GenericEnemy e in Enemies)
                if (e.Action == EnemyAction.Dead)
                    EnemiesRemoveQueue.Add(e);

            foreach (GenericEnemy e in EnemiesRemoveQueue)
                Enemies.Remove(e);

            EnemiesRemoveQueue.Clear();

            foreach (Projectile projectile in ProjsRemoveQueue)
            {
                //SfxManager.Explosion(projectile.Position, Color.Red, Color.Orange, 50, 1f);
                if (projectile.OnHit != null)
                    projectile.OnHit();
                Projectiles.Remove(projectile);
            }

            ProjsRemoveQueue.Clear();

            // Actors Update
            Player.Update(gameTime);
            foreach (GenericEnemy e in Enemies)
            {
                if (Camera.IsInView(e.Position, new Vector2(32, 32)))
                    e.Update(gameTime);
            }
            for (int i = 0; i < MapItems.Count; i++)
            {
                if (MapItems[i].FlaggedForRemoval)
                {
                    Layers[0].Grid[MapItems[i].GridY, MapItems[i].GridX].Event = 0;
                    MapItems.Remove(MapItems[i]);
                    i--;
                    continue;
                }
                if (Camera.IsInView(MapItems[i].Position, MapItems[i].Graphic))
                    MapItems[i].Update(gameTime);
            }
            foreach (BaseMapItem i in MapItems)
            {
                if (Camera.IsInView(i.Position, i.Graphic))
                    i.Update(gameTime);

                if (!i.Walkable && IsInsideMapBoundaries(new Point(i.GridX, i.GridY)))
                    Layers[0].Grid[i.GridY, i.GridX].Event = 1;
                else if (IsInsideMapBoundaries(new Point(i.GridX, i.GridY)))
                    Layers[0].Grid[i.GridY, i.GridX].Event = 2;
            }

            // Projectiles update
            foreach (Projectile projectile in Projectiles)
                projectile.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch, bool minimap = false, Camera2D camera = null, bool drawOnlyInView = true)
        {
            if (!stateManager.GameRef.NewMapLoaded)
                return;
            // Let's draw the map:
            Shader.CurrentTechnique = Shader.Techniques["FogOfWar"];

            int cameraGridCenterX = (int)Camera.Position.X / Tile.TileWidth;
            int cameraGridCenterY = (int)Camera.Position.Y / Tile.TileHeight;
            foreach (Layer layer in Layers)
            {

                // Loop through each layer
                for (int y = 0; y < Grid.GetLength(0); y++)
                {
                    for (int x = 0; x < Grid.GetLength(1); x++)
                    {
                        if (!IsInsideMapBoundaries(new Point(x, y)))
                            continue;
                        // If it's not in view, ignore the tile
                        if (drawOnlyInView)
                        {
                            if (camera == null)
                            {
                                if (!Camera.IsInView(new Vector2(x * Tile.TileWidth, y * Tile.TileHeight), tileSize))
                                {

                                    continue;
                                }
                            }
                            else
                            {
                                if (!camera.IsInView(new Vector2(x * Tile.TileWidth, y * Tile.TileHeight), tileSize))
                                {

                                    continue;
                                }
                            }
                        }

                        // Make sure an actual tile id is specified for this cell to avoid Exeptions throwing all around 
                        if (layer.Grid[y, x].TileID != -1 && layer.Grid[y, x].TilesetID != -1)
                        {
                            if (layer.Grid[y, x].Visisted > 0f)
                            {
                                if (!minimap)
                                    Shader.Parameters["visibility"].SetValue(layer.Grid[y, x].Visisted);
                                else
                                    Shader.Parameters["visibility"].SetValue(1f);
                                Shader.CurrentTechnique.Passes[0].Apply();
                                spriteBatch.Draw(tilesets[(int)layer.Grid[y, x].TilesetID].TileSet,
                                    GetDestinationRectangle(x, y),
                                    tilesets[(int)layer.Grid[y, x].TilesetID].rectangles[(int)layer.Grid[y, x].TileID],
                                    layer.Grid[y, x].Tint);
                                if (ShowAutoTile)
                                    spriteBatch.DrawString(FloatingText.Font, Layers[0].Grid[y, x].neighbourhood.ToString(), new Vector2(x * Tile.TileWidth, y * Tile.TileHeight), Color.Red * 0.2f);
                            }
                        }
                    }
                }

            }

            // z / 1 = y / screen_height
            // z = y / screen_height

            // Draw the actors (Should this be handled by the current gamestate?)
            Shader.Parameters["visibility"].SetValue(1f);
            Shader.CurrentTechnique.Passes[0].Apply();
            if (!minimap && Player != null)
                Player.Draw(spriteBatch);

            if (!minimap)
            {

                int charGridX = 0;
                int charGridY = 0;
                foreach (BaseMapItem i in (from m in MapItems where m.Walkable == true select m))
                {
                    charGridX = (int)i.GetPosition().X / Tile.TileWidth;
                    charGridY = (int)i.GetPosition().Y / Tile.TileHeight;
                    if (!IsInsideMapBoundaries(new Point(charGridX, charGridY)))
                        continue;
                    if (Layers[0].Grid[charGridY, charGridX].Visisted < 4f)
                    {
                        Shader.Parameters["visibility"].SetValue(Layers[0].Grid[charGridY, charGridX].Visisted);
                        Shader.CurrentTechnique.Passes[0].Apply();

                        i.Draw(spriteBatch);
                    }
                    //i.Draw(spriteBatch);
                }
                foreach (IDrawableObject e in EveryActor())
                {
                    charGridX = (int)e.GetPosition().X / Tile.TileWidth;
                    charGridY = (int)e.GetPosition().Y / Tile.TileHeight;
                    if (!IsInsideMapBoundaries(new Point(charGridX, charGridY)))
                        continue;
                    if (Layers[0].Grid[charGridY, charGridX].Visisted < 4f)
                    {
                        Shader.Parameters["visibility"].SetValue(Layers[0].Grid[charGridY, charGridX].Visisted);
                        Shader.CurrentTechnique.Passes[0].Apply();

                        e.Draw(spriteBatch);
                    }

                }

            }

            Shader.Parameters["visibility"].SetValue(1f);
            Shader.CurrentTechnique.Passes[0].Apply();
            if (!minimap)
            {
                foreach (Projectile projectile in Projectiles)
                {
                    if (Camera.IsInView(projectile.Position, projectile.Graphics))
                        projectile.Draw(spriteBatch);
                }
            }

            // If we're in edit mode and the grid is active, draw it.
            if (GridActive)
            {
                for (int y = 0; y <= Grid.GetLength(0); y++)
                {
                    spriteBatch.Draw(gridImage, new Rectangle(0, Tile.TileHeight * y, Tile.TileWidth * Grid.GetLength(1), 2), GridColor);

                }
                for (int x = 0; x <= Grid.GetLength(1); x++)
                {
                    spriteBatch.Draw(gridImage, new Rectangle(Tile.TileWidth * x, 0, 2, Tile.TileHeight * Grid.GetLength(0)), GridColor);
                }
            }
        }

        /// <summary>
        ///  Flags a projectile for removal on the next update
        /// </summary>
        /// <param name="proj">The projectile instance to be flagged</param>
        public void DeleteProjectile(Projectile proj)
        {
            ProjsRemoveQueue.Add(proj);
        }

        /// <summary>
        ///  Flags an enemy for removal on the next update
        /// </summary>
        /// <param name="enemy">The enemy instance to be flagged</param>
        public void DeleteEnemy(GenericEnemy enemy)
        {
            EnemiesRemoveQueue.Add(enemy);
        }

        public bool IsInsideMapBoundaries(Point p)
        {
            if (p.X < 0 || p.X >= Grid.GetLength(1) || p.Y < 0 || p.Y >= Grid.GetLength(0))
                return false;

            return true;

        }

        public List<GridCell> GridCellsUnderRectnalge(Rectangle rect, bool AlreadyConvertedToTile = true)
        {

            List<GridCell> returnList = new List<GridCell>();

            // Top Left corner
            if (rect.Left >= 0 && rect.Top >= 0 && rect.Left < Grid.GetLength(1) && rect.Top < Grid.GetLength(0))
                returnList.Add(Layers[0].Grid[rect.Top, rect.Left]);

            // Bottom left corner
            if (rect.Left >= 0 && rect.Bottom >= 0 && rect.Left < Grid.GetLength(1) && rect.Bottom < Grid.GetLength(0))
                if (!returnList.Contains(Layers[0].Grid[rect.Bottom, rect.Left]))
                    returnList.Add(Layers[0].Grid[rect.Bottom, rect.Left]);

            // Bottom right corner
            if (rect.Right >= 0 && rect.Bottom >= 0 && rect.Right < Grid.GetLength(1) && rect.Bottom < Grid.GetLength(0))
                if (!returnList.Contains(Layers[0].Grid[rect.Bottom, rect.Right]))
                    returnList.Add(Layers[0].Grid[rect.Bottom, rect.Right]);

            // Top right corner
            if (rect.Right >= 0 && rect.Top >= 0 && rect.Right < Grid.GetLength(1) && rect.Top < Grid.GetLength(0))
                if (!returnList.Contains(Layers[0].Grid[rect.Top, rect.Right]))
                    returnList.Add(Layers[0].Grid[rect.Top, rect.Right]);


            ((DebugConsole)stateManager.GetStateByType(typeof(DebugConsole))).DebugText(returnList.Count.ToString());

            return returnList;

        }

        /// <summary>
        /// Restituisce il Rettangolo di destinazione (su schermo) date le coordinate x e y di Grid
        /// </summary>
        /// <param name="gridX">Componente X di Grid</param>
        /// <param name="gridY">Componente Y di Grid</param>
        /// <returns></returns>
        public Rectangle GetDestinationRectangle(int gridX, int gridY)
        {

            return new Rectangle(gridX * Tile.TileWidth, gridY * Tile.TileHeight, Tile.TileWidth, Tile.TileHeight);
        }

        /// <summary>
        ///  Add a Layer to the layers list
        /// </summary>
        public void AddLayer()
        {
            Layers.Add(new Layer(this));
        }

        public List<IDrawableObject> EveryActor()
        {
            List<IDrawableObject> everyActor = new List<IDrawableObject>();
            everyActor.AddRange(Enemies);
            //everyActor.AddRange(MapItems);
            foreach (BaseMapItem i in MapItems)
            {
                if (!i.Walkable)
                    everyActor.Add(i);
            }
            if (Player != null)
                everyActor.Add(Player);
            everyActor = everyActor.OrderBy(x => x.GetPosition().Y).ToList();
            return everyActor;
        }

        public GridCell GetGridUnderPosition(Vector2 position, int layer)
        {
            float gridX = position.X / Tile.TileWidth;
            float gridY = position.Y / Tile.TileWidth;

            if (gridX > 0 && gridX < Grid.GetLength(1) && gridY > 0 && gridY < Grid.GetLength(0))
            {

                return Layers[layer].Grid[(int)Math.Floor(gridY), (int)Math.Floor(gridX)];
            }
            else
                return new GridCell(-2, -2, -2, this);
        }

        public Vector2 GedGridPosition(Vector2 position)
        {
            int x = (int)position.X;
            int y = (int)position.Y;
            return new Vector2(x * Tile.TileWidth, y * Tile.TileHeight);
        }
        public Vector2 GedGridMidPosition(int x, int y)
        {
            return new Vector2((x * Tile.TileWidth) + Tile.TileWidth / 2, (y * Tile.TileHeight) + Tile.TileHeight / 2);
        }

        /// <summary>
        ///  Place a tile at the specified coordinates and layer
        /// </summary>
        /// <param name="x">The grid X coordinate</param>
        /// <param name="y">The grid Y coordinate</param>
        /// <param name="tileID">The Tile ID to be used</param>
        /// <param name="layer">The layer on which to draw</param>
        public void PlaceTile(int x, int y, GridCell tileID, int layer)
        {
            if (layer < Layers.Count)
            {

                if (y < Grid.GetLength(0) && y >= 0 && x < Grid.GetLength(1) && x >= 0)
                {
                    Layers[layer].Grid[y, x] = tileID;
                }
            }
            else
                throw new Exception("Layer non presente nella mappa.");
        }

        /// <summary>
        ///  Generate a Dungeon using a custom Algorithm. This is pretty much broken, do not use it for the time being
        /// </summary>
        /// <param name="roomsnumber"></param>
        /// <param name="minRoomWidth"></param>
        /// <param name="maxRoomWidth"></param>
        /// <param name="minRoomHeight"></param>
        /// <param name="maxRoomHeight"></param>
        /// <returns></returns>
        public int[,] GenerateDungeon(int roomsnumber, int minRoomWidth, int maxRoomWidth, int minRoomHeight, int maxRoomHeight)
        {

            //int corridorsWidth = 3;
            //float monsterChance = 30f;
            Stopwatch sw = Stopwatch.StartNew();
            Console.WriteLine("Genero dungeon...");
            int[,] tempGrid = new int[Grid.GetLength(0), Grid.GetLength(1)];
            List<Room> Rooms = new List<Room>();
            List<Corridor> Corridors = new List<Corridor>();
            Pathfinding.SolidNodes.Clear();

            int tries = 0;

            for (int j = 0; j < roomsnumber; j++)
            {
                if (tries >= 900)
                {
                    Console.WriteLine("Impossibile trovare uno spazio per piazzare la stanza.");
                    return tempGrid;
                }
                Console.WriteLine("Provo la stanza numero {0}", j);
                // Random generated room position and size
                int topLeftX = rnd.Next(0, Grid.GetLength(1) - 1);
                int topLeftY = rnd.Next(0, Grid.GetLength(0) - 1);
                int roomWidth = rnd.Next(minRoomWidth, maxRoomWidth);
                int roomHeight = rnd.Next(minRoomHeight, maxRoomHeight);

                // Room class instance
                Room room = new Room();
                room.X = topLeftX;
                room.Y = topLeftY;
                room.Width = roomWidth;
                room.Height = roomHeight;

                // Check if space is occupied
                bool occupied = false;
                for (int y = topLeftY; y <= topLeftY + roomHeight; y++)
                {
                    for (int x = topLeftX; x <= topLeftX + roomWidth; x++)
                    {
                        if (y < 0 || y >= tempGrid.GetLength(0) || x < 0 || x >= tempGrid.GetLength(1))
                        {
                            occupied = true;
                            break;
                        }
                        if (tempGrid[y, x] != 0)
                        {
                            occupied = true;
                            break;
                        }
                        if (y <= 0 || y + roomHeight + 1 >= tempGrid.GetLength(0) || x <= 0 || x + roomWidth + 1 >= tempGrid.GetLength(1))
                            continue;
                        if (tempGrid[y - 1, x] != 0 ||
                            tempGrid[y + roomHeight + 1, x] != 0 ||
                            tempGrid[y, x - 1] != 0 ||
                            tempGrid[y, x + roomWidth + 1] != 0
                            )
                        {
                            occupied = true;
                            break;
                        }

                    }
                    if (occupied)
                        break;
                }

                if (occupied)
                {
                    Console.WriteLine("Spazio già occupato. Ritento.");
                    j--;
                    tries++;

                    continue;
                }
                Console.WriteLine("Occupo spazio {0} {1} {2} {3}", topLeftX, topLeftY, roomWidth, roomHeight);
                tries = 0;
                // Set the space as occupied
                for (int y = topLeftY; y <= topLeftY + roomHeight; y++)
                {
                    for (int x = topLeftX; x <= topLeftX + roomWidth; x++)
                    {
                        if (y < 0 || y >= tempGrid.GetLength(0) || x < 0 || x >= tempGrid.GetLength(1))
                            break;
                        tempGrid[y, x] = 1;
                    }
                }
                room.SetWalls(tempGrid);
                foreach (Point p in room.Walls)
                {

                    tempGrid[p.Y, p.X] = 4;
                }
                if (j > 0)
                {


                    Node startingRoom = new Node(room.X + room.Width / 2, room.Y + room.Height / 2, TileType.None);
                    Node endingRoom = new Node(Rooms[j - 1].X + Rooms[j - 1].Width / 2, Rooms[j - 1].Y + Rooms[j - 1].Height / 2, TileType.None);

                    // Sopra
                    if (startingRoom.Y < endingRoom.Y)
                    {
                        startingRoom.Y = startingRoom.Y + room.Height / 2 + 1;
                    }
                    else if (startingRoom.Y > endingRoom.Y)
                    {
                        startingRoom.Y = startingRoom.Y - room.Height / 2 - 1;
                    }
                    else if (startingRoom.X < endingRoom.X)
                    {
                        startingRoom.X = startingRoom.X + room.Width / 2 + 1;
                    }
                    else if (startingRoom.X > endingRoom.X)
                    {
                        startingRoom.X = startingRoom.X - room.Width / 2 - 1;
                    }

                    List<Node> path = new List<Node>();
                    while (path.Count < 1)
                    {
                        Pathfinding.FindPathNoDiagonals(startingRoom, endingRoom, true);
                        path = Pathfinding.ReconstructPath();
                    }
                    Corridor corridor = new Corridor();
                    corridor.StartingRoom = startingRoom;
                    corridor.EndingRoom = endingRoom;
                    corridor.WallsFound = new List<Node>();

                    foreach (Node n in path)
                    {
                        if (tempGrid[n.Y, n.X] == 1)
                            continue;
                        else if (tempGrid[n.Y, n.X] == 4)
                        {
                            corridor.WallsFound.Add(n);
                            tempGrid[n.Y, n.X] = 2;
                        }
                        else
                            tempGrid[n.Y, n.X] = 2;
                    }


                    corridor.CorridorPath = path;
                    Corridors.Add(corridor);

                }

                Rooms.Add(room);
            }


            foreach (Corridor corridor in Corridors)
            {

                tempGrid[corridor.CorridorPath[0].Y, corridor.CorridorPath[0].X] = 3;
                tempGrid[corridor.CorridorPath[corridor.CorridorPath.Count - 1].Y, corridor.CorridorPath[corridor.CorridorPath.Count - 1].X] = 3;
            }

            sw.Stop();
            Console.WriteLine("Finito. Impiegato {0} millisecondi.", sw.ElapsedMilliseconds);

            return tempGrid;
        }

        /// <summary>
        ///  Generate a Dungeon using an Agent-based algorithm. This is a work in progress and will not work for now
        /// </summary>
        /// <param name="lookAhead">Should the Agent plan ahead of time before placing rooms? Default is true.</param>
        /// <returns></returns>
        public int[,] DigDungeon(bool lookAhead = true)
        {
            int[,] tempGrid = new int[Grid.GetLength(0), Grid.GetLength(1)];

            DiggingAgent Agent = new DiggingAgent();
            Agent.Initialize();
            Agent.Position = Vector2.Zero;

            //Agent.UpdateValues();
            for (int i = 0; i < 14; i++)
            {
                // Try three random room sizes
                #region Try three random sizes
                bool intersect = true;
                int tries = 0;
                Room room = new Room();
                if (Agent.Debug)
                {
                    Console.WriteLine("Inizio...");
                    Console.ReadLine();
                }
                while (intersect)
                {
                    tries++;
                    if (tries > 10)
                    {
                        /* if (Agent.lastCorridorPlaced.Count > 0)
                         {
                             Point p = Agent.lastCorridorPlaced.Pop();
                             Point p2 = Agent.lastCorridorPlaced.Pop();
                             tempGrid[p.Y, p.X] = 0;
                             tempGrid[p2.Y, p2.X] = 0;
                         }*/
                        break;
                    }
                    intersect = false;
                    int randomRoomSize = rnd.Next(Agent.minRoomSize, Agent.maxRoomSize);

                    room.X = Agent.GridX;
                    room.Y = Agent.GridY;
                    room.Width = randomRoomSize;
                    room.Height = randomRoomSize;
                    if (Agent.Debug)
                    {
                        Console.WriteLine("Provo una stanza grande {0} -> {1} {2} {3} {4}", randomRoomSize, room.X, room.Y, room.Width, room.Height);
                        Console.ReadLine();
                    }
                    for (int y = room.RoomRectangle.Top; y < room.RoomRectangle.Bottom; y++)
                    {
                        if (tempGrid[y, room.RoomRectangle.Left] == 1 || tempGrid[y, room.RoomRectangle.Right] == 1)
                        {
                            intersect = true;
                            break;
                        }
                    }

                    if (!intersect)
                    {
                        for (int x = room.RoomRectangle.Left; x < room.RoomRectangle.Right; x++)
                        {
                            if (tempGrid[room.RoomRectangle.Top, x] == 1 || tempGrid[room.RoomRectangle.Bottom, x] == 1)
                            {
                                intersect = true;
                                break;
                            }
                        }
                    }

                    if (!intersect)
                    {
                        if (Agent.Debug)
                        {
                            Console.WriteLine("Piazzo stanza...");
                            Console.ReadLine();
                        }
                        for (int y = room.RoomRectangle.Top; y < room.RoomRectangle.Bottom; y++)
                            for (int x = room.RoomRectangle.Left; x < room.RoomRectangle.Right; x++)
                                tempGrid[y, x] = 1;
                    }
                }
                #endregion
                if (Agent.Debug)
                {
                    Console.WriteLine("Cambio direzione...");
                    Console.ReadLine();
                }
                Agent.ChangeDirection(rnd);
                tries = 0;
                while (Agent.GridX - Agent.Direction.X < 0 && Agent.GridY - Agent.Direction.Y < 0)
                {
                    if (Agent.Debug)
                    {
                        Console.WriteLine("PCambio direzione...");
                        Console.ReadLine();
                    }
                    Agent.ChangeDirection(rnd);
                }
                if (Agent.Debug)
                {
                    Console.WriteLine("Direzione {0} {1}", Agent.Direction.X, Agent.Direction.Y);
                    Console.ReadLine();
                }
                if (Agent.Direction.Y >= 0 && Agent.Direction.X >= 0)
                {
                    Agent.GridX += (int)Agent.Direction.X * room.RoomRectangle.Width;
                    Agent.GridY += (int)Agent.Direction.Y * room.RoomRectangle.Height;
                }


                Agent.WalkFor(2, ref tempGrid);

            }
            return tempGrid;
        }

        public int[,] WalkThroughDungeon()
        {
            DiggingAgent agent = new DiggingAgent();
            agent.Initialize();
            agent.Position = Vector2.Zero;

            agent.GridX = 1;
            agent.GridY = 1;

            int MaxFloorsTiles = 1800;
            int FloorTiles = 0;

            int[,] tempGrid = new int[Grid.GetLength(0), Grid.GetLength(1)];

            // 0 empty space
            // 1 floor tile

            while (FloorTiles < MaxFloorsTiles)
            {
                if (agent.GridY >= Grid.GetLength(0) || agent.GridX >= Grid.GetLength(1))
                    break;
                if (tempGrid[agent.GridY, agent.GridX] != 1)
                {
                    tempGrid[agent.GridY, agent.GridX] = 1;
                    FloorTiles++;
                }
                agent.PlaceRandomFeature(ref tempGrid);
                agent.ChangeDirection(rnd);
                agent.Position += agent.Direction * Tile.TileWidth;
                agent.Position = Vector2.Clamp(agent.Position, Vector2.Zero, new Vector2(Grid.GetLength(1) * Tile.TileWidth, Grid.GetLength(0) * Tile.TileHeight));


            }

            return tempGrid;
        }

        public int[,] PerlinNoise()
        {
            int[,] tempGrid = new int[Grid.GetLength(0), Grid.GetLength(1)];

            float[,] noise = new float[Grid.GetLength(0), Grid.GetLength(1)];
            noise = Noise.GenerateWhiteNoise(Grid.GetLength(1), Grid.GetLength(0));
            noise = Noise.GeneratePerlinNoise(noise, 4);
            string text = "";
            for (int y = 0; y < Grid.GetLength(0); y++)
                for (int x = 0; x < Grid.GetLength(1); x++)
                {
                    int value = 0;
                    if (noise[y, x] < 1f)
                        value = 0;
                    else
                        value = 1;
                    float f = noise[y, x];
                    text += ", " + f.ToString();
                    tempGrid[y, x] = value;
                }


            System.IO.File.WriteAllText(@"text.txt", text);
            return tempGrid;
        }

        public int[,] TetrisGeneration()
        {
            int[,] tempGrid = new int[Grid.GetLength(0), Grid.GetLength(1)];

            /// Room measurement unit is TILE SIZE! (Grid Position, not Screen Position)

            // Step 1: Make a random list of rooms
            int RoomsNumber = 20;
            int MaxRoomSize = 8;
            int MinRoomSize = 4;

            List<Room> Rooms = new List<Room>();
            for (int i = 0; i < RoomsNumber; i++)
            {
                Room r = new Room();
                r.Width = rnd.Next(MinRoomSize, MaxRoomSize);
                r.Height = rnd.Next(MinRoomSize, MaxRoomSize);
                Rooms.Add(r);
            }

            // Step 2: Room placement
            #region Step2

            int offset = 0;

            /// Place first room at center

            Room currentRoom = Rooms[0];
            currentRoom.X = (Grid.GetLength(1) / 2) - (currentRoom.Width / 2);
            currentRoom.Y = (Grid.GetLength(0) / 2) - (currentRoom.Height / 2);
            for (int y = currentRoom.Y; y < currentRoom.Y + currentRoom.Height; y++)
                for (int x = currentRoom.X; x < currentRoom.X + currentRoom.Width; x++)
                    tempGrid[y, x] = 1;

              Player.Position = new Vector2(currentRoom.X * Tile.TileWidth, currentRoom.Y * Tile.TileHeight);

            int debug_placed = 0;

            /// Place the other rooms by "playing tetris"
            for (int i = 1; i < Rooms.Count; i++) // For each room after the first
            {
                currentRoom = Rooms[i];
                bool _placed = false;

                // From above

                /// Pick a random X value
                int x = rnd.Next(0, Grid.GetLength(1) - 1);
                currentRoom.X = x;
                for (int y = 0; y < Grid.GetLength(0); y++) // Starting from top, down to the map height
                {
                    currentRoom.Y = y;

                    if (currentRoom.CollideWithOtherRoom(tempGrid) || y + currentRoom.Height >= Grid.GetLength(0))
                    {
                        // A spot has been found. Place it on the map
                        // Leave a 1 tile margin
                        if (y - offset >= 0)
                            y -= offset;
                        if (x - offset >= 0)
                            x -= offset;

                        for (int y2 = y; y2 < y + currentRoom.Height; y2++)
                        {
                            if (y2 < 0 || y2 >= Grid.GetLength(0))
                                break;
                            for (int x2 = x; x2 < currentRoom.Width + x; x2++)
                            {
                                if (x2 < 0 || x2 >= Grid.GetLength(1))
                                    break;
                                tempGrid[y2, x2] = 1;
                            }
                        }
                        _placed = true;
                        break;
                    }

                }

                if (_placed)
                {
                    debug_placed++;
                    continue;
                }

                // From below

                /// Pick a random X value
                x = rnd.Next(0, Grid.GetLength(1) - 1);
                currentRoom.X = x;

                for (int y = Grid.GetLength(0) - 1; y > 0; y--)
                {
                    currentRoom.Y = y;

                    if (currentRoom.CollideWithOtherRoom(tempGrid) || currentRoom.Y <= 0)
                    {

                        // A spot has been found. Place it on the map
                        if (y - offset >= 0)
                            y -= offset;
                        if (x - offset >= 0)
                            x -= offset;
                        for (int y2 = y; y2 < y - currentRoom.Height; y2--)
                        {
                            if (y2 < 0 || y2 >= Grid.GetLength(0))
                                break;
                            for (int x2 = x; x2 < currentRoom.Width + x; x2++)
                            {
                                if (x2 < 0 || x2 >= Grid.GetLength(1))
                                    break;
                                tempGrid[y2, x2] = 1;
                            }
                        }
                        _placed = true;
                        break;
                    }
                }

                if (_placed)
                {
                    debug_placed++;
                    continue;
                }

                // From Left
                // Pick random Y value
                currentRoom.Y = rnd.Next(0, Grid.GetLength(0) - 1);
                for (int x_room = 0; x_room < Grid.GetLength(1); x_room++)
                {
                    currentRoom.X = x_room;

                    if (currentRoom.CollideWithOtherRoom(tempGrid) || x_room + currentRoom.Width >= Grid.GetLength(1))
                    {
                        // A spot has been found
                        if (currentRoom.Y - offset > 0)
                            currentRoom.Y -= offset;
                        if (currentRoom.X - offset > 0)
                            currentRoom.X -= offset;
                        for (int y2 = currentRoom.Y; y2 < currentRoom.Y + currentRoom.Height; y2++)
                        {
                            if (y2 < 0 || y2 >= Grid.GetLength(0))
                                break;
                            for (int x2 = currentRoom.X; x2 < currentRoom.X + currentRoom.Width; x2++)
                            {
                                if (x2 < 0 || x2 >= Grid.GetLength(1))
                                    break;

                                tempGrid[y2, x2] = 1;

                            }
                        }
                        _placed = true;
                        break;

                    }
                }

                // From Right
                // Pick random Y value
                currentRoom.Y = rnd.Next(0, Grid.GetLength(0) - 1);

                for (int x_room = Grid.GetLength(1) - 1; x_room > 0; x_room++)
                {
                    currentRoom.X = x_room;

                    if (currentRoom.CollideWithOtherRoom(tempGrid) || x_room + currentRoom.Width >= Grid.GetLength(1))
                    {
                        // A spot has been found
                        if (currentRoom.Y - offset > 0)
                            currentRoom.Y -= offset;
                        if (currentRoom.X - offset > 0)
                            currentRoom.X -= offset;
                        for (int y2 = currentRoom.Y; y2 < currentRoom.Y + currentRoom.Height; y2++)
                        {
                            if (y2 < 0 || y2 >= Grid.GetLength(0))
                                break;
                            for (int x2 = currentRoom.X; x2 < currentRoom.X + currentRoom.Width; x2++)
                            {
                                if (x2 < 0 || x2 >= Grid.GetLength(1))
                                    break;

                                tempGrid[y2, x2] = 1;

                            }
                        }
                        _placed = true;
                        break;

                    }
                }
            }
            #endregion

            // Step 3: Connecting rooms


            Console.WriteLine("Piazzate {0} stanze", debug_placed);
            return tempGrid;
        }

        #region BSP Generated Dungeon

        /// <summary>
        ///  Generate a Dungeon using the Binary Space Partionioning algorithm. This is the most complete of the three methods to generate
        ///  a Dungeon.
        /// </summary>
        /// <returns></returns>
        public int[,] BSPGeneration()
        {

            int[,] tempGrid = new int[Grid.GetLength(0), Grid.GetLength(1)];
            Enemies.Clear();
            MapItems.Clear();
            TreeNode Root = new TreeNode();
            Root.Space = new Rectangle(0, 0, tempGrid.GetLength(1), tempGrid.GetLength(0));
            Root.IsRoot = true;

            List<TreeNode> tree = new List<TreeNode>();
            TreeNode currentNode = Root;
            Root.map = this;
            tree.Add(Root);
            bool did_split = true;

            while (did_split)
            {
                did_split = false;
                Console.WriteLine("did split false");
                // Console.ReadLine();
                for (int i = 0; i < tree.Count; i++)
                {
                    Console.WriteLine("Tree size... {0} Current leaf... {1}", tree.Count, i);
                    if (tree[i].LeftNode == null && tree[i].RightNode == null)
                    {
                        if (tree[i].Space.Width > 22 || tree[i].Space.Height > 22)// || rnd.NextDouble() > 0.80)
                        {
                            if (tree[i].Split(rnd))
                            {
                                tree.Add(tree[i].RightNode);
                                tree.Add(tree[i].LeftNode);
                                tree[i].RightNode.map = this;
                                tree[i].LeftNode.map = this;
                                Console.WriteLine("Aggiunti children...");
                                //     Console.ReadLine();
                                did_split = true;
                                Console.WriteLine("did split true");
                            }
                        }
                    }

                }
            }

            bool PlayerSpawn = false;
            Point PlayerSpawnLocation = new Point(0, 0);
            Root.CreateRoom(ref tempGrid, rnd, ref PlayerSpawn, ref PlayerSpawnLocation);

            if (Player == null)
                return tempGrid;

            Player.Position = new Vector2(PlayerSpawnLocation.X * Tile.TileWidth, PlayerSpawnLocation.Y * Tile.TileHeight);
            if (!PlayerSpawn)
            {
                while (tempGrid[Player.GetCenterYOnGrid(), Player.GetCenterXOnGrid()] != 1)
                {
                    Player.Position += new Vector2(4, 4);
                }
                Player.Position += new Vector2(3, 3);
            }
            return tempGrid;
        }

    }

    // Tree node class for the BSP Algorithm
    public class TreeNode
    {
        public Rectangle Space;
        public TreeNode RightNode;
        public TreeNode LeftNode;
        public List<TreeNode> Childrens = new List<TreeNode>();
        public bool IsRoot = false;
        public bool DoneDividing = false;
        public Rectangle room;
        public Map map;


        public Rectangle GetRoom(Random rnd)
        {
            if (room != Rectangle.Empty)
                return room;
            else
            {
                Rectangle lRoom = Rectangle.Empty;
                Rectangle rRoom = Rectangle.Empty;

                if (LeftNode != null)
                    lRoom = LeftNode.GetRoom(rnd);
                if (RightNode != null)
                    rRoom = RightNode.GetRoom(rnd);

                if (lRoom == Rectangle.Empty && rRoom == Rectangle.Empty)
                    return Rectangle.Empty;

                else if (rRoom == Rectangle.Empty)
                    return lRoom;
                else if (lRoom == Rectangle.Empty)
                    return rRoom;
                else if (rnd.NextDouble() > 0.5)
                    return lRoom;
                else
                    return rRoom;


            }


        }

        public void CreateHall(Rectangle l, Rectangle r, Random rnd, ref int[,] tempGrid)
        {

            Point point1 = new Point(rnd.Next(l.Left + 1, l.Right - 2), rnd.Next(l.Top + 1, l.Bottom - 2));
            Point point2 = new Point(rnd.Next(r.Left + 1, r.Right - 2), rnd.Next(r.Top + 1, r.Bottom - 2));
            List<Rectangle> Halls = new List<Rectangle>();
            int w = point2.X - point1.X;
            int h = point2.Y - point1.Y;

            if (w < 0)
            {
                if (h < 0)
                {
                    if (rnd.NextDouble() > 0.5)
                    {
                        Halls.Add(new Rectangle(point2.X, point1.Y, Math.Abs(w), 1));
                        Halls.Add(new Rectangle(point2.X, point2.Y, 1, Math.Abs(h)));
                    }
                    else
                    {
                        Halls.Add(new Rectangle(point2.X, point2.Y, Math.Abs(w), 1));
                        Halls.Add(new Rectangle(point1.X, point2.Y, 1, Math.Abs(h)));
                    }
                }

                else if (h > 0)
                {
                    if (rnd.NextDouble() > 0.5)
                    {
                        Halls.Add(new Rectangle(point2.X, point1.Y, Math.Abs(w), 1));
                        Halls.Add(new Rectangle(point2.X, point1.Y, 1, Math.Abs(h)));
                    }
                    else
                    {
                        Halls.Add(new Rectangle(point2.X, point2.Y, Math.Abs(w), 1));
                        Halls.Add(new Rectangle(point1.X, point1.Y, 1, Math.Abs(h)));
                    }
                }

                else
                {
                    Halls.Add(new Rectangle(point2.X, point2.Y, Math.Abs(w), 1));
                }
            }
            else if (w > 0)
            {
                if (h < 0)
                {
                    if (rnd.NextDouble() > 0.5)
                    {
                        Halls.Add(new Rectangle(point1.X, point2.Y, Math.Abs(w), 1));
                        Halls.Add(new Rectangle(point1.X, point2.Y, 1, Math.Abs(h)));
                    }
                    else
                    {
                        Halls.Add(new Rectangle(point1.X, point1.Y, Math.Abs(w), 1));
                        Halls.Add(new Rectangle(point2.X, point2.Y, 1, Math.Abs(h)));
                    }
                }

                else if (h > 0)
                {
                    if (rnd.NextDouble() > 0.5)
                    {
                        Halls.Add(new Rectangle(point1.X, point1.Y, Math.Abs(w), 1));
                        Halls.Add(new Rectangle(point2.X, point1.Y, 1, Math.Abs(h)));
                    }
                    else
                    {
                        Halls.Add(new Rectangle(point1.X, point2.Y, Math.Abs(w), 1));
                        Halls.Add(new Rectangle(point1.X, point1.Y, 1, Math.Abs(h)));
                    }
                }

                else
                {
                    Halls.Add(new Rectangle(point1.X, point1.Y, Math.Abs(w), 1));
                }
            }

            else
            {
                if (h < 0)
                {
                    Halls.Add(new Rectangle(point2.X, point2.Y, 1, Math.Abs(h)));
                }
                else if (h > 0)
                {
                    Halls.Add(new Rectangle(point1.X, point1.Y, 1, Math.Abs(h)));

                }
            }

            foreach (Rectangle rect in Halls)
            {

                for (int y = rect.Top; y <= rect.Bottom; y++)
                {

                    for (int x = rect.Left; x <= rect.Right; x++)
                    {

                        if (tempGrid[y, x] != 1)
                        {

                            tempGrid[y, x] = 2;

                        }
                    }

                }



            }
        }
        // 0 empty = basic roof tile
        // 1 Room floor
        // 2 Corridor Floor
        // 3 Door

        public void CreateRoom(ref int[,] tempGrid, Random rnd, ref bool PlayerSpawn, ref  Point playerLocation)
        {


            if (LeftNode == null && RightNode == null)
            {

                Point roomSize = new Point(rnd.Next(5, Space.Width - 1), rnd.Next(3, Space.Height - 1));
                Point roomPos = new Point(rnd.Next(1, (int)(Space.Width - roomSize.X - 1)), rnd.Next(1, (int)(Space.Height - roomSize.Y - 1)));

                room = new Rectangle(Space.X + roomPos.X, Space.Y + roomPos.Y, roomSize.X, roomSize.Y);
                for (int y = room.Top; y <= room.Bottom; y++)
                    for (int x = room.Left; x <= room.Right; x++)
                        tempGrid[y, x] = 1;

                if (rnd.NextDouble() > 0.6 && !PlayerSpawn)
                {
                    PlayerSpawn = true;
                    playerLocation = new Point(room.Left + room.Width / 2, room.Top + room.Height / 2);

                }

                if (rnd.NextDouble() < 0.8)
                {
                    int groupNumber = rnd.Next(4) + 1;

                    for (int i = 0; i <= groupNumber; i++)
                    {
                        int x = rnd.Next(room.Left + 1, room.Right - 1);
                        int y = rnd.Next(room.Top + 1, room.Bottom - 1);
                        var enemy = (GenericEnemy)Activator.CreateInstance(EnemiesDB.EnemyTypes[rnd.Next(EnemiesDB.EnemyTypes.Count)], map);
                        enemy.Position = new Vector2(x * Tile.TileWidth, y * Tile.TileHeight);
                        map.Enemies.Add(enemy);
                    }
                }

                if (rnd.NextDouble() < 0.2)
                {

                    Vector2 chestposition = new Vector2(rnd.Next(room.Left + 1, room.Right - 1) * Tile.TileWidth, rnd.Next(room.Top + 1, room.Bottom - 1) * Tile.TileHeight);

                    ChestItem chest = new ChestItem(chestposition,
                        "Chest",
                        map.stateManager.GameRef.Content.Load<Texture2D>(@"Items\chest2"), map);
                    map.MapItems.Add(chest);
                }
                else
                {
                    Vector2 tableposition = new Vector2(rnd.Next(room.Left + 1, room.Right - 1) * Tile.TileWidth, rnd.Next(room.Top + 1, room.Bottom - 1) * Tile.TileHeight);
                    Table table = new Table(tableposition,
                        "Table",
                        map.stateManager.GameRef.Content.Load<Texture2D>(@"Items\blood_red2"), map);
                    map.MapItems.Add(table);
                }

            }

            else
            {
                if (LeftNode != null)
                    LeftNode.CreateRoom(ref tempGrid, rnd, ref PlayerSpawn, ref playerLocation);
                if (RightNode != null)
                    RightNode.CreateRoom(ref tempGrid, rnd, ref PlayerSpawn, ref playerLocation);

                if (LeftNode != null && RightNode != null)
                {
                    CreateHall(LeftNode.GetRoom(rnd), RightNode.GetRoom(rnd), rnd, ref tempGrid);
                }

            }
        }

        public bool Split(Random rnd)
        {

            if (this.Space.Width < 15 || this.Space.Height < 15)
                return false;
            if (this.DoneDividing)
                return false;

            int min_leaf_size = 8;

            bool splitH = rnd.NextDouble() > 0.5;
            if (Space.Width > Space.Height && Space.Height / Space.Width >= 0.05)
                splitH = true;
            else if (Space.Height > Space.Width && Space.Width / Space.Height >= 0.05)
                splitH = false;
            int max = (splitH ? Space.Height : Space.Width) - min_leaf_size;
            if (max <= min_leaf_size)
                return false;

            int split = rnd.Next(min_leaf_size, max);


            if (!splitH)
            {

                LeftNode = new TreeNode();
                LeftNode.Space = new Rectangle(this.Space.X, this.Space.Y, split, this.Space.Height);
                RightNode = new TreeNode();
                RightNode.Space = new Rectangle(this.Space.X + split, this.Space.Y, this.Space.Width - split, this.Space.Height);

                Childrens.Add(LeftNode);
                Childrens.Add(RightNode);
                this.DoneDividing = true;

                return true;

            }
            else
            {

                LeftNode = new TreeNode();
                LeftNode.Space = new Rectangle(this.Space.X, this.Space.Y, this.Space.Width, split);
                RightNode = new TreeNode();
                RightNode.Space = new Rectangle(this.Space.X, this.Space.Y + split, this.Space.Width, this.Space.Height - split);

                Childrens.Add(LeftNode);
                Childrens.Add(RightNode);
                this.DoneDividing = true;

                return true;
            }


        }


    }
        #endregion

    // Digging agent class for the Agent-based algorithm
    public class DiggingAgent
    {
        public Vector2 Position;
        public int GridX { get { return (int)Position.X / Tile.TileWidth; } set { Position.X = value * Tile.TileWidth; } }
        public int GridY { get { return (int)Position.Y / Tile.TileHeight; } set { Position.Y = value * Tile.TileHeight; } }
        public Vector2 Direction;
        public float RoomChance = 5;
        public float TurnChance = 5;
        public int minRoomSize = 4;
        public int maxRoomSize = 8;
        public Stack<Point> lastCorridorPlaced = new Stack<Point>();
        public bool Debug = false;
        Random rnd = new Random();

        public void UpdateValues()
        {
            GridX = (int)Position.X / Tile.TileWidth;
            GridY = (int)Position.Y / Tile.TileHeight;
        }

        public void Initialize()
        {
            Direction = new Vector2(1, 0);
        }

        public Vector2 GetDirection(int direction)
        {
            switch (direction)
            {
                case 0: return new Vector2(0, 1);
                case 1: return new Vector2(0, -1);
                case 2: return new Vector2(1, 0);
                case 3: return new Vector2(-1, 0);
            }
            return Vector2.Zero;
        }

        public void WalkFor(int steps, ref int[,] tempGrid)
        {
            int oldGridx = (int)MathHelper.Clamp(GridX, 0, tempGrid.GetLength(1));
            int oldGridy = (int)MathHelper.Clamp(GridY, 0, tempGrid.GetLength(0));

            GridX += (int)MathHelper.Clamp((int)Direction.X * steps, 0, tempGrid.GetLength(1));
            GridY += (int)Direction.Y * steps;
            GridY = (int)MathHelper.Clamp(GridY, 0, tempGrid.GetLength(0));

            for (int y = oldGridy; y < GridY; y++)
            {
                lastCorridorPlaced.Push(new Point(y, oldGridx));
                tempGrid[y, oldGridx] = 2;
            }
            for (int x = oldGridx; x < GridX; x++)
            {
                lastCorridorPlaced.Push(new Point(x, oldGridy));
                tempGrid[oldGridy, x] = 2;
            }
        }

        public void PlaceRandomFeature(ref int[,] tempGrid)
        {
            if (tempGrid[GridY, GridX] == 0)
                return;
            if (rnd.Next(0,1000) > 995f)
            {
                Table t = new Table(Position, "Sangue", FloatingText.Map.stateManager.Game.Content.Load<Texture2D>(@"Items\blood_red2"), FloatingText.Map);
                FloatingText.Map.MapItems.Add(t);
            }
        }

        public void ChangeDirection(Random rnd)
        {
            Vector2 tempDir = Direction;

            while (Direction == tempDir)
            {
                switch (rnd.Next(4))
                {
                    case 0: this.Direction = new Vector2(0, 1); break;
                    case 1: this.Direction = new Vector2(0, -1); break;
                    case 2: this.Direction = new Vector2(1, 0); break;
                    case 3: this.Direction = new Vector2(-1, 0); break;
                }
            }
            this.TurnChance = 0;

        }
    }

    // Room types enumeration. Unused.
    public enum RoomTypes
    {
        Test = 0
    }

    // Room class for various algorithm
    public struct Room
    {
        public int X;
        public int Y;
        public int Width;
        public int Height;
        public RoomTypes RoomType;
        public bool HasDoor;
        public List<Point> Walls;
        public List<Point> Doors;
        public Rectangle RoomRectangle
        {
            get { return new Rectangle(X, Y, Width, Height); }
        }

        public bool CanPlaceDoor(int x, int y)
        {
            if (!HasDoor)
            {
                if (IsWallTile(x, y))
                    HasDoor = true;
                return IsWallTile(x, y);
            }
            return false;
        }

        public bool CollideWithOtherRoom(int[,] tempGrid)
        {
            bool collide = false;

            for (int y = Y; y < Y + Height; y++)
            {
                if (y < 0 || y >= tempGrid.GetLength(0)
                    || RoomRectangle.Left  < 0 || RoomRectangle.Left>= tempGrid.GetLength(1)
                    || RoomRectangle.Right  < 0 || RoomRectangle.Right >= tempGrid.GetLength(1))
                { continue; }

                if (tempGrid[y, RoomRectangle.Left ] == 1 || tempGrid[y, RoomRectangle.Right] == 1)
                {
                    collide = true;
                    break;
                }

            }

            if (collide)
                return true;

            for (int x = X; x < X + Width; x++)
            {
                if (x < 0 || x >= tempGrid.GetLength(0)
                   || RoomRectangle.Top - 1 < 0 || RoomRectangle.Top - 1 >= tempGrid.GetLength(0)
                   || RoomRectangle.Bottom + 1 < 0 || RoomRectangle.Bottom + 1 >= tempGrid.GetLength(0))
                { continue; }

                if (tempGrid[RoomRectangle.Top - 1, x] == 1 || tempGrid[RoomRectangle.Bottom + 1, x] == 1)
                {
                    collide = true;
                    break;
                }
            }

            return collide;
        }

        public bool IsWallTile(int x, int y)
        {
            bool returnValue = false;
            foreach (Point p in Walls)
            {
                if (p.X == x && p.Y == y)
                {
                    returnValue = true;
                    break;
                }
            }
            return returnValue;
        }

        public void SetWalls(int[,] grid)
        {
            Walls = new List<Point>();
            Doors = new List<Point>();
            for (int x = X; x < X + Width + 1; x++)
            {
                Walls.Add(new Point(x, Y));
                Walls.Add(new Point(x, Y + Height));
                Pathfinding.SolidNodes.Add(new Node(x, Y, TileType.Solid));
                Pathfinding.SolidNodes.Add(new Node(x, Y + Height, TileType.Solid));
            }
            for (int y = Y; y < Y + Height; y++)
            {
                Pathfinding.SolidNodes.Add(new Node(X, y, TileType.Solid));
                Pathfinding.SolidNodes.Add(new Node(X + Width, y, TileType.Solid));
                Walls.Add(new Point(X, y));
                Walls.Add(new Point(X + Width, y));
            }
        }

        public void SetDoors(int[,] grid)
        {
            for (int x = X; x < X + Width + 1; x++)
            {
                if (x - 1 >= 0 && x + 1 < grid.GetLength(1))
                    if (grid[Y, x + 1] == 2 || grid[Y, x - 1] == 2 || grid[Y + Height, x + 1] == 2 || grid[Y + Height, x - 1] == 2)
                        Doors.Add(new Point(x, Y));
            }
            for (int y = Y; y < Y + Height; y++)
            {
                if (y - 1 >= 0 && y + 1 < grid.GetLength(0))
                    if (grid[y + 1, X] == 2 || grid[y - 1, X] == 2 || grid[y + 1, X + Width] == 2 || grid[y - 1, X + Width] == 2)
                        Doors.Add(new Point(X, y));
            }

        }
    }

    public struct Corridor
    {
        public int X, Y, Width, Height;
        public Node StartingRoom;
        public Node EndingRoom;
        public List<Node> CorridorPath;
        public List<Node> WallsFound;
    }

    public class GridCell
    {
        public int TileID;
        public int Event;
        public int TilesetID;
        public int X;
        public int Y;
        public float Visisted;
        public Color Tint = Color.White;

        public int neighbourhood = 0;
        Map Map;

        public GridCell(int tileId, int eventid, int tilesetid, Map map)
        {
            TileID = tileId;
            Event = eventid;
            TilesetID = tilesetid;
            X = 0;
            Y = 0;
            Visisted = 100f;
            Map = map;
            //neighbourhood = CalculateNeighbours();
        }
        public int CalculateNeighboursByEvent()
        {
            int n = 0;

            if (Top() != null && Top().Event == Event)
                n++;

            if (Right() != null && Right().Event == Event)
                n += 2;

            if (Bottom() != null && Bottom().Event == Event)
                n += 4;

            if (Left() != null && Left().Event == Event)
                n += 8;


            return n;
        }
        public int CalculateNeighbours()
        {
            int n = 0;

            if (Top() != null && Top().TileID == TileID)
                n++;
            else if (Top() == null)
                n++;
            if (Right() != null && Right().TileID == TileID)
                n += 2;
            else if (Right() == null)
                n += 2;
            if (Bottom() != null && Bottom().TileID == TileID)
                n += 4;
            else if (Bottom() == null)
                n += 4;
            if (Left() != null && Left().TileID == TileID)
                n += 8;
            else if (Left() == null)
                n += 8;

            return n;
        }

        #region Neighbours Methods
        public GridCell Top()
        {
            if (Map.IsInsideMapBoundaries(new Point(X, Y - 1)))
                return Map.Layers[0].Grid[Y - 1, X];
            else
                return null;
        }
        public GridCell Bottom()
        {
            if (Map.IsInsideMapBoundaries(new Point(X, Y + 1)))
                return Map.Layers[0].Grid[Y + 1, X];
            else
                return null;
        }
        public GridCell Left()
        {
            if (Map.IsInsideMapBoundaries(new Point(X - 1, Y)))
                return Map.Layers[0].Grid[Y, X - 1];
            else
                return null;
        }
        public GridCell Right()
        {
            if (Map.IsInsideMapBoundaries(new Point(X + 1, Y)))
                return Map.Layers[0].Grid[Y, X + 1];
            else
                return null;
        }
        #endregion


    }

}

