﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gui;
using Colony.Gameplay_Elements;
using Colony.Particle_System;

namespace Colony.Gamestates
{
    public class DebugConsole : BaseGameState
    {
        #region Field Region

        Texture2D bg;
        Vector2 position;
        float FontSize = 0.7f;
        List<string> lines = new List<string>();
        List<DebugLine> dLines = new List<DebugLine>();

        #endregion

        #region Constructor Region
        public DebugConsole(Game game, GameStateManager manager)
            : base(game, manager)
        {
            bg = game.Content.Load<Texture2D>(@"Gui\iteminventorygui");
            screenWidth = 210;
            screenHeight = 150;

            position = new Vector2(stateManager.GameRef.graphics.PreferredBackBufferWidth - screenWidth - 5,
                stateManager.GameRef.graphics.PreferredBackBufferHeight - screenHeight - 5);

        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            while (dLines.Count > 6)
            {
                dLines.Remove(dLines.First());
            }

            for (int i = 0; i < dLines.Count; i++)
            {
                
                dLines[i].lifeTimer += (float)gameTime.ElapsedGameTime.Milliseconds;
               // DebugText(dLines[i].state.ToString(), Color.Red);
                if (dLines[i].lifeTimer > dLines[i].life)
                {
                    dLines.Remove(dLines[i]);
                    i--;
                }

            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            SpriteBatch spriteBatch = stateManager.GameRef.spriteBatch;
            GamePlayState g = ((GamePlayState)stateManager.GetStateByType(typeof(GamePlayState)));
            spriteBatch.Begin();

            spriteBatch.Draw(bg, new Rectangle(stateManager.GameRef.graphics.PreferredBackBufferWidth - screenWidth - 5,
                stateManager.GameRef.graphics.PreferredBackBufferHeight - screenHeight - 5,
                screenWidth, screenHeight), Color.White * 0.7f);

            spriteBatch.DrawString(FloatingText.Font,
                "Data:", new Vector2(stateManager.GameRef.graphics.PreferredBackBufferWidth - screenWidth,
                stateManager.GameRef.graphics.PreferredBackBufferHeight - screenHeight-2), Color.Blue, 0f, Vector2.Zero, FontSize, SpriteEffects.None, 0);

            spriteBatch.DrawString(FloatingText.Font, "Enemies count: " + g.CurrentMap.Enemies.Count.ToString(), position + new Vector2(10, 20), Color.White, 0f, Vector2.Zero, FontSize, SpriteEffects.None, 0);
            int p = 0;
            foreach (Emitter e in SfxManager.ParticleSystem.EmitterList)
            {
                p += e.ActiveParticles.Count();
            }

            spriteBatch.DrawString(FloatingText.Font, "Particles: " + p.ToString(), position + new Vector2(10, 40), Color.White, 0f, Vector2.Zero, FontSize, SpriteEffects.None, 0);

            spriteBatch.DrawString(FloatingText.Font, "Camera Position: " + ((int)g.CurrentMap.Camera.Position.X).ToString() + ", " + ((int)g.CurrentMap.Camera.Position.Y).ToString(),
                position + new Vector2(10, 60), Color.White, 0f, Vector2.Zero, FontSize, SpriteEffects.None, 0);


            spriteBatch.Draw(bg, new Rectangle(
                5, (int)position.Y, screenWidth, screenHeight), Color.White * 0.5f);

            spriteBatch.DrawString(FloatingText.Font, "Debug text:", new Vector2(10, position.Y+3), Color.Red, 0f, Vector2.Zero, FontSize, SpriteEffects.None, 0);

            for (int i = 0; i < dLines.Count; i++)
            {
                spriteBatch.DrawString(FloatingText.Font,
                    dLines[i].text, new Vector2(15, position.Y + (20 * (i + 1))),
                    dLines[i].color, 0f, Vector2.Zero, FontSize, SpriteEffects.None, 0);
            }

            spriteBatch.End();

        }

        public void DebugText(string _text)
        {
            dLines.Add(new DebugLine() { text = _text, color = Color.White, life = 4000f });
        }

        public void DebugText(string _text, Color _color)
        {
            dLines.Add(new DebugLine() { text = _text, color = _color, life = 4000f });
        }

        #endregion


    }

    public class DebugLine
    {
        public string text;
        public Color originalColor;
        public Color color
        {
            get
            {
                return originalColor *( 900 / lifeTimer);
            }
            set { originalColor = value; }
        }
        public float life;
        public float lifeTimer = 0f;
        public float state { get { return lifeTimer / life; } }
    }

}
        // lifet / life = x / 1
        