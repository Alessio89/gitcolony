﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gameplay_Elements;

namespace Colony.Gamestates
{
    public class TestState : BaseGameState
    {
        SpriteFont font;
        SpriteFont genericfont;
        List<Node> nodes = new List<Node>();
        List<Node> Path = new List<Node>();
        Node parentnode = new Node(2, 2, Map_Editor.TileType.None);
        Texture2D whitepixel;
        int selectedNode = 0;
        public TestState(Game game, GameStateManager manager)
            : base(game, manager)
        {

           
        }

        public override void LoadContents()
        {
            base.LoadContents();
            font = stateManager.GameRef.Content.Load<SpriteFont>(@"menuFont");
            genericfont = stateManager.GameRef.Content.Load<SpriteFont>(@"genericfont");
            whitepixel = stateManager.GameRef.Content.Load<Texture2D>(@"Gui\1pixelwhite");
            
            Pathfinding.FindPath(new Node(4, 4, Map_Editor.TileType.None), new Node(12, 0, Map_Editor.TileType.None));
            Path = Pathfinding.ReconstructPath();
            Path.Reverse();
        }
        float timer = 0f;
        float interval = 50f;

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            timer += (float)gameTime.ElapsedGameTime.Milliseconds;
            if (timer >= interval)
            {
                selectedNode++;
                if (selectedNode > Path.Count - 1)
                    selectedNode = 0;
                timer = 0f;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            SpriteBatch spriteBatch = stateManager.GameRef.spriteBatch;

            spriteBatch.Begin();
            if (Pathfinding.Debug)
            {
                foreach (Node node in Pathfinding.ClosedList)
                {
                    if (node == Pathfinding.startingNode)
                        continue;
                    DrawRectangle(node.X, node.Y, spriteBatch, Color.Black);
                    spriteBatch.DrawString(genericfont, node.Score.ToString(), new Vector2((node.X) * 32, (node.Y) * 32), Color.Green);
                    spriteBatch.DrawString(genericfont, node.Heuristic.ToString(), new Vector2(((node.X) * 32) + 20, (node.Y) * 32), Color.Black);
                    spriteBatch.DrawString(genericfont, node.MovementEergy.ToString(), new Vector2((node.X) * 32, ((node.Y) * 32) + 20), Color.White);

                }
                foreach (Node node in Pathfinding.OpenList)
                {
                    if (node == Pathfinding.startingNode)
                        continue;
                    DrawRectangle(node.X, node.Y, spriteBatch, Color.Green);
                    spriteBatch.DrawString(genericfont, node.Score.ToString(), new Vector2((node.X) * 32, (node.Y) * 32), Color.LightGreen);
                    spriteBatch.DrawString(genericfont, node.Heuristic.ToString(), new Vector2(((node.X) * 32) + 20, (node.Y) * 32), Color.LightGreen);
                    spriteBatch.DrawString(genericfont, node.MovementEergy.ToString(), new Vector2((node.X) * 32, ((node.Y) * 32) + 20), Color.LightGreen);
                    if (node.Prop != "")
                        spriteBatch.DrawString(font, node.Prop, new Vector2(((node.X) * 32) + 6, ((node.Y) * 32) + 6), Color.Cyan);
                }
                foreach (Node node in Pathfinding.ReconstructPath())
                {
                    DrawFilledRectangle(node.X, node.Y, spriteBatch, Color.Yellow, Color.Cyan);
                    if (node.Prop != "")
                        spriteBatch.DrawString(font, node.Prop, new Vector2(((node.X) * 32) + 6, ((node.Y) * 32) + 6), Color.Cyan);
                }
            }

            foreach (Node n in Pathfinding.SolidNodes)
            {
                DrawFilledRectangle(n.X, n.Y, spriteBatch, Color.Black, Color.Gray);
            }


            DrawFilledRectangle(Path[selectedNode].X, Path[selectedNode].Y, spriteBatch, Color.Black, Color.Green);
            
           
            spriteBatch.DrawString(font, "A", new Vector2((Pathfinding.startingNode.X * 32) + 6, (Pathfinding.startingNode.Y * 32) + 6), Color.Blue);
            DrawRectangle(Pathfinding.startingNode.X, Pathfinding.startingNode.Y, spriteBatch, Color.White);
            spriteBatch.DrawString(font, "B", new Vector2((Pathfinding.endingNode.X * 32) + 6, (Pathfinding.endingNode.Y * 32) + 6), Color.Red);
            DrawRectangle(Pathfinding.endingNode.X, Pathfinding.endingNode.Y, spriteBatch, Color.White);
            spriteBatch.End();
        }

        public void DrawRectangle(int topLeftX, int topLeftY, SpriteBatch spritebatch, Color color)
        {
            spritebatch.Draw(whitepixel, new Rectangle(topLeftX*32, topLeftY*32, 32, 1), color);
            spritebatch.Draw(whitepixel, new Rectangle(topLeftX*32, topLeftY*32, 1, 32), color);
            spritebatch.Draw(whitepixel, new Rectangle((topLeftX*32)+32, topLeftY*32, 1, 32), color);
            spritebatch.Draw(whitepixel, new Rectangle(topLeftX*32, (topLeftY*32)+32, 32, 1), color);
        }
        public void DrawFilledRectangle(int topLeftX, int topLeftY, SpriteBatch spritebatch, Color color, Color bgColor)
        {
            spritebatch.Draw(whitepixel, new Rectangle(topLeftX * 32, topLeftY * 32, 32, 32), bgColor);
            spritebatch.Draw(whitepixel, new Rectangle(topLeftX * 32, topLeftY * 32, 32, 1), color);
            spritebatch.Draw(whitepixel, new Rectangle(topLeftX * 32, topLeftY * 32, 1, 32), color);
            spritebatch.Draw(whitepixel, new Rectangle((topLeftX * 32) + 32, topLeftY * 32, 1, 32), color);
            spritebatch.Draw(whitepixel, new Rectangle(topLeftX * 32, (topLeftY * 32) + 32, 32, 1), color);
        }
    }
}
