﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.InputHandler;

namespace Colony.Gui
{
    public class GenericTextInput : BaseGuiElement
    {

        #region Field Region

        public Action OnConfirm;
        public List<string> Texts = new List<string>();
        public List<Vector2> textPositions = new List<Vector2>();
        public List<TextBox> TextBoxes = new List<TextBox>();

        #endregion

        #region Constructor Region

        public GenericTextInput(Gamestates.GameStateManager manager)
            : base(manager)
        {
            ContentManager content = manager.GameRef.Content;

            image = content.Load<Texture2D>(@"Gui\menuBg");
            width = 350;
            height = 50;
            font = content.Load<SpriteFont>(@"menuFont");

        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            foreach (TextBox textbox in TextBoxes)
                textbox.Update(gameTime);

            if (Manager.GameRef.InputHandler.IsKeyPressed(Keys.Enter))
            {
                if (OnConfirm != null)
                    OnConfirm();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            if (Visible)
            {
                spriteBatch.Draw(image, new Rectangle((int)Position.X, (int)Position.Y, width, height), Color.White);
                foreach (string text in Texts)
                {
                    spriteBatch.DrawString(font, text, Position + textPositions[Texts.IndexOf(text)], Color.White);
                }
                foreach (TextBox textbox in TextBoxes)
                    textbox.Draw(spriteBatch);
            }
        }

        #endregion


    }
}
