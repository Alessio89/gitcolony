﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.InputHandler;

namespace Colony.Gui
{
    public class TextBox : BaseGuiElement
    {

        #region Field Region

        int MaxChars;
        Texture2D bg;

        #endregion

        #region Constructor Region

        public TextBox(Gamestates.GameStateManager manager, int width, int height, int maxchars)
            : base(manager)
        {
            HasFocus = false;
            ContentManager content = manager.GameRef.Content;
            image = content.Load<Texture2D>(@"Gui\1pixelwhite");
            font = content.Load<SpriteFont>("menuFont");
            bg = content.Load<Texture2D>(@"Gui\menuBg");
            this.width = width;
            this.height = height;
            text = "";
            MaxChars = maxchars;
        }


        public TextBox(Gamestates.GameStateManager manager, int width, int height, int maxchars, Vector2 position)
            : base(manager)
        {
            HasFocus = false;
            ContentManager content = manager.GameRef.Content;
            image = content.Load<Texture2D>(@"Gui\1pixelwhite");
            font = content.Load<SpriteFont>("menuFont");
            bg = content.Load<Texture2D>(@"Gui\menuBg");
            this.width = width;
            this.height = height;
            text = "";
            MaxChars = maxchars;
            Position = position;
        }


        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (HasFocus)
            {
                char k;
                if (KeysManager.TryConvertKeyboardInput(Manager.GameRef.InputHandler, out k))
                {
                    if (text.Length < MaxChars)
                        text += k;
                }

                if (Manager.GameRef.InputHandler.IsKeyPressed(Keys.Back))
                {
                    if (text.Length > 0)
                    {
                        text = text.Remove(text.Length - 1);
                    }

                }
            }

            if (IsMouseInsideThis() && Manager.GameRef.InputHandler.LeftClick())
            {
                HasFocus = true;
                if (DebugMode)
                    Console.WriteLine("Focus preso textbox");
            }
            else if (!IsMouseInsideThis() && Manager.GameRef.InputHandler.LeftClick())
            {
                HasFocus = false;
            }

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Draw(bg, new Rectangle((int)Position.X, (int)Position.Y, width, height), Color.White);
            spriteBatch.Draw(image, new Rectangle((int)Position.X, (int)Position.Y, width, 1), Color.White);
            spriteBatch.Draw(image, new Rectangle((int)Position.X, (int)Position.Y + height, width, 1), Color.White);
            spriteBatch.Draw(image, new Rectangle((int)Position.X, (int)Position.Y, 1, height), Color.White);
            spriteBatch.Draw(image, new Rectangle((int)Position.X + width, (int)Position.Y, 2, height), Color.White);
            spriteBatch.DrawString(font, text, Position+ new Vector2(3, -2), Color.White);

        }

        public string GetText()
        {

            return text;
        }

        #endregion

    }
}
