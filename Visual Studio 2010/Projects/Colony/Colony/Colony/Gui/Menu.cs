﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.InputHandler;

// TODO: Update -> Usare funzioni di baseguielement

namespace Colony.Gui
{
    public class Menu : BaseGuiElement
    {

        #region Field Region

        public int selectedItem;
        string[] menuItems;
        public List<Action> OnClicks = new List<Action>();
        Gamestates.GameStateManager manager;
        public bool MouseActivated = true;
        public bool RightClickActivation = true;
        public Keys ActivationKey = Keys.F1;
        public Keys DeactivationKey = Keys.F2;
        public bool CanOpen = true;

        public bool OpenOnlyOne = true;
        public List<Menu> openableMenus = new List<Menu>();


        #endregion

        #region Constructor Region

        public Menu(string[] menuItems, Texture2D backgroundImage, Gamestates.GameStateManager manager)
            : base(manager)
        {
            this.menuItems = menuItems;
            image = backgroundImage;

            font = manager.GameRef.Content.Load<SpriteFont>("menuFont");
            this.manager = manager;
            int biggestStringIndex = 0;
            string biggestString = "";
            for (int i = 0; i < menuItems.Length; i++)
            {
                if (font.MeasureString(menuItems[i]).X > font.MeasureString(biggestString).X)
                {
                    biggestStringIndex = i;
                    biggestString = menuItems[i];
                }
            }

            this.width = (int)font.MeasureString(biggestString).X + 8;
            this.height = (int)font.MeasureString(biggestString).Y * menuItems.Length;

        }


        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            InputHandler.InputHandler inputHandler = manager.GameRef.InputHandler;

            if (Mouse.GetState().X > Position.X && Mouse.GetState().X < Position.X + width && Mouse.GetState().Y > Position.Y && Mouse.GetState().Y < Position.Y + height && Visible)
            {
                selectedItem = (int)Math.Floor((Mouse.GetState().Y - this.Position.Y) / (height / menuItems.Length));
                drawedColor = color;
                HasFocus = true;
            }
            else
            {
                drawedColor = color * 0.7f;
                HasFocus = false;
                selectedItem = -1;
            }
            if (CanOpen && !Visible)
            {
                if (MouseActivated)
                {
                    if (RightClickActivation)
                    {
                        bool openmenu = false;

                        if (inputHandler.RightClick())
                        {

                            if (OpenOnlyOne && openableMenus.Count > 0)
                            {

                                foreach (Menu menu in openableMenus)
                                {

                                    if (menu.Visible)
                                    {
                                        openmenu = true;
                                    }
                                }

                            }
                            if (!openmenu)
                            {
                                this.Visible = true;
                                this.Position = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                            }
                        }
                    }
                    else
                    {

                        bool openmenu = false;

                        if (inputHandler.LeftClick())
                        {

                            if (OpenOnlyOne && openableMenus.Count > 0)
                            {

                                foreach (Menu menu in openableMenus)
                                {

                                    if (menu.Visible)
                                    {
                                        openmenu = true;
                                    }
                                }

                            }
                            if (!openmenu)
                            {
                                this.Visible = true;
                                this.Position = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                            }
                        }


                    }
                }
                else
                {
                    if (inputHandler.IsKeyPressed(ActivationKey))
                    {
                        this.Visible = true;
                        this.Position = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                    }
                }
            }


            if (inputHandler.LeftClick())
            {
                if (Mouse.GetState().X < Position.X || Mouse.GetState().X > Position.X + width || Mouse.GetState().Y < Position.Y || Mouse.GetState().Y > Position.Y + height)
                    this.Visible = false;
                else
                {


                    if (selectedItem < OnClicks.Count && selectedItem >= 0)
                        OnClicks[selectedItem]();
                }
            }


            else
            {
                if (inputHandler.IsKeyPressed(DeactivationKey))
                {
                    this.Visible = false;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                spriteBatch.Draw(image, new Rectangle((int)Position.X, (int)Position.Y, width, height), drawedColor);

                for (int i = 0; i < menuItems.Length; i++)
                {
                    Vector2 textSize = font.MeasureString(menuItems[i]);
                    if (i != selectedItem)
                        spriteBatch.DrawString(font, menuItems[i], new Vector2(Position.X + 3, Position.Y + (font.MeasureString(menuItems[i]).Y) * i), drawedColor);
                    else
                        spriteBatch.DrawString(font, menuItems[i], new Vector2(Position.X + 3, Position.Y + (font.MeasureString(menuItems[i]).Y) * i), Color.Wheat);
                }
            }

        }

        #endregion

    }
}
