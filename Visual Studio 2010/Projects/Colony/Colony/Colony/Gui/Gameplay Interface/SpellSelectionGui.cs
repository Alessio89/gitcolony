﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.Gamestates;
using Colony.InputHandler;
using Colony.Gamestates.States;
using Colony.Gamestates.Map_Editor;
using Colony.Gameplay_Elements;
using System.Diagnostics;
using Colony.Gameplay_Elements.Items.Map_Items;
using Colony.Gameplay_Elements.Items;

namespace Colony.Gui.Gameplay_Interface
{
    public class SpellSelectionGui : BaseGameState
    {
        #region Field Region

        Texture2D background;
        Texture2D ItemRectangle;
        Texture2D SelectedItemRectangle;
        BaseCharacter Player;
        Vector2 Position;
        public int width = 146;
        public int height = 144;

        public int Selected = 0;
        int itemsPerRow = 4;

        #endregion

        #region Constructor Region
        public SpellSelectionGui(BaseCharacter player, Game game, GameStateManager manager)
            : base(game, manager)
        {
            background = game.Content.Load<Texture2D>(@"Gui\iteminventorygui");
            SelectedItemRectangle = game.Content.Load<Texture2D>(@"Gui\actionbuttonSelected");
            ItemRectangle = game.Content.Load<Texture2D>(@"Gui\actionbutton");
            Player = player;
            Position = new Vector2(manager.GameRef.graphics.PreferredBackBufferWidth / 2 - width / 2, manager.GameRef.graphics.PreferredBackBufferHeight / 2 - height / 2);
        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            Selected = (int)MathHelper.Clamp(Selected, 0, Player.Spells.Count - 1);
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.Left))
            {
                Selected = (int)MathHelper.Clamp(Selected - 1, 0, Player.Spells.Count - 1);
            }
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.Right))
            {
                Selected = (int)MathHelper.Clamp(Selected + 1, 0, Player.Spells.Count - 1);
            }
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.Down))
            {
                Selected = (int)MathHelper.Clamp(Selected + itemsPerRow, 0, Player.Spells.Count - 1);
            }
            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.Up))
            {
                Selected = (int)MathHelper.Clamp(Selected - itemsPerRow, 0, Player.Spells.Count - 1);
            }

            InputHandler.InputHandler inputHandler = stateManager.GameRef.InputHandler;

            if (inputHandler.IsKeyPressed(Keys.D1))
                Gui.ActionBar.UsableItems[0] = new InventoryItem(Player.Spells[Selected]);
            else if (inputHandler.IsKeyPressed(Keys.D2))
                Gui.ActionBar.UsableItems[1] = new InventoryItem(Player.Spells[Selected]);
            else if (inputHandler.IsKeyPressed(Keys.D3))
                Gui.ActionBar.UsableItems[2] = new InventoryItem(Player.Spells[Selected]);
            else if (inputHandler.IsKeyPressed(Keys.D4))
                Gui.ActionBar.UsableItems[3] = new InventoryItem(Player.Spells[Selected]);
            else if (inputHandler.IsKeyPressed(Keys.D5))
                Gui.ActionBar.UsableItems[4] = new InventoryItem(Player.Spells[Selected]);
            else if (inputHandler.IsKeyPressed(Keys.D6))
                Gui.ActionBar.UsableItems[5] = new InventoryItem(Player.Spells[Selected]);
            else if (inputHandler.IsKeyPressed(Keys.D7))
                Gui.ActionBar.UsableItems[6] = new InventoryItem(Player.Spells[Selected]);
            else if (inputHandler.IsKeyPressed(Keys.D8))
                Gui.ActionBar.UsableItems[7] = new InventoryItem(Player.Spells[Selected]);
            else if (inputHandler.IsKeyPressed(Keys.D9))
                Gui.ActionBar.UsableItems[8] = new InventoryItem(Player.Spells[Selected]);

            else if (inputHandler.IsKeyPressed(Keys.D0))
                Gui.ActionBar.UsableItems[9] = new InventoryItem(Player.Spells[Selected]);

            if (stateManager.GameRef.InputHandler.IsKeyPressed(Keys.NumPad8))
            {
                Close();
                BaseGameState s = (from i in stateManager.GameStates where i.GetType() == typeof(GamePlayState) select i).First();
                s.HasFocus = true;
            }

            if (inputHandler.IsKeyPressed(Keys.NumPad7))
            {
                Close();
                stateManager.PushToStack(new InventoryGui(Player.Inventory, stateManager.Game, stateManager));
            }

            if (inputHandler.LeftClick() && InsideThis(new Vector2(Mouse.GetState().X, Mouse.GetState().Y)))
            {

                DebugConsole d = stateManager.GetStateByType(typeof(DebugConsole)) as DebugConsole;

                Vector2 relativePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y) - Position;

                int r = (int)relativePos.Y / 36;

                int c = (int)relativePos.X / 36;
                int sel = c + (r * itemsPerRow);
                d.DebugText("Click! " + sel.ToString());
                Selected = sel;

            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            SpriteBatch spriteBatch = stateManager.GameRef.spriteBatch;

            int x = 0;

            spriteBatch.Begin();

            spriteBatch.Draw(background, new Rectangle((int)Position.X, (int)Position.Y, width, height), Color.White);
            for (int i = 0; i < Player.Spells.Count; i++)
            {

                int row = (int)Math.Floor((double)i / itemsPerRow);
                x++;
                if (i % itemsPerRow == 0)
                    x = 0;

                
                Color rectColor = Color.Black;
                if (i == Selected)
                    spriteBatch.Draw(SelectedItemRectangle, new Rectangle(8 + (int)Position.X + x * 34, 6 + (int)Position.Y + 34 * row, 32, 32), Color.White);
                else
                spriteBatch.Draw(ItemRectangle, new Rectangle(8 + (int)Position.X + x * 34, 6 + (int)Position.Y + 34 * row, 32, 32), Color.White);
                spriteBatch.Draw(Player.Spells[i].GetTexture(), new Rectangle(8 + (int)Position.X + x * 34, 6 + (int)Position.Y + 34 * row, 32, 32), Color.White);

                //spriteBatch.DrawString(FloatingText.Font, Inventory.Items[i].Count.ToString(), Position + new Vector2(x * 32, row * 32 + 6), Color.White);

            }



            spriteBatch.End();
        }
        public bool InsideThis(Vector2 v)
        {
            if (v.X > Position.X && v.X < Position.X + screenWidth && v.Y > Position.Y && v.Y < Position.Y + screenHeight)
                return true;
            return false;
        }
        #endregion

    }
}
