﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.InputHandler;
using Colony.Gameplay_Elements.Items.Map_Items;
using Colony.Gameplay_Elements.Items;
using Colony.Gameplay_Elements.Spells;
using Colony.Gameplay_Elements;
using Colony.Gamestates;

namespace Colony.Gui
{
    public static class ActionBar
    {

        #region Field Region

        static Texture2D Graphic;
        static Texture2D ActionButton;
        static Texture2D ActionButtonSelected;
        public static InventoryItem[] UsableItems = new InventoryItem[10];
        public static int SelectedItem;
        static int Width = 348;
        static int Height = 44;
        static Game1 GameRef;
        public static Vector2 Position;

        static bool Initialized;

        #endregion

        #region Methods Region

        public static void Initialize(Texture2D graphic, Game1 game)
        {
            Graphic = graphic;
            ActionButton = game.Content.Load<Texture2D>(@"Gui\actionbutton");
            ActionButtonSelected = game.Content.Load<Texture2D>(@"Gui\actionbuttonSelected");
            //UsableItems = new List<InventoryItem>(10);
            SelectedItem = 0;

            Initialized = true;
            GameRef = game;
            Position = new Vector2(game.graphics.PreferredBackBufferWidth / 2 - Width / 2, game.graphics.PreferredBackBufferHeight - Height - 5);
        }

        public static void Update(GameTime gameTime)
        {
            // if ( SelectedItem >= UsableItems.Count)
            //   SelectedItem = 0;
            if (UsableItems[SelectedItem] != null)
                UsableItems[SelectedItem].InstancesList[0].Selected();

            if (GameRef.InputHandler.LeftClick())
            {
                if (InsideThis(new Vector2(Mouse.GetState().X, Mouse.GetState().Y)))
                {
                    DebugConsole d = GameRef.StateManager.GetStateByType(typeof(DebugConsole)) as DebugConsole;
                    
                    Vector2 relativePosition = new Vector2(Mouse.GetState().X, Mouse.GetState().Y) - Position;
                    int sel = (int)relativePosition.X / 34;
                    d.DebugText("Click " + relativePosition.X.ToString()+", "+relativePosition.Y.ToString()+" - "+sel.ToString());
                    SelectedItem = sel;
                    

                }
            }
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            // Must initialize first!
            if (!Initialized)
                return;

            spriteBatch.Draw(Graphic, new Rectangle((int)Position.X, (int)Position.Y, Width, Height), Color.White);
            for (int i = 0; i < 10; i++)
            {
                Color c = Color.White;
                if (SelectedItem == i)
                    spriteBatch.Draw(ActionButtonSelected, new Rectangle((int)Position.X + 4 + i * 34, (int)Position.Y + Height / 2 - 16, 32, 32), Color.White);
                else
                    spriteBatch.Draw(ActionButton, new Rectangle((int)Position.X + 4 + i * 34, (int)Position.Y + Height / 2 - 16, 32, 32), Color.White);
                //GameRef.StateManager.DrawRectangle((int)Position.X + 2 + i * 34, (int)Position.Y + Height / 2 - 16, 32, 32, spriteBatch, c);

                int number = i;
                if (number == 0)
                    number = 1;
                else
                    number++;
                string snumber = number.ToString();
                if (snumber == "10")
                    snumber = "0";

                spriteBatch.DrawString(FloatingText.Font, snumber, new Vector2(Position.X + 4 + i * 34, Position.Y - Height / 2), Color.Red);
            }
            for (int i = 0; i < UsableItems.Length; i++)
            {
                if (UsableItems[i] != null)
                    spriteBatch.Draw(UsableItems[i].InstancesList[0].GetTexture(), new Rectangle((int)Position.X + 4 + i * 34, (int)Position.Y + Height / 2 - 16, 32, 32), Color.White);
            }

        }

        public static void Select(int index)
        {
            if (UsableItems[SelectedItem] != null)
            {
                UsableItems[SelectedItem].InstancesList[0].UnSelected();
            }
            SelectedItem = index;
        }

        static bool InsideThis(Vector2 v)
        {
            if (v.X > Position.X && v.X < Position.X + Width && v.Y > Position.Y && v.Y < Position.Y + Height)
                return true;
            return false;
        }
        #endregion

    }

    public interface IUsable
    {
        // public Texture2D Sprite;
    }
}
