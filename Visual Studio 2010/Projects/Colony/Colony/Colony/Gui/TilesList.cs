﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.InputHandler;
using Colony.Gamestates.Map_Editor;

// TODO: Update -> Usare funzioni di baseguielement

namespace Colony.Gui
{
    public class TilesList : BaseGuiElement
    {

        #region Field Region

        List<Tileset> tilesets;
        MapEditorState MapEditor;
        Texture2D whitepixel;
        int selectedTileset = 0;

        #endregion

        #region Constructor Region

        public TilesList(Gamestates.GameStateManager manager, MapEditorState mapEditor)
            : base(manager)
        {

            tilesets = mapEditor.tilesets;
            width = tilesets[selectedTileset].TileSet.Width + 8;
            height = tilesets[selectedTileset].TileSet.Height + 8;
            image = manager.GameRef.Content.Load<Texture2D>(@"Gui\menuBg");
            MapEditor = mapEditor;
            font = manager.GameRef.Content.Load<SpriteFont>("menuFont");
            whitepixel = manager.GameRef.Content.Load<Texture2D>(@"Gui\1pixelwhite");
        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            InputHandler.InputHandler inputHandler = Manager.GameRef.InputHandler;
            if (inputHandler.IsKeyPressed(Keys.Tab) && !Visible)
            {
                Visible = true;
                HasFocus = true;
            }
            else if (inputHandler.IsKeyPressed(Keys.Tab) && Visible)
            {
                Visible = false;
                HasFocus = false;
            }
            int mouseXInsideItem = (int)Math.Floor(Mouse.GetState().X - Position.X);
            int mouseYInsideItem = (int)Math.Floor(Mouse.GetState().Y - Position.Y);
            if (inputHandler.LeftClick())
            {
                if (mouseXInsideItem > 200 &&
                    mouseXInsideItem < 200 + font.MeasureString("+").X &&
                    mouseYInsideItem > height - 5 &&
                    mouseYInsideItem < height - 5 + font.MeasureString("+").Y)
                {
                    if (MapEditor.SelectedLayer != 0)
                        MapEditor.SelectedLayer--;
                }
                if (Mouse.GetState().X > Position.X + 230 &&
                    Mouse.GetState().X < Position.X + 230 + font.MeasureString("+").X &&
                    Mouse.GetState().Y > Position.Y + height - 5 &&
                    Mouse.GetState().Y < Position.Y + height - 5 + font.MeasureString("+").Y)
                {

                    MapEditor.SelectedLayer++;
                }
                if (Mouse.GetState().X > Position.X + 120 &&
                    Mouse.GetState().X < Position.X + 120 + font.MeasureString("L+").X &&
                    Mouse.GetState().Y > Position.Y + height - 5 &&
                    Mouse.GetState().Y < Position.Y + height - 5 + font.MeasureString("L+").Y)
                {

                    MapEditor.AddLayer();
                }
                if (Mouse.GetState().X > Position.X + 150 &&
                    Mouse.GetState().X < Position.X + 150 + font.MeasureString("L-").X &&
                    Mouse.GetState().Y > Position.Y + height - 5 &&
                    Mouse.GetState().Y < Position.Y + height - 5 + font.MeasureString("L-").Y)
                {

                    if (MapEditor.map.Layers.Count > 1 && MapEditor.SelectedLayer != 0)
                        MapEditor.RemoveLayer(MapEditor.SelectedLayer);
                }
            }

            if (IsMouseInsideThis())
            {
                if (inputHandler.LeftClick() && HasFocus)
                {
                    int tileIDX = mouseXInsideItem / Tile.TileWidth;
                    int tileIDY = mouseYInsideItem / Tile.TileHeight;

                    int tileID = tileIDX + (tilesets[selectedTileset].tilesWide * tileIDY);
                    if (tileID < (tilesets[selectedTileset].tilesWide * tilesets[selectedTileset].tilesHigh))
                    {
                        MapEditor.SelectedTile.TileID = tileID;
                        Console.WriteLine("X: {1} Y: {2} TileID: {0} TileSet: {3}", tileID, tileIDX, tileIDY, selectedTileset);
                    }
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                spriteBatch.Draw(image, new Rectangle((int)Position.X - 4, (int)Position.Y - 4, width, height), Color.White);
                spriteBatch.Draw(tilesets[selectedTileset].TileSet, Position, Color.White);
                spriteBatch.DrawString(font, "Layer: " + MapEditor.SelectedLayer.ToString() + " / " + (MapEditor.map.Layers.Count - 1).ToString(), Position + new Vector2(0, height - 5), Color.White);
                spriteBatch.DrawString(font, "-", Position + new Vector2(200, height - 5), Color.White);
                spriteBatch.DrawString(font, "+", Position + new Vector2(230, height - 5), Color.White);
                spriteBatch.DrawString(font, "L+", Position + new Vector2(120, height - 5), Color.White);
                spriteBatch.DrawString(font, "L-", Position + new Vector2(150, height - 5), Color.White);

                int rectangleX = 0;
                int rectangleY = 0;

                if (MapEditor.SelectedTile.TileID <= tilesets[selectedTileset].tilesWide)
                {
                    rectangleX = (int)(Position.X + MapEditor.SelectedTile.TileID * Tile.TileWidth);
                    rectangleY = (int)Position.Y;
                }
                else
                {
                    // gridY = tileid/tileswide
                    // screenY = gridY * TileHeight + position
                    // gridX = tileid - (gridy*tileswide)
                    // screenX = gridX * Tilewide + position
                    
                    // -gridy*tileheighy = position - screeny
                    // gridy = screeny-pos/tileheight

                    int gridY = (int)(MapEditor.SelectedTile.TileID / tilesets[selectedTileset].tilesWide);
                    int gridX = (int)(MapEditor.SelectedTile.TileID - (gridY * tilesets[selectedTileset].tilesWide));

                    rectangleY = (gridY * Tile.TileHeight)  + (int)Position.Y;
                    rectangleX = (gridX * Tile.TileWidth) + (int)Position.X;

                }

                spriteBatch.Draw(whitepixel, new Rectangle(rectangleX, rectangleY, Tile.TileWidth, 2), Color.White);
                spriteBatch.Draw(whitepixel, new Rectangle(rectangleX, rectangleY + Tile.TileHeight, Tile.TileWidth + 1, 2), Color.White);
                spriteBatch.Draw(whitepixel, new Rectangle(rectangleX, rectangleY, 2, Tile.TileHeight), Color.White);
                spriteBatch.Draw(whitepixel, new Rectangle(rectangleX + Tile.TileWidth, rectangleY, 2, Tile.TileHeight + 1), Color.White);
            }
        }

        public void ChangeTileset(int tilesetID)
        {
            if (tilesetID < MapEditor.tilesets.Count)
            {
                selectedTileset = tilesetID;
                MapEditor.SelectedTile.TilesetID = tilesetID;
                MapEditor.SelectedTileSet = tilesetID;
            }
        }
        #endregion

    }
}
