﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Colony.InputHandler;

namespace Colony.Gui
{
    public class MapEditorToolSelection : BaseGuiElement
    {

        #region Field Region

        Texture2D paint, fill, erase, events, grid;
        Gamestates.Map_Editor.MapEditorState MapEditor;

        #endregion

        #region Constructor Region

        public MapEditorToolSelection(Gamestates.GameStateManager manager, Gamestates.Map_Editor.MapEditorState mapEditor)
            : base(manager)
        {
            width = 400;
            height = 40;
            HasFocus = false;
            ContentManager content = manager.GameRef.Content;
            image = content.Load<Texture2D>(@"Gui\menuBg");
            paint = content.Load<Texture2D>(@"Mouse Icons\brush");
            erase = content.Load<Texture2D>(@"Mouse Icons\eraser");
            fill = content.Load<Texture2D>(@"Mouse Icons\fill");
            events = content.Load<Texture2D>(@"Mouse Icons\event");
            grid = content.Load<Texture2D>(@"Mouse Icons\grid");
            MapEditor = mapEditor;
        }

        #endregion

        #region Methods Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            InputHandler.InputHandler inputHandler = Manager.GameRef.InputHandler;

            GetFocusAt(0, 0, width, 8);
            LoseFocusAt(0, 0, width, height);

            if (HasFocus)
            {
                Visible = true;
            }
            else
                Visible = false;

            if (IsMouseInsideThis() && HasFocus)
            {
                if (inputHandler.LeftClick())
                {
                    if (Mouse.GetState().X > 4 && Mouse.GetState().X < 36)
                    {
                        MapEditor.CurrentAction = Gamestates.Map_Editor.ClickAction.Paint;
                    }

                    else if (Mouse.GetState().X > 45 && Mouse.GetState().X < 77)
                    {
                        MapEditor.CurrentAction = Gamestates.Map_Editor.ClickAction.Fill;
                    }

                    else if (Mouse.GetState().X > 86 && Mouse.GetState().X < 118)
                        MapEditor.CurrentAction = Gamestates.Map_Editor.ClickAction.Erase;
                    else if (Mouse.GetState().X > 127 && Mouse.GetState().X < 159)
                        MapEditor.CurrentAction = Gamestates.Map_Editor.ClickAction.Events;
                    else if (Mouse.GetState().X > 168 && Mouse.GetState().X < 200)
                        MapEditor.map.GridActive = !MapEditor.map.GridActive;
                }
            }
            
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            if (Visible)
            {
                spriteBatch.Draw(image, new Rectangle(0, 0, width, height), Color.White);
                spriteBatch.Draw(paint, new Rectangle(4, 2, 32, 32), Color.White);
                spriteBatch.Draw(fill, new Rectangle(45, 2, 32, 32), Color.White);
                spriteBatch.Draw(erase, new Rectangle(86, 2, 32, 32), Color.White);
                spriteBatch.Draw(events, new Rectangle(127, 2, 32, 32), Color.White);
                spriteBatch.Draw(grid, new Rectangle(168, 2, 32, 32), Color.White);
            }
        }

        #endregion

    }
}
